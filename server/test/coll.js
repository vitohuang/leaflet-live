// Set the environment to test
process.env.NODE_ENV = 'test';

let mongoose = require('mongoose');

let chai = require('chai');
let chaiHttp = require('chai-http');
let should = chai.should();

chai.use(chaiHttp);

const endPoint = 'http://localhost:5000';

// Test
describe('Collection', () => {
  // Delete everything before test
  beforeEach((done) => {
    // Remove
    done();
  });

  /*
   * Test GET
   */
  describe('/GET', () => {
    it('Should GET to server', (done) => {
      chai.request(endPoint)
        .get('/coll/race')
        .end((error, res) => {
          res.should.have.status(200);
          //console.log('after get', res);
          done();
        })
    })
  })

  // Test POST
  describe('/POST', () => {
  });

  // Test PUT
  describe('/PUT', () => {
  });
})
