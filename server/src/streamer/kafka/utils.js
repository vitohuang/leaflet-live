const debug = require('debug')('live-kafka-utils');
const _ = require('lodash');
const rp = require('request-promise');

// Broadcast live stage to the discover channel
function broadcastLiveStage(kafkaProducer, kafkaConsumer, topicName) {
  return new Promise((resolve, reject) => {
    if (kafkaProducer && kafkaConsumer) {
      // Get the latest from the discover topic
      const discoveryTopic = kafkaConsumer.discoveryTopic;
      const latest = kafkaConsumer.getHistory(discoveryTopic).latest();

      let needBroadcast = false;
      let newTopics = null;
      if  (typeof latest == 'undefined') {
        debug('Nothing on the discover topic yet, so put the first one one');
        needBroadcast = true;
        newTopics = [topicName];
      } else if (latest && latest.value) {
        const existingTopics = latest.value;
        debug('Got the latest in the discovery topic', existingTopics);

        debug('Got the latest in the discovery topic', existingTopics);
        // Check if the topic name is inside the existing topics
        if (existingTopics.indexOf(topicName) == -1) {
          needBroadcast = true;
          newTopics = existingTopics.concat([topicName]);
        }
      }

      if (needBroadcast && newTopics) {
        debug('Going to broadcast - latest', discoveryTopic, newTopics);
        kafkaProducer.toPipe(discoveryTopic, 'liveStages', newTopics)
          .then((data) => {
            resolve(data);
          })
          .catch((error) => {
            reject(error);
          });
      } else {
        debug('Do not need to broadcast');
        resolve(true);
      }
    } else {
      reject('Sorry no Kafka producer/consumer');
    }
  })
}

// Create a new topic
// Need to set auto.create.topics.enable on the server
function ensureTopicExist(producer, topic) {
  return new Promise((resolve, reject) => {
    debug('Ensure topic exist');
    if (producer) {
      producer.client.loadMetadataForTopics([topic], function(error, results) {
        // Check if the result
        if (error) {
          debug(`The topic - ${topic} cant not get meta data`, error);
          reject(error);
        } else {
          let topics = _.get(results, '1.metadata');
          debug('List of topics', _.keys(topics));

          if (_.isEmpty(topics[topic])) {
            debug('Going to create new topci - ', topic);
            producer.createTopics([topic], true, function(error, response) {
              if (error) {
                debug('Sorry can not create topic');
                reject(error);
              } else {
                debug(`${topic} created`);
                resolve(response);
              }
            });
          } else {
            debug(`${topic} already existed on kafka`);
            resolve(true);
          }
        }
      })
    } else {
      reject('Sorry no Kafka producer');
    }
  })
}

const dbHost = 'mongo';
const dbPort = '27017';
const endpoint = 'http://kafka:8083';
function createConnectorSink(topicName) {
  const connectorName = `mongo-sink-${topicName}`;

  const sinkConfig = {
   "name": connectorName,
   "config" :{
     "connector.class":"com.startapp.data.MongoSinkConnector",
     "tasks.max":"1",
     "db.host": dbHost,
     "db.port": dbPort,
     "db.name":"kafka",
     "db.collections": "stageTopics",
     "write.batch.enabled":"true",
     "write.batch.size":"200",
     "connect.use_schema":"false",
     "topics": topicName,
    },
  };

  rp({
    uri: endpoint + '/connectors',
    json: true,
  })
    .then((result) => {
      debug('result', result);
      if (result.indexOf(connectorName) === -1) {
        debug('Need to create a new sink connector');

        // Going to create the connector
        rp({
          uri: endpoint + '/connectors',
          method: 'POST',
          json: sinkConfig,
        })
          .then((connectors) => {
            debug('created connector', connectorName, connectors);
          })
          .catch((error) => {
            debug('There is error creating connector', error);
          })
      } else {
        debug(`${connectorName} already exists`);
      }
    })
    .catch((error) => {
      debug('there is an error checking connectors', error);
    })
}

module.exports = {
  ensureTopicExist,
  createConnectorSink,
  broadcastLiveStage,
}
