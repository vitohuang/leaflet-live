const _ = require('lodash');
const debug = require('debug')('live-kafka-superconsumer');
const SizedArray = require('../../utils/SizedArray.js');
const kafka = require('./index.js');
let Consumer;

const KafkaUtil = require('./utils.js');

// Topic and partition
const discoveryTopic = 'DiscoveryHub';
const partition = 0;

let CACHE = null;
const historyCount = 100;
let HISTORY = {}
HISTORY[discoveryTopic] = new SizedArray(historyCount);

// Get history from the
function getHistory(topic = null) {
  if (topic) {
    return HISTORY[topic];
  } else {
    return HISTORY
  }
}

// Process stuff on the discovery topic
function processDiscoverTopic(data) {
	debug('Discover topic - sub to the new topics', data);
	// Convert the value into topics
	let topics = topicsToAdd = data.value;
	if (_.isArray(topics)) {
    debug('Discover topic - The new topics', topics)

		// Compared with the last topics, see which is new need subscribe
    if (!HISTORY[discoveryTopic].isEmpty()) {
      let latestHistory = HISTORY[discoveryTopic].latest().value;
      debug('Discover topic - latest in the history',latestHistory);
      topicsToAdd = _.difference(topics, latestHistory);
    }
    // Add the topic
    debug('Dicovery topic - Topics need to add', topicsToAdd);

    // Going to add the topics to consumer
    if (topicsToAdd.length > 0) {
      // Add the topic
      debug('Discovery topic - Topics going to be added', topicsToAdd);

      Consumer.addTopics(topicsToAdd, function(error, added) {
        if (error) {
          console.error('Discover topic - Can not add topic', error);
        } else {
          debug('Discover topic - added topic', added);
        }
      })

      // Todo also add a new connector that will pump data into db
      /*
      topicsToAdd.forEach((topic) => {
        KafkaUtil.createConnectorSink(topic);
      });
      */
    }

	}
}

// Initialse the self discovery consumer
async function init(dataProcessor, cache) {
  debug('Initialise consumer - get latest from the discover topic');
  CACHE = cache;

  // Get the latest offset in the discover topic
  let latestOffset = await kafka.getLatestOffsetInTopic(discoveryTopic, partition, true);

	// This latest is the High water offset, the real one is one less
  if (latestOffset <= 0) {
    debug('There is no offset in DiscoverHub, so start from 0')
    latestOffset = 0;
  } else {
    latestOffset -= 1;
  }

	debug('going to get from the latest offset', latestOffset);
  // Get the latest in the discover topic
  Consumer = kafka.consumer([
    {
			topic: discoveryTopic,
			offset: latestOffset,
			partition,
			//fromOffset: -1,
    },
  ],{
		fromOffset: true,
	});

	Consumer
  .on('ready', function() {
		console.log('Consumer ready!');
  })
  .on('offsetOutOfRange', function(err) {
    console.log('Consumer offset out of range', err);
  })
  .on('error', function(err) {
		console.log('There is a error with consumer in super consumer', err);
    // todo: Maybe reconnect or something
  })
  .on('message', function(data) {
    debug(`Data on the pipe ****************** super consumer - ${data.topic} > ${data.offset}`);
    try {
      // Parse key
      if (data.key instanceof Buffer) {
        data.key = data.key.toString('utf8');
      }

      // Parse value
      data.value = JSON.parse(data.value);

			// Check if this is from the discovery topic
			if (data.topic == discoveryTopic) {
				processDiscoverTopic(data, HISTORY);
			} else {
        // Process data on pipeline
        dataProcessor(data);
      }

      // Add it to a sorted set with score equal to offset
      debug('Adding data on the pipe to HISTORY');
      const key = `history:${data.topic}`;
      CACHE.zadd(key, data.offset, JSON.stringify(data));
    } catch (error) {
      console.log('cant not decode the message', error);
    }
  });

  // Return
  return {
    Consumer,
    getHistory,
    discoveryTopic,
    getTopics: kafka.getTopics,
  }
}

module.exports = init;
