const debug = require('debug')('live-kafka');
const _ = require('lodash');

const kafka = require('kafka-node');
const Producer = kafka.Producer;
const Consumer = kafka.Consumer;
const KeyedMessage = kafka.KeyedMessage;

const KafkaUtil = require('./utils.js');

// Topic and partition
const discoveryTopic = 'DiscoveryHub';
const partition = 0;

// Zookeeper connection string
const zookeeperUri = process.env.ZOOKEEPER_URI || '127.0.0.1:2181';

// Client
const client = new kafka.Client(zookeeperUri);
exports.client = client;
exports.utils = KafkaUtil;

client.once('connect', (error) => {
  debug('Connected to kafka');
})

// List topics
exports.getTopics = function() {
  return new Promise(function(resolve, reject) {
    // Check if the client is connected
    client.loadMetadataForTopics([], function(error, results) {
      if (error) {
        reject(error);
      } else {
        resolve(_.get(results, '1.metadata'));
      }
    })
  })
}

// Consumer
const consumerPayloads = [
];
const consumerOptions = {
  autoCommit: false,
  groupId: 'Web',
  encoding: 'utf8',
}
function simpleConsumer(payloads = [], options = {}) {
  payloads = _.union(consumerPayloads, payloads);
  options = _.merge(consumerOptions, options);
  return new Consumer(client, payloads, options);
}
exports.consumer = simpleConsumer;

// Producer
const producerOptions = {
  autoCommit: false,
}
exports.producer = function(options = {}) {
  options = _.extend(options,producerOptions);
  const producer = new Producer(client, options);

  // Events for producer
  producer.on('ready', function() {
    debug('Producer ready');
  });
  producer.on('error', function(err) {
    // Todo: handle the error
    console.error('Error from producer');
    console.error(err);
  })

  // Send data
  const toPipe = function(topic, key, value) {
    // todo:Make sure the topic exist before actually send to it
    // Also the topic is added to the discovery channel
    debug('Put data onto pipline', topic, key);
    return new Promise((resolve, reject) => {
      const message = new kafka.KeyedMessage(key, JSON.stringify(value));
      const payload = [{
          topic: topic,
          messages: [message]
      }];
      producer.send(payload, function(error, data) {
          if (error) {
            debug('Problem with putting data to pipe', error, typeof error);
            // Todo: see if the leader is not available error
            if (error == 'LeaderNotAvailable') {
              debug('Its leader not available');
              // Create the topic and put it on the discovery channel
            }
            // Add the newly created topic to the discovery hub if its not already on it
            reject(error);
          } else {
            debug('Data should be on the pipe now', data);
            resolve(data);
          }
      });
    });
  }

  const ensureTopicAndDiscovery = async function(topic) {
    // Make sure the topic exists
    debug('Going to ensure topic exists and its on discoveryHub');
    try {
      debug('Make sure the topic exists');
      await KafkaUtil.ensureTopicExist(producer, topic);
      // Now make sure its on the discovery hub
      data = await getLatest(discoveryTopic);
      debug('Got data from the discover hub', data);
      if (data && data.indexOf(topic) === -1) {
        // Going to put it on the discovery hub
        debug('Going to put it on the discovery hub');
      } else {
        debug('nothng to do its already on the discovery hub');
      }
    } catch (error) {
      debug('There is something wrong', error);
    }
  }

  // Return object
  return {
    producer,
    ensureTopicAndDiscovery,
    toPipe,
  }
}

// Offsets
const Offset = new kafka.Offset(client);
function getLatestOffsetInTopic (topic, partition = 0, notExistCreate = false) {
  debug('Get latest in topic - before promise');
  return new Promise(function(resolve, reject) {
    debug('going to fetch latest in - ', topic);
		Offset.fetchLatestOffsets([topic], function(error, offsets) {
			if (error) {
        debug(`Error fetching latest in topic:${topic}, offsets:${offsets}`, error);
        debug('error code and message', error.code, error.message);
        debug(typeof error);
        console.error(error);

        // Try to create the topic if the topic doesn't exists
        if (notExistCreate && error.message === "Topic(s) does not exist") {
          debug('Going to create the topic', topic);
        }
				reject(error);
        // Check if this is a topic not exists error
			} else {
        debug('Latest offset', topic, offsets);
				resolve(offsets[topic][partition]);
			}
		});
  });
}
exports.getLatestOffsetInTopic = getLatestOffsetInTopic;

// Get topic data
function getTopicData(topic, offset) {
  debug(`Going to get topic data ${topic} ${offset}`);
  return new Promise((resolve, reject) => {
    debug('Makeing a new simple consumer');
    let consumer = simpleConsumer([
      {
        topic,
        offset: 0,
        partition,
      },
    ],{
      fromOffset: true,
    });

    // Handle error
    let handleError = () => {
      consumer.close(true, () => {
        debug('GetTopicData consumer closed');
      });
    }

    debug('Going to attach all the events', consumer);
    consumer
    .on('ready', function() {
      console.log('Consumer ready!');
    })
    .on('offsetOutOfRange', function(err) {
      console.log('Consumer offset out of range', err);
      handleError();
      reject(error);
    })
    .on('error', function(error) {
      console.log('There is a error with consumer', err);
      handleError();
      reject(error);
    })
    .on('message', function(data) {
      debug(`GetLatest - ${data.topic} > ${data.offset}`);
      try {
        // Parse key
        if (data.key instanceof Buffer) {
          data.key = data.key.toString('utf8');
        }

        // Parse value
        data.value = JSON.parse(data.value);

        resolve(data.value);
      } catch (error) {
        console.log('cant not decode the message', error);
        handleError();
        reject(error);
      }
    });
  });
}

// Get the latest from a topic
async function getLatest(topic, offset = 0) {
  debug(`Going to get latest from ${topic} from ${offset}`);

  if (offset === null) {
    // Get the latest offset in the discover topic
    let offset = await getLatestOffsetInTopic(topic, partition, true);

    debug('Latest offset', offset);
    // This latest is the High water offset, the real one is one less
    if (offset <= 0) {
      debug('There is no offset in DiscoverHub, so start from 0')
      offset = 0;
      debug('There is no offset in DiscoverHub, so start from 0', offset)
    } else {
      offset -= 1;
    }
  }

	debug('going to get from the offset', offset, topic);
  try {
    data = await getTopicData(topic, offset);
    return data;
  } catch (error) {
    debug('There is something worng with getting topic data', error);
  }
}

