// Kafka
const kafka = require('./kafka');
const superConsumer = require('./kafka/superconsumer.js');
const kafkaProducer = kafka.producer();
const debug = require('debug')('live-event:Streamer');

let IO = null;

// The consumer - simply broadcast to the client
function processPipeData(data) {
  debug(`processing data - ${data.topic} > ${data.offset}`);
  debug('processing data *************************************', data);
  IO.to(data.topic).emit('message', data);
}

async function initKafka(cache) {
  // Setup the consumer pipeline
  const kafkaConsumer = await superConsumer(processPipeData, cache)

  // THIS IS THE MUST HAVE FUNCTION FOR TRANSPORTER
  let newStage = async function(name) {
    return await kafka.utils.ensureTopicExist(kafkaProducer.producer, name);
  };

  // THIS IS THE MUST HAVE FUNCTION FOR TRANSPORTER
  let putStageLive = async function(name) {
    return await kafka.utils.broadcastLiveStage(kafkaProducer, kafkaConsumer, name);
  }

  // Return
  return {
    producer: kafkaProducer,
    consumer: superConsumer,
    utils: kafka.utils,
    newStage,
    putStageLive,
  };
}

module.exports = async function init(io, cache) {
  IO = io;

  // Only have kafka as stream transporter for now
  return await initKafka(cache);
}
