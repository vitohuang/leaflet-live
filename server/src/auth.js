const passport = require('koa-passport')
const debug = require('debug')('auth')
const User = require('./models/User.js');
const cryptoUtil = require('./utils/crypto.js');

const testUserName = 'tester';
const pass = 'test123';

// Automatically create test uer
User.findOne({userName: testUserName}, function(error, testUser) {
  if (!testUser) {
    console.log('Test user did not exists; creating test uer...');
    let salt = cryptoUtil.getRandomString();
    let password = cryptoUtil.sha512(pass, salt);

    testUser = new User({
      email: 'test@test.com',
      userName: testUserName,
      salt,
      password,
      firstName: 'test',
      lastName: 'test',
    });

    testUser.save();
  }
});

passport.serializeUser(function(user, done) {
	debug('serialize user', user);
  done(null, user._id)
})

passport.deserializeUser(async function(id, done) {
  debug('deserialize user - going to find it in mongo', id);
  User.findById(id, done);
})

const LocalStrategy = require('passport-local').Strategy
passport.use(new LocalStrategy(function(userName, password, done) {
	debug('local login strategy', userName, password);
  User.findOne({
    userName,
  }, function(error, user) {
    if (error) {
      done(null, false);
    } else if (user) {
      // Check if the password is correct
      // make suer there is salt
      if (cryptoUtil.sha512(password, user.salt || cryptoUtil.getRandomString()) === user.password) {
        done(null, user);
      } else {
        done(null, false);
      }
    } else {
      done(null, false);
    }
  });
}))

// Callback url
let callbackUrl ='http://localhost:' + (process.env.PORT || 5000);
if (process.env.NODE_ENV === 'production') {
  callbackUrl = 'https://live.cluelesscycling.com/api';
}

const FacebookStrategy = require('passport-facebook').Strategy
passport.use(new FacebookStrategy({
    clientID: '122585118437157',
    clientSecret: '093e0333658f05dd66ba7cade4b3bc64',
    callbackURL: callbackUrl + '/auth/facebook/callback'
  },
  function(token, tokenSecret, profile, done) {
    // retrieve user ...
    console.log('Facebook login********************************');
    console.log(token);
    console.log(tokenSecret);
    console.log(profile);

    // Try to get it the detail from profile or get the user already created
    let userObj = {
      facebookId: profile.id,
      displayName: profile.displayName,
      firstName: profile.name.givenName,
      lastName: profile.name.familyName,
      authMethod: 'Facebook',
      data: {
        token,
        tokenSecret,
        profile,
      }
    }

    // Use searc obj, because some provider doesn't provide email
    let searchObj = {
      facebookId: userObj.facebookId,
    };

    // Get or create account
    getOrCreateAccount(searchObj, userObj, done);
  }
))

const TwitterStrategy = require('passport-twitter').Strategy
passport.use(new TwitterStrategy({
    consumerKey: 'DPljWW41ciTydLlGBc4PHHePg',
    consumerSecret: 'qLH2Botqj4m6HTowHqFwfR9rUNqrw7Ioj30ZJ1oSZH8ag6kQ6e',
    callbackURL: callbackUrl + '/auth/twitter/callback'
  },
  function(token, tokenSecret, profile, done) {
    console.log('Twitter login********************************');
    console.log(token);
    console.log(tokenSecret);
    console.log(profile);

    // Try to get it the detail from profile or get the user already created
    let userObj = {
      twitterId: profile.id,
      userName: profile.username,
      displayName: profile.displayName,
      firstName: null,
      lastName: null,
      authMethod: 'Twitter',
      data: {
        token,
        tokenSecret,
        profile,
      }
    }

    // Use searc obj, because some provider doesn't provide email
    let searchObj = {
      twitterId: userObj.twitterId,
    };
    // Get or create account
    getOrCreateAccount(searchObj, userObj, done);
  }
))

const GoogleStrategy = require('passport-google-auth').Strategy
passport.use(new GoogleStrategy({
    clientId: '697562849330-15vrhk6odb3od69o6qhm43dk7h2tpkpa.apps.googleusercontent.com',
    clientSecret: 'ngdyXVj8AkyKiyU9b0h3athY',
    callbackURL: callbackUrl + '/auth/google/callback'
  },
  function(token, tokenSecret, profile, done) {
    console.log('google login********************************');
    console.log(token);
    console.log(tokenSecret);
    console.log(profile);
    // Try to get it the detail from profile or get the user already created
    let userObj = {
      googleId: profile.id,
      email: profile.emails[0].value,
      displayName: profile.displayName,
      firstName: profile.name.givenName,
      lastName: profile.name.familyName,
      authMethod: 'google',
      data: {
        token,
        tokenSecret,
        profile,
      }
    }

    // Use searc obj, because some provider doesn't provide email
    let searchObj = {
      googleId: userObj.googleId,
    };

    // Get or create account
    getOrCreateAccount(searchObj, userObj, done);
  }
))

function getOrCreateAccount(searchObj, userObj, done) {
  // Try to get it from email
  User.findOne(searchObj, function(error, user) {
    if (error) {
      console.log('there is error at get or create user', error);
      done(error);
    } else if (user) {
      console.log('update user');
      // Found the user - update it with the latest detail
      user.displayName = userObj.displayName;
      user.firstName = userObj.firstName;
      user.lastName = userObj.lastName;
      user.data = userObj.data;

      // Save it
      user.save(done);
    } else {
      console.log('goiing to create the user');
      // There is no user
      // Create one
      user = new User(userObj);

      user.save(done);
    }
  });
}

module.exports = passport;
