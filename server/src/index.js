const Koa = require('koa');
const session = require('koa-session');
const redisStore = require('koa-redis');
const koaBody = require('koa-body');
const serve = require('koa-static');

// Create a new app
const app = new Koa();

// Add on socket io
var server = require('http').createServer(app.callback());
var io = require('socket.io')(server);

// Streamer
const Streamer = require('./streamer/index.js');
const cache = require('./cache/');
const webSocket = require('./webSocket/')(io, cache);
const middlewares = require('./middlewares/');
const passport = require('./auth');

// Use all the routes
const allRoutes = require('./routes/index.js');

// Public directory
const publicDir = __dirname + '/../static';

// Settings
const port = process.env.PORT || 5000;

app.use(serve(publicDir));

// Keys
app.keys = ['im a newer secret', 'i like turtle'];
app.use(session({
  store: redisStore({
    host: process.env.REDIS.split(':')[0],
  }),
}, app));

// Body allow upload file
app.use(koaBody({
    multipart: true,
    formidable: {
        keepExtensions: true,
        uploadDir: publicDir + '/uploads',
    }
}));

// Authentication
app.use(passport.initialize());
app.use(passport.session())

// Add on the middlewares
app.use(middlewares.errors);
app.use(middlewares.headers);

// Public Routes
app.use(allRoutes.getPublic(passport));
app.use(allRoutes.allowedMethods());

// Require authentication for now
app.use(middlewares.requireAuth);

// Set up the streamer
Streamer(io, cache)
  .then((streamer) => {
    // Use the private routes
    app.use(allRoutes.getPrivate(passport, streamer));
  })

// Log error
app.on('error', (err, ctx) => {
	console.error(err);
});

server.listen(port, (ctx) => {
	console.log(`Listening on port ${port}`);
});
