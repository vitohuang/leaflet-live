const _ = require('lodash');
const slugify = require('slugify')
const debug = require('debug')('live-router-streamer');

const utils = require('../utils/streamer.js');

// Global producer
let Producer = null;
let Consumer = null;

// Get
async function discoveryTopics(ctx, next) {
	// Response data
	let resData = {
		success: true,
	};

  if (Consumer) {
    resData['data'] = Consumer.getHistory(Consumer.discoveryTopic);
  }

  ctx.body = resData;
}

async function ensureTopicAndDiscovery(ctx, next) {

 // Response data
	let resData = {
    msg: 'This is the message',
		success: true,
	};
  debug('route ensure topic and discovery', ctx.params.topic);

  debug(Producer.ensureTopicAndDiscovery);
 const result = await Producer.ensureTopicAndDiscovery(ctx.params.topic);

  debug('going to return it');
  ctx.body = resData;
}

// Topics
async function topics(ctx, next) {
	// Response data
	let resData = {
		success: true,
	};

  if (Consumer) {
    resData['data'] = await Consumer.getTopics();
  }

  ctx.body = resData;
}

// History
async function history(ctx, next) {
	// Response data
	let resData = {
		success: true,
	};

  if (Consumer) {
    resData['data'] = Consumer.getHistory();
  }

  ctx.body = resData;
}

// Add new object to streamer
async function toTopic(ctx, next) {
	debug('request body', ctx.request.body);

	// Response data
	let resData = {
		success: false,
	};

	let rawInput = ctx.request.body;
	if (rawInput) {
		try {
			input = JSON.parse(rawInput);
		} catch (err) {
			resData.success = false;
			resData.error = 'Invalid JSON';
		}

    // Todo check see if the topic exist or not
		debug('input', typeof input, input);
		if (input && input.topic && input.type && input.payload) {
			try {
        // Add in the type and topic into the payload itself
        if (typeof input.payload === 'object') {
          input.payload["_topic"] = input.topic;
          input.payload["_type"] = input.type;
        }

				debug('going try to send input object');
        const result = await Producer.toPipe(input.topic, input.type, input.payload);

        // Data should be on the pipe now { '5cab0e6911e9f70043234987_TianHe-Loop': { '0': 6 } }
        // Do the normal after processing - like a connector/consumer will do
        const offset = Object.values(Object.values(result)[0])[0];
        await utils.processStreamData({
          offset,
          value: input.payload,
          topic: input.topic,
          key: input.type,
        });

        resData.success = true;
        debug('Sent:', input, result);
			} catch (err) {
				console.error('A problem occurred when sending our message');
				console.error(err);
				resData.error = err;
			}
		}
	}

	// Send it to streamer
	ctx.body = resData;
}

module.exports = function(streamer) {
  // Get the producer
  Producer = streamer.producer;
  Consumer = streamer.consumer;

  return {
    discoveryTopics,
    history,
    topics,
    toTopic,
    ensureTopicAndDiscovery,
  }
}
