const _ = require('lodash');
const debug = require('debug')('live-router-home');
const path = require('path');
const StageTopics = require('../models/StageTopics.js');

// todo:move into authorised route
const geoUtils = require('../utils/elevation.js');
const aws = require('../utils/aws.js');
async function lonlatToElevation(ctx, next) {
  debug('get elevation', ctx.request);

  body = ctx.request.body;
  debug('body', body, typeof body);
  if (body) {
    try {
      const coordinates = JSON.parse(body);
      debug('the coordinate after parse the body', coordinates);
      let results = await geoUtils.getElevation(geoUtils.flipCoordinate(coordinates));
      ctx.body = {
        success: true,
        data: geoUtils.flipCoordinate(results),
      };
    } catch (error) {
      ctx.body = {
        success: false,
        msg: 'Sorry can not decode it',
        error: error.message,
      }
    }
  } else {
    ctx.body = {
      success: false,
      msg: 'Please provide a coordinate in body',
    }
  }
}

// Allow others to upload file
async function uploadFile (ctx, next) {
	ctx.set('Access-Control-Allow-Origin', '*');

    // Getting the body
    const body = ctx.request.body;

    if (body.files) {
        // Uploaded
        let uploaded = [];
        const files = body.files;
        for (const key in files) {

            // Push to the output
            uploaded.push({
                name: files[key].name,
                link: '/uploads/'+ path.basename(files[key].path),
                size: files[key].size,
                type: files[key].type,
            })
        }

        ctx.body = {
            success: true,
            files: uploaded,
        }
    } else {
        ctx.body = {
            success: false,
            msg: 'Please upload a file',
        }
    }
}

// Sign s3 request
const acceptedMIMETypes = [
  'image/jpeg',
  'image/pjpeg',
  'image/png',
  'image/gif',
  'image/x-ms-bmp',
  'image/tiff',
  'image/x-tiff',
  'application/pdf',
  'application/zip',
  'application/gzip',
  'application/x-gzip',
  'application/x-tar',
];
async function signS3(ctx) {
  // Set CORS
	ctx.set('Access-Control-Allow-Origin', '*');

  // Get a signed s3, so the browser can upload image directly to S3
  const s3Key = [
    ctx.state.user.username,
  ];

  const {
    fileName,
    fileType,
    filePath,
  } = ctx.request.query;

  // Only accept certain mime type
  if (acceptedMIMETypes.indexOf(fileType) === -1) {
    ctx.body = {
      success: false,
      msg: 'File type not allow',
    };
  } else {
    debug('signS3', {
      fileName,
      fileType,
    });

    // Log
    if (filePath) {
      s3Key.push(filePath);
    }

    // Add time to the file name
    s3Key.push(`${+ new Date()}_${fileName}`);
    const data = await aws.signS3(s3Key.join('/'), fileType);

    ctx.body = {
      success: true,
      data,
    };
  }
}

// Get things off the stage topics
async function stageTopics(ctx) {
  const topicName = ctx.params.topicName;

  if (topicName) {
    try {
      let docs = await StageTopics.find({
        "_topic": topicName,
      });

      console.log('to json', docs);
      ctx.body = {
        success: true,
        data: docs,
      };
    } catch (error) {
      ctx.body = {
        success: false,
        msg: 'There is something wrong - ' + error.message,
      };
    }
  } else {
    ctx.body = {
      success: false,
      msg: 'Please specify a stage name',
    };
  }
}

async function index(ctx) {
  ctx.body = {
    success: true,
    msg: 'Welcome to live!',
  }
}

module.exports = {
  index,
  uploadFile,
  lonlatToElevation,
  stageTopics,
	signS3,
}
