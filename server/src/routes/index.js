// Router
const Router = require('koa-router');

// Import other routes
const Coll = require('./coll.js');
const StreamerRoutes  = require('./streamer.js');
const Riders = require('./riders.js');
const Home = require('./home.js');
const User = require('./user.js');

// Public routes
function getPublic (passport) {
  let router = new Router();

  // Index
  router.get('/', Home.index);

  router.get('/stage/:topicName', Home.stageTopics);

  router.get('/something', function(ctx, next) {
    console.log('This is the something');
    console.log(ctx.request);
    console.log('url - ', ctx.request.url);
    console.log('originalUrl - ', ctx.request.originalUrl);
    console.log('origin - ', ctx.request.origin);
    console.log('href - ', ctx.request.href);
  })

  // Search riders
  router.get('/riders', Riders.getRiders);
  router.get('/rider/:handle', Riders.getRider);

  // Collections for Race/Stage/Result etc
  let coll = Coll();
  router.get('/coll/:type', coll.get);
  router.get('/coll/:type/:id', coll.getById);

  // Login
  router.post('/login', passport.authenticate('local', {
    failureRedirect: '/failed-login',
  }), User.getUserState);

  router.get('/failed-login', User.failedLogin);
  router.get('/logout', User.logout);

  // Google
  router.get('/auth/google', async function(ctx, next) {
    ctx.session.authReferer = ctx.request.header.referer;
    console.log('going to forward to the authentication');
    await passport.authenticate('google')(ctx, next);
  });
  router.get('/auth/google/callback', passport.authenticate('google', {
    failureRedirect: '/failed-login',
  }), User.getUserState);

  // Facebook
  router.get('/auth/facebook', async function(ctx, next) {
    ctx.session.authReferer = ctx.request.header.referer;
    console.log('going to forward to the authentication');
    await passport.authenticate('facebook')(ctx, next);
  });
  router.get('/auth/facebook/callback', passport.authenticate('facebook', {
    failureRedirect: '/failed-login',
  }), User.getUserState);

  router.get('/auth/twitter', async function(ctx, next) {
    ctx.session.authReferer = ctx.request.header.referer;
    console.log('going to forward to the authentication', ctx.session);
    await passport.authenticate('twitter')(ctx, next);
    console.log('this is afte rthe authentite');
  });
  router.get('/auth/twitter/callback', passport.authenticate('twitter', {
    failureRedirect: '/failed-login',
  }), User.getUserState);

  // User
  router.get('/user/:userName', User.get);
  router.put('/user/:userName', User.put);

  // Return all routes
  return router.routes();
}

// Private routes need authentication
function getPrivate (passport, streamer) {
  let router = new Router();

  // Allow others to upload file
  router.post('/file', Home.uploadFile);
  router.post('/signS3', Home.signS3);

  // Get elevation from lonlat
  router.post('/geo/elevation', Home.lonlatToElevation);

  // Collection base routes
  let coll = Coll(streamer);
  // Middleware to check if the authenticated user allow to make changes
  router.post('/coll/:type', coll.post);
  router.put('/coll/:type', coll.checkAuthorisation, coll.put);
  router.delete('/coll/:type/:id', coll.checkAuthorisation, coll.remove);

  // Streamer routes
  let sr = StreamerRoutes(streamer);
  router.post('/object', sr.toTopic);
  router.get('/streamer/discovery', sr.discoveryTopics);
  router.get('/streamer/history', sr.history);
  router.get('/streamer/topics', sr.topics);
  router.get('/streamer/ensureTopicAndDiscovery/:topic', sr.ensureTopicAndDiscovery);

  // Return me
  router.get('/me', User.getUserState);

  // Return all routers
  return router.routes();
}

// Respose to OPTIONS
function allowedMethods () {
  let router = new Router();
  return router.allowedMethods();
}

module.exports = {
  getPublic,
  getPrivate,
  allowedMethods,
}
