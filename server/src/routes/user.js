const _ = require('lodash');
const debug = require('debug')('live-router-user');
const models = require('../models/');
const Race = models.Race;
const User = models.User;

// Pick user
function pickUserProps(user) {
  return _.pick(user, ['_id', 'displayName', 'firstName', 'lastName', 'userName', 'email', 'avatar', 'facebookId', 'twitterId', 'googleId']);
}

// Getuser
async function get(ctx, next) {
  const userName = ctx.params.userName;

  try {
    const user = await User.findOne({
      userName,
    });

    if (user) {
      // Find the races as well
      const races = await Race.find({
        owner: user._id,
      });

      console.log('races', races);
      let data = pickUserProps(user);

      // Add the race
      data['races'] = races;
      // Get the user and its races associate with it
      ctx.body = {
        success: true,
        data,
      };
    } else {
      ctx.body = {
        success: false,
        msg: 'Sorry user not exist',
      }
    }
  } catch (error) {
    ctx.body = {
      success: false,
      msg: error.message,
    }
  }
}

async function getUserState(ctx, next) {
  console.log('get user state', ctx.session);
  if (ctx.session.authReferer) {
    // Redirect it back to the referer
    let referer = ctx.session.authReferer;
    ctx.session.authReferer = '';

    ctx.redirect(referer);
  } else {
    ctx.body = {
      success: true,
      data: pickUserProps(ctx.state.user),
    };
  }
}

async function put(ctx, next) {
  const userName = ctx.params.userName;
  const input = ctx.request.body;

  // Try to convert the body into json if its a string
  if (typeof input == 'string') {
    input = JSON.parse(input);
  }

  // todo: Filter to certain input first

  // Find the user
  let user = await User.findOne({
    userName,
  });

  // Check if the user exist or not
  if (user) {
    // Authorisation check - make sure the user is said who he/she is
    if (user._id === ctx.state.user._id) {
      // Update it with input
      user = _.extend(user, input);

      // Save it
      updated = await user.save();

      ctx.body = {
        success: true,
        updated: true,
        data: updated,
      }
    } else {
      ctx.body = {
        success: false,
        msg: 'Sorry you not authorise for this action',
      }
    }
  } else {
    // Create one
    let salt = cryptoUtil.getRandomString();
    let password = cryptoUtil.sha512(input.password, salt);

    // Make a new user
    let newUser = new User({
      email: input.email,
      userName: input.userName,
      salt,
      password,
      firstName: input.firstName,
      lastName: input.lastName,
    });

    let saved = await newUser.save();

    // Return data
    ctx.body = {
      success: true,
      saved: true,
      data: saved,
    };
  }
}

// Logout
async function logout(ctx, next) {
  // Logout
  ctx.logout()

  // Let others know
  ctx.body = {
    success: true,
    msg: 'Logout successfully!'
  };
}

async function failedLogin(ctx, next) {
  ctx.body = {
    success: false,
    msg: 'Sorry login failed, please login at /login',
  };
}

module.exports = {
  get,
  put,
  getUserState,
  logout,
  failedLogin,
}

