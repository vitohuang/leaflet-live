const _ = require('lodash');
const debug = require('debug')('live-router-riders');

// Models
const models = require('../models/');
const Rider = models.Rider;

// Mock data
const fakeAvatars = [
	'http://www.material-ui.com/images/kolage-128.jpg',
	'http://www.material-ui.com/images/kerem-128.jpg',
	'http://www.material-ui.com/images/jsa-128.jpg',
	'http://www.material-ui.com/images/adhamdannaway-128.jpg',
	'http://www.material-ui.com/images/angelceballos-128.jpg'
];
const fakeAges = [25, 26, 27,28, 29, 30, 31, 32, 33, 34, 35, 36, 27];

const fakeTeams = ['Sky', 'Astana Pro', 'AG2R La mondiale', 'Bahrain-Merida', 'BMC Racing', 'Movistar', 'Orica-Scott', 'Trek-Segafredo'];
const fakeCountries = [
  {
    name: 'United Kingdom',
    icon: 'https://raw.githubusercontent.com/gosquared/flags/master/flags/flags-iso/flat/64/GB.png',
  },
  {
    name: 'France',
    icon: 'https://raw.githubusercontent.com/gosquared/flags/master/flags/flags-iso/flat/64/FR.png',
  },
  {
    name: 'Spain',
    icon: 'https://raw.githubusercontent.com/gosquared/flags/master/flags/flags-iso/flat/64/ES.png',
  },
  {
    name: 'Italy',
    icon: 'https://raw.githubusercontent.com/gosquared/flags/master/flags/flags-iso/flat/64/IT.png',
  },
  {
    name: 'Germany',
    icon: 'https://raw.githubusercontent.com/gosquared/flags/master/flags/flags-iso/flat/64/DE.png',
  }
];

// Get riders
async function getRiders(ctx, next) {
  debug('search rider', ctx.request, ctx.request.query);

  query = ctx.request.query;

  if (query.q) {
    const riders = await Rider.find({
      $text: {
        $search: query.q,
      }
    })
			.select({ "score": { "$meta": "textScore" } })
			.sort({ "score": { "$meta": "textScore" } })
      .limit(25)

		// Map the riders to get certain things
		ctx.body = riders.map(function(rider) {
			return {
				id: rider._id,
				name: rider.name,
				firstName: rider.name_first,
				lastName: rider.name_last,
				handle: rider.handle,
				avatar: _.sample(fakeAvatars),
        team: _.sample(fakeTeams),
        age: _.sample(fakeAges),
			}
		});
  } else {
    ctx.body = {
      success: false,
      msg: 'Please provide a search query',
    }
  }
}

// Get rider
async function getRider(ctx, next) {
  // Get the handle
  handle = ctx.params.handle;

  // Check if there is an handle or not
  if (handle) {
    const rider = await Rider.findOne({
      handle,
    });

    if (rider) {
      riderObj = rider.toObject();
      riderObj['team'] = _.sample(fakeTeams);
      riderObj['age'] = _.sample(fakeAges);
      riderObj['avatar'] = _.sample(fakeAvatars);
      riderObj['country'] = _.sample(fakeCountries);

      debug('rider', rider);
      ctx.body = {
        success: true,
        data: _.pick(riderObj, ['name', 'handle', 'team', 'age', 'avatar', 'country']),
      };
    } else {
      ctx.body = {
        success: false,
        msg: `Can not find the rider you looking for - ${handle}`,
      };
    }
  } else {
    ctx.body = {
      success: false,
      msg: 'Please provide a valid handle',
    };
  }
}

module.exports = {
  getRider,
  getRiders,
}
