const _ = require('lodash');
const slugify = require('slugify')
const debug = require('debug')('live-router-coll');

// Models
const models = require('../models/');
const Rider = models.Rider;
const Stage = models.Stage;
const Race = models.Race;
const Result = models.Result;

// Global producer
let streamer = null;

// Convert stage into topic name
function topicNameFromStage(stage) {
  return `${stage._id}_${stage.slug}`;
}

// Get collection by type
function getCollectionByType(type) {
  // Get collection
  let collection = null;
  switch(type) {
    case 'race':
      collection = Race;
      break;
    case 'stage':
      collection = Stage;
      break;
    case 'result':
      collection = Result;
      break;
  }

  return collection;
}

// Get collection by type
function newCollectionByType(type, data) {
  // Get collection
  let collection = null;
  switch(type) {
    case 'race':
      collection = new Race(data);
      break;
    case 'stage':
      collection = new Stage(data);
      break;
    case 'result':
      collection = new Result(data);
      break;
  }

  return collection;
}

// Get
async function get(ctx, next) {
  debug('going to get new thing', ctx.request.query);
  debug("and type", ctx.params);

  const type = ctx.params.type;
	let input = ctx.request.query;
  //todo: sanitize the input
	if (input && type) {
      // Save it
      let collection = getCollectionByType(type);

      try {
        let doc = false;

        // Check if need to get draft only or not
        let draft = {};

        // If its login user, then only get the race that's belong to him/her
        if (ctx.state.user) {
          input['owner'] = String(ctx.state.user._id);
        } else {
          // Not get the draft
          draft = {
            status: {
              $ne: 'draft',
            },
          };
        }

        debug('the input to get', input);
        // Fetch multiple version or just one
        if (input.multi) {
          input = Object.assign(input, draft);

          // Find the collection
          doc = await collection.find(_.omit(input, ['multi', 'owner'])).populate('owner', 'userName');
        } else {
          doc = await collection.findOne(input).populate('owner', 'userName');
        }

        // If its race, add the stages belong to that race
        if (['race', 'stage'].indexOf(type) != -1) {
          if (doc.toJSON) {
            doc = doc.toJSON();

            if (type == 'race') {
              // Race will have stages
              doc['stages'] = [];

              const stageQuery = Object.assign({
                raceId: doc._id,
              }, draft);

              // Get all the stages
              let stages = await Stage.find(stageQuery);

              if (stages) {
                doc['stages'] = stages;
              }
            }

            // Find the results as well
            doc['results'] = []
            let results = await Result.find({
              relationType: type,
              relationId: doc._id,
            });

            if (results) {
              doc['results'] = results;
            }
          }
         }

        ctx.body = {
          success: true,
          data: doc,
        };
      } catch (error) {
        ctx.body = {
          success: false,
          msg: 'There is something wrong - ' + error.message,
        };
      }
  } else {
    ctx.body = {
      success: false,
      msg: 'Please supply the correct params',
    };
  }
}

async function getById(ctx, next) {
  debug('going to get new thing', ctx.request.query);
  debug("and type", ctx.params);

  const type = ctx.params.type;
  const id = ctx.params.id;
  //todo: sanitize the input
	if (type && id) {
      // Save it
      let collection = getCollectionByType(type);

      try {
        let doc = false;
        doc = await collection.findById(id);

        // If its race, add the stages belong to that race
        if (['race', 'stage'].indexOf(type) != -1) {
          if (doc.toJSON) {
            doc = doc.toJSON();

            if (type == 'race') {
              // Race will have stages
              doc['stages'] = [];

              // Get all the stages
              let stages = await Stage.find({
                raceId: doc._id,
              });

              if (stages) {
                doc['stages'] = stages;
              }
            }

            // Find the results as well
            doc['results'] = []
            let results = await Result.find({
              relationType: type,
              relationId: doc._id,
            });

            if (results) {
              doc['results'] = results;
            }
          }
         }

        ctx.body = {
          sucess: true,
          data: doc,
        };
      } catch (error) {
        ctx.body = {
          success: false,
          msg: 'There is something wrong - ' + error.message,
        };
      }
  } else {
    ctx.body = {
      success: false,
      msg: 'Please supply the correct params',
    };
  }
}

// Add new one
async function post(ctx, next) {
  debug('going to add new thing', ctx.request.body);
  debug("add type", ctx.params);

  const type = ctx.params.type;
	let input = ctx.request.body;
  // Try to convert the body into json if its a string
  if (typeof input == 'string') {
    input = JSON.parse(input);
  }
  debug('the input', input, input.name, type);
  if (input && input.name && type) {
    input.slug = slugify(input.name);

    // Put the current user as owner
    input.owner = ctx.state.user._id;

    // Save it
    let collection = newCollectionByType(type, input);

    try {
      let saved = await collection.save();

      switch (type) {
        case 'stage':
          debug("going to create topic for the stage");
          // Create a new topic if its creating new stage
          let topic = await streamer.newStage(topicNameFromStage(saved));

          // Only send the topic when its actually live
          break;
        default:
      }

      ctx.body = {
        success: true,
        saved: true,
        data: saved,
      };
    } catch (error) {
      debug("There is an error", error);
      ctx.body = {
        success: false,
        msg: 'Something wrong when creating - ' + error.message,
      };
    }
  } else {
    ctx.body = {
      success: false,
      msg: 'Please supply the correct params',
    };
  }
}

// Update/replace
async function put(ctx, next) {
  debug('going to add new thing', ctx.request.body);
  debug("add type", ctx.params);

  const type = ctx.params.type;
	let input = ctx.request.body;

  // Try to convert the body into json if its a string
  if (typeof input == 'string') {
    input = JSON.parse(input);
  }
  debug("input and type", input, type, typeof input);
	if ((input.name || input._id) && type) {
    debug("have name and type, going to save");
    let updated = false;
    let existing = null;

    // get it it
    let collection = getCollectionByType(type);

    // Check if there is already an id
		if (input._id) {

      debug('update existing one');
      existing = await collection.findOne({_id: input._id});

      // Update it if its already exist
      if (existing) {
        try {
          existing = _.extend(existing, input);
          updated = await existing.save();
          debug('updated', updated);

          // Check for the status
          // Send the topic to discover topic if needed
          if (type == 'stage' && input.status !== 'draft') {
            debug('Going to broadcast stage to discover topics', topicNameFromStage(updated));
            // Make sure the topic exists
            let ret = await streamer.newStage(topicNameFromStage(updated));

            // Then broadcast it
            await streamer.putStageLive(topicNameFromStage(updated));
          }

          ctx.body = {
            success: true,
            updated: true,
            data: updated,
          }
        } catch (error) {
          debug('there is error saving', error);
          ctx.body = {
            success: false,
            msg: 'Somethine wrong when saving it - ' + error.message,
          }
        }
      }
    }
  } else {
    ctx.body = {
      success: false,
      msg: 'Please supply the correct params',
    }
  }
}

// Delete
async function remove(ctx, next) {
  debug("going to delete", ctx.params);

  const type = ctx.params.type;
  const id = ctx.params.id;
  debug(`type:${type} > id: ${id}`);

	if (type && id) {
    // Get collection
    let collection = getCollectionByType(type);

    try {
      let deleted = await collection.findByIdAndRemove(id);

      debug('deleted', deleted);
      ctx.body = {
        success: true,
        data: {
          _id: id,
        },
      };
    } catch (error) {
      debug("There is an error", error);
      ctx.body = {
        success: false,
        msg: 'Sorry something went wrong, can not delete it:' + error.message,
        data: {
          id,
          type,
        },
      };
    }
  } else {
    ctx.body = {
      success: false,
      msg: 'Please supply the params',
    }
  }
}

async function checkAuthorisation(ctx, next) {
  const type = ctx.params.type;
  let id = ctx.params.id;
  let input = ctx.request.body;

  debug('check authorisation');

  // Only check for race and stages
  if (['race','stage'].indexOf(type.toLowerCase()) !== -1) {
    // If there is no id in the param
    // then check the input
    if (_.isEmpty(id)) {
      if (typeof input === 'string') {
        input = JSON.parse(input);
      }

      if (input && input._id) {
        id = input._id;
      }
    }

    debug(`type:${type} > id: ${id}`);

    // If there is type and id
    // then get it
    if (type && id) {
      let collection = getCollectionByType(type);

      let coll = await collection.findById(id);

      // Check if the collection owner is really the authenticate user
      let currentUserId = ctx.state.user._id;

      // Check if the owner is current logged in user
      if (String(coll.owner) === String(currentUserId)) {
        // Go for next one
        await next();
      } else {
        debug('not included');
        ctx.throw(401, 'Not allow', {
          body: {
            success: false,
            msg: 'Not authorise for the action',
          },
        });
      }
    } else {
      ctx.throw(401, 'Fail authorisation');
    }
  } else {
    await next();
  }
}

module.exports = function(incomingStreamer) {
  // Get the incoming streamer to the streamer
  if (incomingStreamer) {
    streamer = incomingStreamer;
  }

  return {
    get,
    getById,
    post,
    put,
    remove,
    checkAuthorisation,
  }
}
