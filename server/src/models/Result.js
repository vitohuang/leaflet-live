const mongoose = require('mongoose');

const resultSchema = mongoose.Schema({
    type: String, // Rider time, Rider point, Team time, Team points, none,
    relationType: String,
    relationId: mongoose.Schema.Types.ObjectId,
    name: String,
    slug: {
      type: String,
      required: true,
    },
    description: String,
    sortOrder: Number,
    generalClassification: Boolean,
    entries: [],
}, {
    timestamps: {
      createdAt: 'createdAt',
      updatedAt: 'updatedAt',
    },
});

const Result = mongoose.model('Result', resultSchema);

module.exports = Result;
