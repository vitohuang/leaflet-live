const mongoose = require('mongoose')

const userRoleSchema = mongoose.Schema({
    userId: {
        type: mongoose.schema.types.objectid,
        ref: 'user',
    },
    roleId: {
        type: mongoose.schema.types.objectid,
        ref: 'role',
    },
}, {
    timestamps: {
      createdAt: 'createdAt',
      updatedAt: 'updatedAt',
    },
});

const UserRole = mongoose.model('UserRole', userRoleSchema);

module.exports = UserRole;
