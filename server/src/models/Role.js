const mongoose = require('mongoose')

const roleSchema = mongoose.Schema({
  name: String,
  description: String,
}, {
    timestamps: {
      createdAt: 'createdAt',
      updatedAt: 'updatedAt',
    },
});

const Role = mongoose.model('Role', roleSchema);

module.exports = Role;
