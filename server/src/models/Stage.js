const mongoose = require('mongoose');

// Stage schema
const stageSchema = mongoose.Schema({
    name: String,
    slug: {
      type: String,
      required: true,
    },
    startDate: {
        type: Date,
    }, // Handle timezone
    distance: Number,
    coverImageUrl: String,
    description: String,
    raceType: String,
    courseGeo: mongoose.Schema.Types.Mixed,
    situations: mongoose.Schema.Types.Mixed,
    startList: [],
    startLocation: {
        name: String,
        latLon: mongoose.Schema.Types.Mixed,
    },
    endLocation: {
        name: String,
        latLon: mongoose.Schema.Types.Mixed,
    },
    raceId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Race',
    },
    status: String,
    sortOrder: Number,
    owner: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'User',
    },
}, {
    timestamps: {
      createdAt: 'createdAt',
      updatedAt: 'updatedAt',
    },
});

// Make a model
const Stage = mongoose.model('Stage', stageSchema);

module.exports = Stage;
