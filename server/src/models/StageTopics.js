const mongoose = require('mongoose');

const conn = mongoose.createConnection(process.env.MONGODB_KAFKA_URI || 'mongodb://mongo/kafka');

const StageTopics = conn.model('StageTopics', new mongoose.Schema({
    id: Number,
    topic: String,
    value: mongoose.Schema.Types.Mixed,
    offset: Number,
    partition: Number,
    highWaterOffset: Number,
    key: mongoose.Schema.Types.Mixed,
}, {
    timestamps: {
      createdAt: 'createdAt',
      updatedAt: 'updatedAt',
    },
}), 'stageTopics');

module.exports = StageTopics;
