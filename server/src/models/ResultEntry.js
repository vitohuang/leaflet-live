const mongoose = require('mongoose');

const resultEntrySchema = mongoose.Schema({
    id: Number,
    resultId: mongoose.Schema.Types.ObjectId,
    riderId: mongoose.Schema.Types.ObjectId,
    teamId: mongoose.Schema.Types.ObjectId,
    position: Number,
    time: String,
    timeGap: String,
    points: Number,
    averageSpeed: Number,
    sortOrder: Number,
    comment: String,
}, {
    timestamps: {
      createdAt: 'createdAt',
      updatedAt: 'updatedAt',
    },
});

const ResultEntry = mongoose.model('ResultEntry', resultEntrySchema);

module.exports = ResultEntry;
