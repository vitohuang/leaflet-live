const mongoose = require('mongoose');

const kafkaDataSchema = mongoose.Schema({
    id: Number,
    topic: String,
    value: mongoose.Schema.Types.Mixed,
    offset: Number,
    partition: Number,
    highWaterOffset: Number,
    key: mongoose.Schema.Types.Mixed,
}, {
    timestamps: {
      createdAt: 'createdAt',
      updatedAt: 'updatedAt',
    },
});

// Text search for the index
kafkaDataSchema.index({
  topic: 'text',
});

// Make a model
const KafkaData = mongoose.model('KafkaData', kafkaDataSchema);

module.exports = KafkaData;
