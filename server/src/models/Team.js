const mongoose = require('mongoose');

// Team schema
const teamSchema = mongoose.Schema({
    id: Number,
    name: String,
    slug: String,
    type: String, // uci worldtour, natinal etc
    country: String,
    discipline: String, // Road, mtb, offroad, time trial
    shortName: String,
    bikes: String,
    sponsors: [],
    summary: String,
    description: String,
    url: String,
    email: String,
    tel: String,
    fax: String,
    address: String,
}, {
    timestamps: {
      createdAt: 'createdAt',
      updatedAt: 'updatedAt',
    },
});

// Text index for search
teamSchema.index({
  name: 'text',
});

const Team = mongoose.model('Team', teamSchema);

module.exports = Team;
