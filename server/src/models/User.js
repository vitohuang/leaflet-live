const mongoose = require('mongoose')

const userSchema = mongoose.Schema({
	email: String,
  userName: String,
	firstName: String,
	lastName: String,
	displayName: String,
	avatar: String,
  salt: String,
  password: String,
  facebookId: String,
  twitterId: String,
  instagramId: String,
  googleId: String,
  data: mongoose.Schema.Types.Mixed,
}, {
    timestamps: {
      createdAt: 'createdAt',
      updatedAt: 'updatedAt',
    },
});

const User = mongoose.model('User', userSchema);

module.exports = User;
