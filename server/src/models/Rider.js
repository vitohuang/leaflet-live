const mongoose = require('mongoose');

// Rider schema
const riderSchema = mongoose.Schema({
    id: Number,
    handle: String,
    summary: String,
    description: String,
    name: String,
    name_first: String,
    name_last: String,
    uci_id: String,
    ig_id: String,
    country: mongoose.Schema.Types.Mixed,
    birth_place: String,
    date_of_birth: String,
    date_of_death: String,
    years_active: String,
    gender: String,
    height: Number,
    weight: Number,
    website: String,
    facebook: String,
    twitter: String,
    instagram: String,
    strava: String,
    status: String,
    created: mongoose.Schema.Types.Mixed,
    modified: mongoose.Schema.Types.Mixed,
    types: [],
    disciplines: [],
    alias: [],
    team_seasons: [],
    _links: mongoose.Schema.Types.Mixed,
}, {
    timestamps: {
      createdAt: 'createdAt',
      updatedAt: 'updatedAt',
    },
});

// Text index for search
riderSchema.index({
  name: 'text',
  name_first: 'text',
  name_last: 'text',
});

const Rider = mongoose.model('Rider', riderSchema);

module.exports = Rider;
