const mongoose = require('mongoose');

// Connect to db
mongoose.connect(process.env.MONGODB_URI || 'mongodb://mongo/cycling');

var db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', (error, one, two) => {
    console.log('Mongo connection opened');
})

const Race = require('./Race.js');
const Stage = require('./Stage.js');
const Result = require('./Result.js');
const ResultEntry = require('./ResultEntry.js');
const Rider = require('./Rider.js');
const Team = require('./Team.js');
const User = require('./User.js');

// This is for record all data on pipe
const KafkaData = require('./kafkaData.js');

module.exports = {
    Race,
    Stage,
    Result,
    Rider,
    Team,
    KafkaData,
    User,
}
