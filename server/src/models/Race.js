const mongoose = require('mongoose');

// Race schema
const raceSchema = mongoose.Schema({
    name: String,
    slug: {
      type: String,
      required: true,
    },
    startDate: {
        type: Date,
    },
    endDate: {
        type: Date,
    },
    distance: Number,
    numberOfStages: Number,
    series: String,
    group: String,
    discipline: String,
    edition: String, // Tour de France 100 etc
    coverImageUrl: String,
    description: String,
    courseGeo: mongoose.Schema.Types.Mixed,
    startList: [],
    currentStage: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Stage',
    },
    owner: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'User',
    },
}, {
    timestamps: {
      createdAt: 'createdAt',
      updatedAt: 'updatedAt',
    },
});

const Race = mongoose.model('Race', raceSchema);

module.exports = Race;
