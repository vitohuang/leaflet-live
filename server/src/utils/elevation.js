const rp = require('request-promise');
const async = require('async');
const _ = require('lodash');

const apiKey = 'AIzaSyB7bqXQJIwOdR2dYbNVg6g7BmGyu6ICKd0';

//const openElevationEndpoint = 'https://api.open-elevation.com';
const openElevationEndpoint = 'http://18.162.55.222:8080';
// http://18.162.55.222:8080/api/v1/lookup?locations=41.161758,-8.583933
var getElevationFromOpen = function(lat, lon) {
    return new Promise((resolve, reject) => {
        rp(`${openElevationEndpoint}/api/v1/lookup?locations=${lat},${lon}`)
            .then((response) => {
                const data = JSON.parse(response);
                if (data.results.length > 0) {
                    console.log(`Got elevation for ${lat},${lon} -> ${data.results[0].elevation}`);
                    resolve(data.results[0].elevation);
                }
            })
            .catch((err) => {
                reject(err);
            })
    });
}

var getElevationFromGoogle = function(lat, lon) {
    return new Promise((resolve, reject) => {
        rp(`https://maps.googleapis.com/maps/api/elevation/json?locations=${lat},${lon}&key=${apiKey}`)
            .then((response, one, two, three) => {
                const data = JSON.parse(response);
                if (data.status == 'OK' || data.results.length > 0) {
                    resolve(data.results[0].elevation);
                }
            })
            .catch((err) => {
                reject(err);
            })
    });
}


function getElevations(latLons) {
    return new Promise((resolve, reject) => {
        async.map(latLons, (latLon, cb) => {
            // Get elevation
            getElevationFromOpen(latLon[0], latLon[1])
                .then((elevation) => {
                    cb(null, latLon.concat([elevation]));

                }).catch((err) => {
                    cb(err);
                })
        }, (err, results) => {
            if (err) {
                reject(err);
            } else {
                resolve(results);
            }
        })
    })
}

// Group locations in one version
var getElevationMulti = function(pathString, samples) {
    return new Promise((resolve, reject) => {
        rp(`https://maps.googleapis.com/maps/api/elevation/json?path=${pathString}&samples=${samples}&key=${apiKey}`)
            .then((response, one, two, three) => {
                const data = JSON.parse(response);
                if (data.status == 'OK' || data.results.length > 0) {
                    resolve(data);
                }
            })
            .catch((err) => {
                reject(err);
            })
    });
}

function getElevationsMulti (elevations) {
    return new Promise((resolve, reject) => {
        async.map(_.chunk(elevations, 2), (col, cb) => {
            const samples = _.size(col);

            if (samples > 1) {
                let pathString = col.reduce((acc, item) => {
                    return acc + item.join(',') + '|';
                }, '');

                // Remove the last |
                pathString = pathString.substring(0, pathString.length - 1);

                getElevationMulti(pathString, _.size(col))
                    .then((elevations) => {
                      console.log('got elevation', elevations);
                        // Transform the elevations
                        let ret = elevations.results.map((result) => {
                            return [result.location.lat, result.location.lng, result.elevation];
                        })
                        cb(null, ret);
                    }).catch((err) => {
                      console.log('errors getElevationMulti', err);
                        cb(err);
                    })
            } else {
                const latLon = col[0];
                getElevation(latLon[0], latLon[1])
                    .then((elevation) => {
                      console.log('got elevation', elevation);
                        cb(null, [latLon.concat([elevation])]);
                    }).catch((err) => {
                      console.log('getElevation error', err)
                        cb(err);
                    })
            }
        }, (err, results) => {
          console.log('finish the mapping', err);
            if (err) {
              reject(err);
            } else {
              const r = results.reduce((acc, i) => acc.concat(i), [])
              resolve(r);
            }
        })
    })
}

async function a(latLons) {
  let results = [];
  try {
    //results = await getElevationsMulti(latLons);
    results = await getElevations(latLons);
  } catch (error) {
    console.log('there is an error while getElevationsMulti', error);
    throw error;
  }

  console.log('the results in get elevations mutil', results);
  let data = [];
  results.map((result) => {
      if (typeof result[0] == 'number') {
          data.push(result);
      } else {
          result.map((ret) => {
              data.push(ret);
          })
      }
  })

  return results;
}

function flipCoordinate(coordinates) {
  return coordinates.map(l => [l[1], l[0]].concat(l.slice(2)));
}

module.exports = {
  getElevation: a,
  flipCoordinate,
}
