const crypto = require('crypto');
const SALT_LEN = 128;

// Get random string- e.g salt
function getRandomString(length) {
  // Default to salt length
  if (!length) {
    length = SALT_LEN;
  }

  return crypto.randomBytes(Math.ceil(length/2))
          .toString('hex')
          .slice(0, length);
}

function sha512(password, salt) {
  let hash = crypto.createHmac('sha512', salt);
  hash.update(password);
  return hash.digest('hex');
}

module.exports = {
  getRandomString,
  sha512,
  SALT_LEN,
}
