const debug = require('debug')('live-aws-utils');
const _ = require('lodash');
const aws = require('aws-sdk');

// Load the config
const AWSConfig = require('../../configs/aws.js');

// Only update if there is key set in the config
if (AWSConfig.secretAccessKey) {
	aws.config.update({
		accessKeyId: AWSConfig.accessKeyId,
		secretAccessKey: AWSConfig.secretAccessKey,
		region: AWSConfig.region,
	});
}

function slugify(text) {
  return text.toString().toLowerCase()
    .replace(/\s+/g, '-')           // Replace spaces with -
    .replace(/[^\w\-]+/g, '')       // Remove all non-word chars
    .replace(/\-\-+/g, '-')         // Replace multiple - with single -
    .replace(/^-+/, '')             // Trim - from start of text
    .replace(/-+$/, '');            // Trim - from end of text
}

// Convert case etc
const S3 = new aws.S3();
const S3_BUCKET = AWSConfig.uploadBucketName;
async function signS3(Key, ContentType, ACL = 'public-read') {
  // Param for s3
  const params = {
    Bucket: S3_BUCKET,
    Key,
    Expires: 300,
    ContentType,
    ACL,
  };

  // Do a s3 putObject promise
  return new Promise((resolve, reject) => {
    S3.getSignedUrl('putObject', params, (error, data) => {
      if (error) {
        reject(error);
      } else {
        resolve({
          signedRequest: data,
          url: `https://${S3_BUCKET}.s3.amazonaws.com/${params.Key}`,
          key: params.Key,
        });
      }
    });
  });
}

module.exports = {
  signS3,
};
