const debug = require('debug')('live-router-streamer');
const StageTopics = require('../models/StageTopics.js');

async function processStreamData(data) {
  console.log('going to process stream data', data);
  // Get the data into the more managable format
  const {
    topic,
    offset,
    value,
  } = data;
  const type = data.key;
  const action = value.action || 'new';

  // Step 1 - Add to the raw data

  // Step 2 - Do things depends on the action
  switch (action) {
    case "edit":
      break;
    case "delete":
      break;
    default:
      collection = new StageTopics(data);
      let saved = await collection.save();
  }
}

module.exports = {
  processStreamData,
}
