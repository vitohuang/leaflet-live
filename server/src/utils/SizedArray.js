'use strict'

// Array of fixed size
function SizedArray(length) {
    this.maxLength = length || 10;
    this.arr = [];
}

SizedArray.prototype.push = function(item) {
    // Get array size
    var size = this.arr.filter(function(value) {
        return value !== undefined
    }).length;

    if (size >= this.maxLength) {
        this.arr.pop();
    }
    this.arr.unshift(item);
}

SizedArray.prototype.items = function() {
    return this.arr;
}

SizedArray.prototype.latest = function() {
    return this.arr[0];
}

SizedArray.prototype.oldest = function() {
    return this.arr[this.arr.length - 1];
}

SizedArray.prototype.isEmpty = function() {
    return this.arr.length === 0;
}

module.exports = SizedArray;
