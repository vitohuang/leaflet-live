const GeoUtils = require('./elevation.js');

const geoJson = {
  "type": "FeatureCollection",
  "features": [
    {
      "type": "Feature",
      "properties": {},
      "geometry": {
        "type": "LineString",
        "coordinates": [
          [
            -2.360687255859375,
            51.38292387723015
          ],
          [
            -2.5989532470703125,
            51.45315114582281
          ],
          [
            -2.50762939453125,
            51.53480425870272
          ]
        ]
      }
    }
  ]
};

const lonLat = geoJson.features[0].geometry.coordinates
function flipCoordinate(lonLat) {
  console.log('before', lonLat);
  const latLon = lonLat.map(l => [l[1], l[0]].concat(l.slice(2)));
  console.log('after', latLon);
  return latLon;
}

flipCoordinate(flipCoordinate(lonLat));

GeoUtils.getElevation(flipCoordinate(lonLat))
.then((results) => {
    console.log('results');
    console.log(results);
});
