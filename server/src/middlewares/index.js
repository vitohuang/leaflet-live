const debug = require('debug')('live-event:Middlewares');

async function headers(ctx, next) {
  debug('start the timeer');
	const start = new Date();

  // Next middleware
	await next();

  // End time
	const ms = new Date() - start;

  // Set response time
	ctx.set('x-response-time', `${ms}ms`);

  // Access control
	// Allow everyone to use it for now
	ctx.set('Access-Control-Allow-Origin', ctx.req.headers.origin || '*');
	ctx.set('Access-Control-Allow-Credentials', 'true');
	ctx.set('Access-Control-Allow-Methods','GET,POST,PUT,DELETE,OPTIONS');

  // Log the time
	debug(`[Log] ${ctx.method} ${ctx.url} - ${ms}ms`);
};

async function errors(ctx, next) {
  try {
    debug('the error handling', ctx.request.body);
    await next();
  } catch (e) {
    // Internal logging
    debug('error handing exception', e, e.status);

    // Put the status to the response
    ctx.status = e.status || 500;

    // Check if there is body or not
    if (e.body) {
      ctx.body = e.body;
    } else {
      ctx.body = {
        success: false,
        msg: e.message || '500 - something went wrong',
      }
    }
  }

  if (parseInt(ctx.status) === 404) {
    debug('This is 404');
    // Internal logging
    debug('4000000000004 going to redirect to homepage');
    ctx.body = {
      success: false,
      msg: 'Can not find the page you looking for',
    }
  }
};

async function requireAuth(ctx, next) {
  if (ctx.isAuthenticated()) {
		debug('is authenticated, going for next');
    return next()
  } else {
		debug('not authenticated');
    ctx.body = {
      success: false,
      msg: 'Authentication required',
    }
  }
}

module.exports = {
  headers,
  errors,
  requireAuth,
};
