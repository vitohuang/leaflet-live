const debug = require('debug')('live-event:Socket');
const StageTopics = require('../models/StageTopics.js');

let IO = null;
let CACHE = null;

// Figure out number of clients in the room
function getClientsInRoom(io, namespace, room) {
	var clients = io.nsps[namespace].adapter.rooms[room];
	return Object.keys(clients).length;
}

function handleData(socket, data) {
  debug("there is a data", data);
  socket.emit("back", {'one': '1'});
}

function handleAck(socket, data, fn) {
  debug('ack', data);
  // Send it back
  fn(data);
}

function handleInRoom(socket, data, fn) {
  debug('check if in the room', data, socket.rooms, typeof socket.rooms);
  // Check if the current socket is in the room or not
  let inRoom = false;
  if (data.roomId) {
    if (socket.rooms[data.roomId]) {
      debug('already in the room');
      inRoom = true;
    }
  }

  fn({
    inRoom,
  });
}

function handleMessage(socket, data, fn) {
  // Check for the room id
  if (data.rooms) {
    debug('rooms to send', data.rooms, socket.id);
    data.rooms.map((room) => {
      IO.to(room).emit('message', data.payload);
    })
    //socket.broadcast.to(data.roomId).emit('message', data.payload);
  }

  // Ack the message
  fn({
    complete: true,
  })
}

function handleRegister(socket, data, fn) {
  debug('On register', data, data.roomId, socket.id);
  const roomId = data.roomId;
  // Join the room
  socket.join(roomId, function() {
    debug(roomId, 'room joined', socket.rooms, 'end room joined', socket.id);

    // Tell everyone about the number of connections
    socket.broadcast.to(roomId).emit('register', {
      totalConnections: getClientsInRoom(IO, '/', roomId)
    });

    // Ack the action
    fn({
      complete: true,
      totalConnections: getClientsInRoom(IO, '/', roomId)
    })

    // Going to get histories from cache
    const cacheKey = `history:${roomId}`;
    CACHE.zrange(cacheKey, 0, -1)
      .then(histories => {
        histories = histories.map(d => JSON.parse(d));
        console.log(`the history for the key ************* ${cacheKey}`, histories.length);
        // Send histories
        if (histories.length > 0) {
          debug('going to send the history', data);
          socket.emit('history', histories);
        }
      })
      .catch(error => {
        console.error('Can not get histories or send it', error);
      });

    /*
    StageTopics.find({
      "topic": roomId
    }, function(error, results) {
      console.log('found inside the stage topic *****************************', results);
      if (error) {
        debug('There is an error getting topic history for the room/stage', roomId, error);
        socket.emit('history', error);
      } else {
        socket.emit('history', results);
      }
    });
    */

  });
}

function handleDisconnect(socket, data, fn) {
  debug('Socket/client discounnected');
}

function init(io, cache) {
  IO = io;
  CACHE = cache;

  // WS events
  IO.on('connection', function(socket) {
    debug("there is a connection", socket.id);

    socket.on('data', (data) => {
      handleData(socket, data);
    })

    socket.on('ack', (data, fn) => {
      handleAck(socket, data, fn);
    });

    socket.on('inRoom', (data, fn) => {
      handleInRoom(socket, data, fn);
    });

    socket.on('message', (data, fn) => {
      handleMessage(socket, data, fn);
    });

    // Register the client
    socket.on('register', (data, fn) => {
      handleRegister(socket, data, fn);
    });

    // When the socket is disconnect
    socket.on('disconnect', (data, fn) => {
      handleDisconnect(socket, data, fn);
    });
  });
}

module.exports = init;
