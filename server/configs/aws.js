require('dotenv').config({
  path: __dirname + '/../.env',
});

const env = process.env.NODE_ENV || 'development';
const Config = {
  production: {
    bucketName: 'live-cycling',
  },
  development: {
    bucketName: 'live-cycling-dev',
  },
};

module.exports = Object.assign(Config[env], {
  uploadBucketName: 'live-cycling-assets',
  accessKeyId: process.env.AWS_ACCESS_KEY_ID,
  secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY,
  region: process.env.AWS_REGION,
});
