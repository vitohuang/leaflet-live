const models = require('./models');

// Save data into db
function processPipData(data) {
  debug('going to process', data);
}

// Start a super consumer with procesisng pipe
const superConsumer = require('./kafka/superconsumer.js')(processPipData);
