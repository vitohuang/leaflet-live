const _ = require('lodash');
const readline = require('readline');
const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout
});

// Zookeeper connection string
const zookeeperUri = process.env.ZOOKEEPER_URI || '127.0.0.1:2181';

// Initialise the kafka client
var kafka = require('kafka-node'),
    Consumer = kafka.Consumer,
    client = new kafka.Client(zookeeperUri);

console.log(client);
client.once('connect', function() {
    console.log('client connected');
    client.loadMetadataForTopics([], function(error, results) {
        if (error) {
            console.log('there is error getting list of topics', error);
        } else {
            console.log('List of topics');
            let topics = _.get(results, '1.metadata');
            // Print the selection
            topics = _.mapValues(topics, function(val, key) {
                const partitions = _.keys(_.mapValues(val, function(vv, key) {
                        return vv.partition;
                    })
                );
                console.log(key, '-', partitions.join(','));
            });

            rl.question('Please select a topic to consume?\n', (selectedTopic) => {
                console.log('Going to consume from - ', selectedTopic);

                // Consumer
                consumer = new Consumer(
                    client,
                    [
                        { topic: selectedTopic, offset: 0},
                        //{ topic: selectedTopic, partition: 0 },
                        //{ topic: 'topic2', partition: 0 }
                    ],
                    {
                        autoCommit: false,
                      fromOffset:'latest',
                    }
                );


                consumer.on('ready', function() {
                  console.log('Consumer ready!');
                })
                .on('offsetOutOfRange', function(err) {
                  console.log('Consumer offset out of range', err);
                })
                .on('error', function(err) {
                  console.log('There is a error with consumer', err);
                  // todo: Maybe reconnect or something
                })
                .on('message', function (message) {
                    console.log(message);
                    if (message.key !== -1) {
                        console.log('key', message.key.toString());
                    }

                });
            })

        }
    })
});

client.on('error', function(error) {
  console.log('There is an error on client', error);
});

// Handle exits
function exitHandler(options, error) {
    console.log('handling exit', error);
    client.close(function (error){
        console.log('connection closed', error);
    });
}

process.on('exit', exitHandler);
process.on('SIGINT', exitHandler);
process.on('uncaughtException', exitHandler);
