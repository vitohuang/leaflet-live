// Zookeeper connection string
const zookeeperUri = process.env.ZOOKEEPER_URI || '127.0.0.1:2181';

// Start the kafka
var kafka = require('kafka-node'),
    Producer = kafka.Producer,
    KeyedMessage = kafka.KeyedMessage,
    client = new kafka.Client(zookeeperUri),
    producer = new Producer(client),
    km = new KeyedMessage('key', 'message');
const _ = require('lodash');
const async = require('async');

// Command prompt
const readline = require('readline');
const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout
});

let counter = 0;

// Wait for the ready event before proceeding
let producerReady = false;
producer.on('ready', function() {
    producerReady = true;
});

// Select a topic
let selectedTopic = 'DiscoveryHub';
let supportedTypes = ['post', 'location'];
client.once('connect', function() {
    console.log('client connected');
    client.loadMetadataForTopics([], function(error, results) {
        if (error) {
            console.log('there is error getting list of topics', error);
        } else {
            console.log('List of topics');
            let topics = _.get(results, '1.metadata');
            // Print the selection
            topics = _.mapValues(topics, function(val, key) {
                const partitions = _.keys(_.mapValues(val, function(vv, key) {
                        return vv.partition;
                    })
                );
                console.log(key, '-', partitions.join(','));
            });

            rl.question('Please select a topic to send to?\n', (selectedTopic) => {

              console.log('Please choose message type:');
              supportedTypes.map((item) => {
                console.log(item);
              });

              // Ask to choose type of message
              rl.question('Please select type message type\n', (topicType) => {
                if (supportedTypes.indexOf(topicType) != -1) {
                  rl.question('Please specify the delay (in seconds)\n', (delay) => {
                    delay = parseInt(delay) * 1000;
                    console.log(`Going to send ${topicType} to ${selectedTopic}`);
                    switch (topicType) {
                      case 'location':
                        sendMessages(selectedTopic, topicType, locations, delay);
                        break;
                      default:
                        sendMessages(selectedTopic, topicType, posts, delay);
                    }
                  })
                } else {
                  console.log('Sorry the type is not supported, Bye!');
                }
              })
            })

        }
    })
});

function sendMessages(selectedTopic, topicType, data, delay) {
  // Go through  each of them
  async.eachSeries(data, (item, cb) => {
    try {
      const message = new KeyedMessage(topicType, JSON.stringify(item));
      const payload = [{
          topic: selectedTopic,
          messages: [message]
      }];
      producer.send(payload, function(error, data) {
                  if (error) {
                      console.log('error sending a type',error);
                      cb(error);
                  } else {
                    setTimeout(() => {
                      cb(null);
                      console.log('sent!');
                    }, delay);
                  }
              });
    } catch (err) {
      console.error('A problem occurred when sending our message');
      console.error(err);
    }
  }, (err) => {
    if (err) {
      console.log('failed', err);
    } else {
      console.log('Sent all!');
    }
  })
}

// Any errors we encounter, including connection errors
producer.on('error', function(err) {
  console.error('Error from producer');
  console.error(err);
});

// Handle exits
function exitHandler(options, error) {
    console.log('handling exit', options, error);
    client.close(function (error){
        console.log('connection closed');
    });
}

process.on('exit', exitHandler);
process.on('SIGINT', exitHandler);
process.on('uncaughtException', exitHandler);

// Get the locations
locations = [
  [12,33],
  [12,34],
  [12,35],
  [12,36],
  [12,37],
  [12,38],
  [12,39],
  [12,40],
  [12,41],
  [12,42],
  [12,43],
  [12,44],
  [12,45],
  [12,46],
  [12,47],
];

posts = [
  {
    publishDate: new Date(),
    content: {},
  }
];
