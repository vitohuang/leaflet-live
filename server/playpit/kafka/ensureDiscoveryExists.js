// Zookeeper connection string
const zookeeperUri = process.env.ZOOKEEPER_URI || '127.0.0.1:2181';

// Start the kafka
var kafka = require('kafka-node'),
    Producer = kafka.Producer,
    KeyedMessage = kafka.KeyedMessage,
    client = new kafka.Client(zookeeperUri),
    producer = new Producer(client),
    km = new KeyedMessage('key', 'message');
const debug = require('debug')('live-init');
const _ = require('lodash');

// Kafka util to ensure topic exists
const KafkaUtil = require('../../src/streamer/kafka/utils.js');

// Select a topic
let selectedTopic = 'DiscoveryHub';
debug(`Going to check if the ${selectedTopic} topic exist or not`);
client.on('ready', function() {
  debug('The Client is ready');
});

client.once('connect', function() {
  debug('Kafka client connected');

  // Introduce a bit delay
  let t = setTimeout(() => {
    debug('Ensure topic exists');
    KafkaUtil.ensureTopicExist(producer, selectedTopic)
      .then((result) => {
        debug('Its all good');
        exitHandler();
      })
      .catch((error) => {
        debug('Something is wrong', error);
        exitHandler();
      })
  }, 500);
});

client.on('error', function(error) {
  console.log('there is some error on the kafka client', error);
})

// Any errors we encounter, including connection errors
producer.on('error', function(err) {
  console.error('Error from producer');
  console.error(err);
});

// Handle exits
function exitHandler(options, error) {
    debug('handling exit', options, error);
    client.close(function (error){
      debug('errors', error);
      debug('connection closed');
    });
}

process.on('exit', exitHandler);
process.on('SIGINT', exitHandler);
process.on('uncaughtException', exitHandler);
