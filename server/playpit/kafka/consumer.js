var Kafka = require('node-rdkafka');
var config = require('../kafka/config.js');

// Start a consumer
var consumer = new Kafka.KafkaConsumer(Object.assign({}, {
  'group.id': 'my-consumer',
}, config), {
  'auto.offset.reset': 'smallest'
});

const topics = ["tufc-test"];
consumer
  .on('ready', function() {
		console.log('Consumer ready!');
    consumer.subscribe(topics);
    consumer.consume();
  })
  .on('error', function(err) {
		console.log('There is a error');
    console.log(err);
    process.exit(1);
  })
  .on('data', function(data) {
		console.log('Data on the pipe', data);
   // var msg = JSON.parse(data.value.toString());
		msg = data.value.toString();
    console.log(msg);
  });

consumer.connect();
