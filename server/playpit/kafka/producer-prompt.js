// Command prompt
const readline = require('readline');
const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout
});

// Kafka
var Kafka = require('node-rdkafka');
var config = require('../kafka/config.js');

var topics = ["tufc-test"];
var producer = new Kafka.Producer(config);

// Connect to the broker manually
producer.connect();

let counter = 0;
producer.on('delivery-report', function(report) {
  console.log('delivery-report: ' + JSON.stringify(report));
  counter++;
});

// Wait for the ready event before proceeding
producer.on('ready', function() {
	console.log("Producer ready!\nStart typing to send message");
/*
    producer.produce(
      // Topic to send the message to
      'tufc-test',
      // optionally we can manually specify a partition for the message
      // this defaults to -1 - which will use librdkafka's default partitioner (consistent random for keyed messages, random for unkeyed messages)
      null,
      // Message to send. If a string is supplied, it will be
      // converted to a Buffer automatically, but we're being
      // explicit here for the sake of example.
      new Buffer('Awesome message'),
      // for keyed messages, we also specify the key - note that this field is optional
      'Stormwind',
      // you can send a timestamp here. If your broker version supports it,
      // it will get added. Otherwise, we default to 0
      Date.now()
      // you can send an opaque token here, which gets passed along
      // to your delivery reports
    );
*/

		rl.on('line', function(input) {
			try {
				input = JSON.parse(input)
			} catch (err) {
				console.log('Please enter a valid JSON');
				return false;
			};

			try {
				const result = producer.produce('tufc-test', null, new Buffer(JSON.stringify(input)), 'Stormwind', Date.now());
				if (result) {
					console.log('Sent:', input);
				} else {
					console.log('Fail to send:', input);
				}
			} catch (err) {
				console.error('A problem occurred when sending our message');
				console.error(err);
			}
		}).on('close', function() {
			producer.disconnect(function(err, metrics) {
				console.log('Bye for now!');
			});
		});
});

// Any errors we encounter, including connection errors
producer.on('event.error', function(err) {
  console.error('Error from producer');
  console.error(err);
})
