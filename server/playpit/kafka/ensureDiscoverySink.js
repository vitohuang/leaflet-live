var rp = require('request-promise');
var debug = require('debug')('live-kafka-connector')

const connectorName = 'mongo-sink-discovery';
const topicName = 'DiscoveryHub';

// Going to replace this with kafkaUtil.createConnectorSink
let dbHost = 'mongo';
let dbPort = '27017';
let endpoint = 'http://kafka:8083';

const connectorData = {
 "name": connectorName,
 "config" :{
	 "connector.class":"com.startapp.data.MongoSinkConnector",
	 "tasks.max":"1",
	 "db.host": dbHost,
	 "db.port": dbPort,
	 "db.name":"kafka",
	 "db.collections": topicName,
	 "write.batch.enabled":"true",
	 "write.batch.size":"200",
	 "connect.use_schema":"false",
	 "topics": topicName,
	},
};

rp({
	uri: endpoint + '/connectors',
	json: true,
})
	.then((result) => {
		debug('result', result);
		if (result.indexOf(connectorName) === -1) {
			debug('Need to create a new sink connector');

			// Going to create the connector
			rp({
				uri: endpoint + '/connectors',
				method: 'POST',
				json: connectorData,
			})
				.then((connectors) => {
					debug('created connector', connectorName, connectors);
				})
				.catch((error) => {
					debug('There is error creating connector', error);
				})
		} else {
			debug(`${connectorName} already exists`);
		}
	})
	.catch((error) => {
		debug('there is an error checking connectors', error);
	})
