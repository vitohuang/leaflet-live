// Zookeeper connection string
const zookeeperUri = process.env.ZOOKEEPER_URI || '127.0.0.1:2181';

// Start the kafka
var kafka = require('kafka-node'),
    Producer = kafka.Producer,
    KeyedMessage = kafka.KeyedMessage,
    client = new kafka.Client(zookeeperUri),
    producer = new Producer(client),
    km = new KeyedMessage('key', 'message');
const _ = require('lodash');

// Command prompt
const readline = require('readline');
const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout
});

let counter = 0;

// Wait for the ready event before proceeding
let producerReady = false;
producer.on('ready', function() {
    producerReady = true;
});

// Select a topic
let selectedTopic = 'DiscoveryHub';
client.once('connect', function() {
    console.log('client connected');
    client.loadMetadataForTopics([], function(error, results) {
        if (error) {
            console.log('there is error getting list of topics', error);
        } else {
            console.log('List of topics');
            let topics = _.get(results, '1.metadata');
            // Print the selection
            topics = _.mapValues(topics, function(val, key) {
                const partitions = _.keys(_.mapValues(val, function(vv, key) {
                        return vv.partition;
                    })
                );
                console.log(key, '-', partitions.join(','));
            });

            rl.question('Please select a topic to send to?\n', (t) => {
                selectedTopic = t;

                // Get to the prompt
                promptInput();
            })

        }
    })
});

function promptInput() {
	console.log("Producer ready!\nStart typing to send message");

		rl.on('line', function(rawInput) {
			try {
				input = JSON.parse(rawInput)
			} catch (err) {
				console.log('Please enter a valid JSON');
				return false;
			};

			try {
                const message = new KeyedMessage('prompt-input', rawInput);
                const payload = [{
                    topic: selectedTopic,
                    messages: [message]
                }];
				producer.send(payload, function(error, data) {
                    if (error) {
                        console.log(error);
                    } else {
                        console.log('sent!');
                    }
                });
			} catch (err) {
				console.error('A problem occurred when sending our message');
				console.error(err);
			}
		}).on('close', function() {
			producer.close(function(err, metrics) {
				console.log('Bye for now!');
			});
		});
}

// Any errors we encounter, including connection errors
producer.on('error', function(err) {
  console.error('Error from producer');
  console.error(err);
});

// Handle exits
function exitHandler(options, error) {
    console.log('handling exit', options, error);
    client.close(function (error){
        console.log('connection closed');
    });
}

process.on('exit', exitHandler);
process.on('SIGINT', exitHandler);
process.on('uncaughtException', exitHandler);
