const KafkaUtil = require('../../src/streamer/kafka/utils.js');

const readline = require('readline');
const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout
});


rl.question('Please enter the topic name for the stage?', (selectedTopic) => {
  if (selectedTopic) {
    KafkaUtil.createConnectorSink(selectedTopic);
  }
});
