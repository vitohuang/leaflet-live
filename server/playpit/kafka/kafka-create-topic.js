// Zookeeper connection string
const zookeeperUri = process.env.ZOOKEEPER_URI || '127.0.0.1:2181';

// Start the kafka
var kafka = require('kafka-node'),
    Producer = kafka.Producer,
    KeyedMessage = kafka.KeyedMessage,
    client = new kafka.Client(zookeeperUri),
    producer = new Producer(client),
    km = new KeyedMessage('key', 'message');
const _ = require('lodash');

// Wait for the ready event before proceeding
producer.on('ready', function() {
  let topic = process.argv[2];
  console.log('producer is ready, going to create topics - ' + topic);
  producer.createTopics([topic], true, function(err, data) {
    if (err) {
      console.log('there is an error', err)
    } else {
      console.log('created topic - ' + topic, data);
      listTopics();
    }
  })
});

// List topics
function listTopics () {
    client.loadMetadataForTopics([], function(error, results) {
        if (error) {
            console.log('there is error getting list of topics', error);
        } else {
            console.log('List of topics');
            let topics = _.get(results, '1.metadata');
            // Print the selection
            topics = _.mapValues(topics, function(val, key) {
                const partitions = _.keys(_.mapValues(val, function(vv, key) {
                        return vv.partition;
                    })
                );
                console.log(key, '-', partitions.join(','));
            });
        }
    })
};

// Any errors we encounter, including connection errors
producer.on('error', function(err) {
  console.error('Error from producer');
  console.error(err);
});

// Handle exits
function exitHandler(options, error) {
    console.log('handling exit', options, error);
    client.close(function (error){
        console.log('connection closed');
    });
}

process.on('exit', exitHandler);
process.on('SIGINT', exitHandler);
process.on('uncaughtException', exitHandler);
