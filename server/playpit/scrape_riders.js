const mongoose = require('mongoose');
const request = require('request');

mongoose.connect('mongodb://127.0.0.1/riders');

var db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', (error, one, two) => {
    console.log('Mongo connection opened');
    //getContent('http://integration.api.cyclingnews.com/riders?limit=50')
    getContent('http://integration.api.cyclingnews.com/riders?offset=16050&limit=50&page=322');
})

const riderSchema = new mongoose.Schema({}, {strict: false});
var Rider = mongoose.model('Rider', riderSchema);

var counter = 1;
var riderCount = 1;
function getContent(url) {
    counter++;
    console.log('counter', counter);

    /*
    if (counter > 5) {
        return false;
    }
    */

    request(url, function(error, response, body) {
        console.log(typeof body);
        try {
            const bodyJson = JSON.parse(body);
            const data = bodyJson.data;
            const meta = bodyJson.meta;
            if (data) {

                Rider.collection.insert(data.riders, function(err, docs) {
                    if (err) {
                        console.log('there is something wrong with insert');
                    } else {
                        console.log('document inserted');
                    }
                });

                riderCount += data.riders.length;
                console.log('rider - ' + riderCount);
                /*
                data.riders.forEach(function(rider) {
                    console.log(rider);

                });
                */

                if (meta.pagination.links.next) {
                    const nextLink = meta.pagination.links.next;
                    console.log('there is a next link', nextLink);
                    setTimeout(() => {
                        getContent(nextLink);
                    }, 300);
                }
            }
        } catch (error) {
          console.log("Can't convert to json", error);
          setTimeout(() => {
            getContent(url);
          }, 300);
        }

    })
}

