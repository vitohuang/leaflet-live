function CirclueArray(length) {
    this.maxLength = length || 10;
    this.arr = new Array(this.maxLength);
}

CirclueArray.prototype.push = function(item) {
    // Get array size
    var size = this.arr.filter(function(value) {
        return value !== undefined
    }).length;

    console.log('real size', size);
    if (size >= this.maxLength) {
        console.log("going to pop one");
        this.arr.pop();
        console.log(this.arr);
    }
    this.arr.unshift(item);

}

a = new CirclueArray(2);

a.push("one");
console.log(a);
a.push("two");
console.log(a);
a.push("three");
console.log(a);
a.push("four");
console.log(a);
a.push("five");

