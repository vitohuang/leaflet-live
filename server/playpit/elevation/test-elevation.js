const addElevation = require('./elevation.js');
const geometry = require('./ge1.json');

const lonLat = geometry.features[0].geometry.coordinates
function lonLat2LatLon(lonLat) {
  console.log('before', lonLat);
  const latLon = lonLat.map(l => [l[1], l[0]].concat(l.slice(2)));
  console.log('after', latLon);
  return latLon;
}

addElevation(lonLat2LatLon(lonLat))
.then((results) => {
    console.log(results);
});
