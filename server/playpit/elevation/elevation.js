const rp = require('request-promise');
const async = require('async');
const _ = require('lodash');

const apiKey = 'AIzaSyB7bqXQJIwOdR2dYbNVg6g7BmGyu6ICKd0';

var getElevation = function(lat, lon) {
    return new Promise((resolve, reject) => {
        rp(`https://maps.googleapis.com/maps/api/elevation/json?locations=${lat},${lon}&key=${apiKey}`)
            .then((response, one, two, three) => {
                const data = JSON.parse(response);
                if (data.status == 'OK' || data.results.length > 0) {
                    resolve(data.results[0].elevation);
                }
            })
            .catch((err) => {
                reject(err);
            })
    });
}


function getElevations (elevations) {
    return new Promise((resolve, reject) => {
        async.mapSeries(elevations, (latLon, cb) => {
            // Get elevation
            getElevation(latLon[0], latLon[1])
                .then((elevation) => {
                    cb(null, latLon.concat([elevation]));

                }).catch((err) => {
                    cb(true, err);
                })
        }, (err, results) => {
            if (err) {
                reject(err);
            } else {
                resolve(results);
            }
        })
    })
}

// Group locations in one version
var getElevationMulti = function(pathString, samples) {
    return new Promise((resolve, reject) => {
        rp(`https://maps.googleapis.com/maps/api/elevation/json?path=${pathString}&samples=${samples}&key=${apiKey}`)
            .then((response, one, two, three) => {
                const data = JSON.parse(response);
                if (data.status == 'OK' || data.results.length > 0) {
                    resolve(data);
                }
            })
            .catch((err) => {
                reject(err);
            })
    });
}


function getElevationsMulti (elevations) {
    return new Promise((resolve, reject) => {
        async.map(_.chunk(elevations, 2), (col, cb) => {
            const samples = _.size(col);

            if (samples > 1) {
                let pathString = col.reduce((acc, item) => {
                    return acc + item.join(',') + '|';
                }, '');

                // Remove the last |
                pathString = pathString.substring(0, pathString.length - 1);

                getElevationMulti(pathString, _.size(col))
                    .then((elevations) => {
                        // Transform the elevations
                        let ret = elevations.results.map((result) => {
                            return [result.location.lat, result.location.lng, result.elevation];
                        })
                        cb(null, ret);
                    }).catch((err) => {
                        cb(true, err);
                    })
            } else {
                const latLon = col[0];
                getElevation(latLon[0], latLon[1])
                    .then((elevation) => {
                        cb(null, latLon.concat([elevation]));
                    }).catch((err) => {
                        cb(true, err);
                    })
            }
        }, (err, results) => {
            if (err) {
                reject(err);
            } else {
                resolve(results);
            }
        })
    })
}

async function a(latLons) {
    const results = await getElevationsMulti(latLons);
    let data = [];
    results.map((result) => {
        if (typeof result[0] == 'number') {
            data.push(result);
        } else {
            result.map((ret) => {
                data.push(ret);
            })
        }
    })

    return results;
}

module.exports = a;
