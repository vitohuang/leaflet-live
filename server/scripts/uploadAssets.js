const aws = require('aws-sdk');
const fs = require('fs');
const path = require('path');
const mime = require('mime-types');

// Load the config
const AWSConfig = require('./config.js');

aws.config.update({
  accessKeyId: AWSConfig.accessKeyId,
  secretAccessKey: AWSConfig.secretAccessKey,
  region: AWSConfig.region,
});

const s3 = new aws.S3();

const adminAssets = __dirname + '/../../admin/build/';
const feAssets = __dirname + '/../../frontend/build/';
const map = {
  [adminAssets]: 'admin.cluelesscycling.com',
  [feAssets]: 'live.cluelesscycling.com',
};

const uploadDir = function(s3Path, bucketName) {
  // Resolve the path - in case there is relative path
  s3Path = path.resolve(s3Path);

  function walkSync(currentDirPath, callback) {
    fs.readdirSync(currentDirPath).forEach(function (name) {
      var filePath = path.join(currentDirPath, name);
      var stat = fs.statSync(filePath);
      if (stat.isFile()) {
        callback(filePath, stat);
      } else if (stat.isDirectory()) {
        walkSync(filePath, callback);
      }
    });
  }

  walkSync(s3Path, function(filePath, stat) {
    const bucketPath = filePath.substring(s3Path.length + 1);
    const mimeType = mime.lookup(filePath);
    const params = {
      Bucket: bucketName,
      Key: bucketPath,
      Body: fs.readFileSync(filePath),
      ACL: 'public-read',
      ContentType: mimeType,
    };

    s3.putObject(params, function(err, data) {
      if (err) {
        console.log(err)
      } else {
        //console.log('Successfully uploaded '+ bucketPath +' to ' + bucketName);
        console.log('Successfully uploaded '+ filePath + ' -> ' + bucketPath +' to ' + bucketName);
      }
    });

  });
};

let uploads = [];
Object.keys(map).forEach((key) => {
  console.log('going to upload', key, map[key]);
  if (fs.existsSync(key)) {
    uploadDir(key, map[key]);
  }
});
