var async = require('async');
var mysql = require('mysql');
var mongo = require('mongodb').MongoClient;
var debug = require('debug')('live-event');

function createConnections() {
  return new Promise(function(resolve, reject) {
    async.parallel({
      // Mongo
      mongo: function(callback) {
        mongo.connect('mongodb://localhost:27017/riders', function(error, db) {
          if (error) {
            console.error('Error connecting to MongoDB: ' + error);
            callback(error);
          } else {
            console.log('Connected to mongodb');
            callback(null, db);
          }
        });
      },

      // Mysql
      mysql: function(callback) {
        var Mysql = mysql.createConnection({
          host: 'localhost',
          port: 8889,
          user: 'root',
          password: 'root',
          database: 'cyclingnews',
        });

        // Connect
        Mysql.connect(function(error) {
          if (error) {
            console.error('Error connecting: ' + error);
            callback(error);
          } else {
            console.log('Connected to mysql');
            callback(null, Mysql);
          }
        });
      }
    }, function(error, results) {
      if (error) {
        console.error('Error connecting to databases', error);
        reject(error);
      } else {
        resolve(results);
        /*
        deleteCollections(results.mongo, function(error) {
          // Deleted everything and done
          resolve(results);
        });
        */
      }
    });
  });
}

function deleteCollections(db, callback) {
  async.each(['teams', 'uci', 'teamtypes', 'teamseasons'], function(name, callback) {
    db.collection(name).drop(function(error) {
      console.log('Deleted ' + name + ' collection');
      callback();
    });
  }, function(error) {
    if (error) {
      console.log('error deleting the collections', error);
      callback(error);
    } else {
      callback(null);
    }
  })
}

createConnections().then(function(connections) {
  const Mongo = connections.mongo;
  const Mysql = connections.mysql;

  console.log('got all the conntections');

  // Uci codes
  Mysql.query('select * from uci_codes', function(error, results, fields) {
    if (error) {
      throw error;
    }

    let Uci = Mongo.collection('uci');
    results.forEach(function(result) {
      Uci.insert({
        id: result.id,
        name: result.name,
        uci_code: result.uci_code,
        description: result.description,
        createdAt: new Date(),
        updatedAt: new Date(),
      }, function (error, doc) {
        if (error) {
          console.error('There is error insert uci code:' + result.name);
        } else {
          debug('Uci code:' + result.name + ' inserted');
        }
      });
    });
  });

  // Team types
  Mysql.query('select * from team_types', function(error, results, fields) {
    if (error) {
      throw error;
    }

    let Types = Mongo.collection('teamtypes');
    results.forEach(function(result) {
      Types.insert({
        id: result.id,
        name: result.name,
        slug: result.handle,
        order: result.order,
        createdAt: new Date(),
        updatedAt: new Date(),
      }, function (error, doc) {
        if (error) {
          console.error('There is error insert team type:' + result.name);
        } else {
          debug('Team type:' + result.name + ' inserted');
        }
      });
    });
  })

  // Teams
  Mysql.query('select * from teams', function(error, results, fields) {
    if (error) {
      throw error;
    }

    let Types = Mongo.collection('teams');
    results.forEach(function(result) {
      Types.insert(result, function (error, doc) {
        if (error) {
          console.error('There is error insert team:' + result.id);
        } else {
          debug('Team id:' + result.id + ' inserted');
        }
      });
    });
  });

  // Team Seasons
  const teamQuery = 'SELECT ts.name, ts.handle, tln.handle as alt_handle, ts.abbrev, d.name as discipline, c.abbrev as country_abbrev, c.name as country_name, ts.bikes, ts.sponsor, tt.name as team_type, tt.handle as team_type_handle, t.summary as team_summary, t.description as team_description, t.established, ts.summary, ts.description, ts.official_site, ts.official_email, ts.official_tel, ts.official_fax, ts.official_address FROM teams_seasons as ts left join teams as t on t.id = ts.team_id left join seasons as s on s.id = ts.season_id left join team_types as tt on tt.id = ts.team_type_id left join countries as c on c.id = ts.country_id left join disciplines as d on d.id = ts.discipline_id left join teamsaltnames as tln on tln.team_season_id = ts.id';
  Mysql.query(teamQuery, function(error, results, fields) {
    if (error) {
      throw error;
    }

    let TeamSeasons = Mongo.collection('teamseasons');
    results.forEach(function(result) {
      TeamSeasons.insert(result, function (error, doc) {
        if (error) {
          console.error('There is error insert team seasons:' + result.name);
        } else {
          debug('Team seasons:' + result.name + ' inserted');
        }
      });
    });
  });

  // Todo get the image as well :-)
  // Also the riders
  Mysql.end();
});
