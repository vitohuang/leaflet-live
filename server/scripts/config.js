require('dotenv').config({
  path: '../.env',
});

const env = process.env.NODE_ENV || 'development';
const Config = {
  production: {
  },
  development: {
  },
};

module.exports = Object.assign(Config[env], {
  accessKeyId: process.env.AWS_ACCESS_KEY_ID,
  secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY,
  region: process.env.AWS_REGION,
});
