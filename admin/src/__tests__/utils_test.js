import {
  handleMessages,
  sortByTimestamp,
  mergeByTimestamp,
} from '../share/utils.js';

test('adds 1 + 2 to equal 3', () => {
  expect(3).toBe(3);
})

const existingData = {
    maps: {},
    posts: {
        'stage-1': [{
            timestamp: '2019-09-12 14:21:00',
            data: {
                publishDate: '2019-09-12 14:21:00',
                distance: null,
                content: 'abc',
            },
            _topic: 'stage-1',
            _type: 'post',
        }, {
            timestamp: '2019-09-12 12:22:00',
            data: {
                publishDate: '2019-09-12 12:22:00',
                distance: null,
                content: 'cde',
            },
            _topic: 'stage-1',
            _type: 'post',
        }, ],
    },
    locations: {
        'stage-1': [{
            timestamp: '2019-09-12 12:21:00',
            data: {},
            _topic: 'stage-1',
            _type: 'location',
        }, ],
    },
};

const data = [{
    topic: 'stage-1',
    value: {
        timestamp: '2019-09-12 13:22:00',
        data: {
            publishDate: '2019-09-12 13:22:00',
            distance: null,
            content: 'new abc',
        },
        _topic: 'stage-1',
        _type: 'post',
    },
    offset: 12,
    key: 'post',
}, {
    topic: 'stage-1',
    value: {
        timestamp: '2019-09-12 13:23:00',
        data: {
            publishDate: '2019-09-12 13:23:00',
            distance: null,
            content: 'new cde',
        },
        _topic: 'stage-1',
        _type: 'post',
    },
    offset: 13,
    key: 'post',
}, {
    topic: 'stage-1',
    value: {
        timestamp: '2019-09-12 13:23:00',
        data: [12, 32],
        _topic: 'stage-1',
        _type: 'location',
    },
    offset: 13,
    key: 'location',
}, {
    topic: 'stage-2',
    value: {
        timestamp: '2019-09-12 13:23:00',
        data: {
            publishDate: '2019-09-12 13:23:00',
            distance: null,
            content: 'new cde',
        },
        _topic: 'stage-2',
        _type: 'post',
    },
    offset: 13,
    key: 'post',
}, ];

// For test the merge by timestamp
const t1 = [{
    timestamp: '2019-09-12 14:21:00',
    data: {
        publishDate: '2019-09-12 14:21:00',
        distance: null,
        content: 'abc',
    },
    _topic: 'stage-1',
    _type: 'post',
}, {
    timestamp: '2019-09-12 12:22:00',
    data: {
        publishDate: '2019-09-12 12:22:00',
        distance: null,
        content: 'cde',
    },
    _topic: 'stage-1',
    _type: 'post',
}, ];

const t2 = [{
    timestamp: '2019-09-12 11:21:00',
    data: {
        publishDate: '2019-09-12 14:21:00',
        distance: null,
        content: 'abc',
    },
    _topic: 'stage-1',
    _type: 'post',
}, {
    timestamp: '2019-09-12 10:22:00',
    data: {
        publishDate: '2019-09-12 12:22:00',
        distance: null,
        content: 'cde',
    },
    _topic: 'stage-1',
    _type: 'post',
}, ];

test('Sort by time stamp', () => {
  const ret = sortByTimestamp(existingData);
  // The second entry should be first now
  expect(ret.posts['stage-1'][0].data.content).toEqual('cde');
});

test('Merge by timestamp', () => {
  const ret = mergeByTimestamp(t1, t2);
  expect(ret[0].data.content).toEqual('cde');
});

test('HandleMessages should return the correct results', () => {
  const ret = handleMessages(existingData, data);

  expect(ret.posts['stage-1'][0].data.content).toEqual('cde');

  expect(ret.posts['stage-2'][0].data.content).toEqual('new cde');
});


/*
const existingData = {
  mpas: {
  },
  posts: {
    'stage-1': [
      {
        timestamp: '2019-09-12 12:21:00',
        data: {
          publishDate: '2019-09-12 12:21:00',
          distance: null,
          content: 'abc',
        },
        _topic: 'stage-1',
        _type: 'post',
      },
      {
        timestamp: '2019-09-12 12:22:00',
        data: {
          publishDate: '2019-09-12 12:22:00',
          distance: null,
          content: 'cde',
        },
        _topic: 'stage-1',
        _type: 'post',
      },
    ],
  },
  locations: {
    'stage-1': [
      {
        timestamp: '2019-09-12 12:21:00',
        data: {
        },
        _topic: 'stage-1',
        _type: 'location',
      },
    ],
  },
};

const data = [
  {
    topic: 'stage_1',
    value: {
      timestamp: '2019-09-12 13:22:00',
      data: {
        publishDate: '2019-09-12 13:22:00',
        distance: null,
        content: 'new abc',
      },
      _topic: 'stage-1',
      _type: 'post',
    },
    offset: 12,
    key: 'post',
  },
  {
    topic: 'stage_1',
    value: {
      timestamp: '2019-09-12 13:23:00',
      data: {
        publishDate: '2019-09-12 13:23:00',
        distance: null,
        content: 'new cde',
      },
      _topic: 'stage-1',
      _type: 'post',
    },
    offset: 13,
    key: 'post',
  },
  {
    topic: 'stage_1',
    value: {
      timestamp: '2019-09-12 13:23:00',
      data: [12,32],
      _topic: 'stage_1',
      _type: 'location',
    },
    offset: 13,
    key: 'location',
  },
  {
    topic: 'stage_2',
    value: {
      timestamp: '2019-09-12 13:23:00',
      data: {
        publishDate: '2019-09-12 13:23:00',
        distance: null,
        content: 'new cde',
      },
      _topic: 'stage_2',
      _type: 'post',
    },
    offset: 13,
    key: 'post',
  },
];
*/
