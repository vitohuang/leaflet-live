import React, { Component } from 'react';
import { Provider } from 'react-redux';

// Styling
import {
  MuiThemeProvider,
  createMuiTheme,
} from '@material-ui/core/styles';
import blue from '@material-ui/core/colors/blue';

import Overview from './containers/Overview.js';

// Stores
import store from './store';
import './App.css';
import {
  checkLogin,
} from './store/actions.js';

// Initialise the websocket
import ws from './store/ws.js';
ws(store);

// Dispatch the check login
store.dispatch(checkLogin())

// Theme
const theme = createMuiTheme({
  typography: {
    useNextVariants: true,
  },
  palette: {
    primary: blue,
  },
});

// The app
class App extends Component {

	// Render the component
  render() {
		// Render the tags
    return (
			<Provider store={store}>
        <MuiThemeProvider theme={theme}>
          <Overview />
				</MuiThemeProvider>
			</Provider>
    );
  }
}

export default App;
