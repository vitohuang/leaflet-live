import React from 'react';
import {
  Route,
} from 'react-router-dom';

import StageWrapper from './containers/StageWrapper.js';
import LiveReport from './components/LiveReport.js';
import Situations from './containers/Situations.js';
import StageResults from './containers/stage/Results.js';
import StageCourseMap from './containers/stage/CourseMap.js';
import StageStartList from './containers/stage/StartList.js';
import StageSettings from './containers/StageSettings.js';

import RaceWrapper from './containers/RaceWrapper.js';
import RaceResults from './containers/race/Results.js';
import RaceCourseMap from './containers/race/CourseMap.js';
import RaceStartList from './containers/race/StartList.js';
import RaceSettings from './containers/race/Settings.js';
import AddRace from './containers/AddRace.js';

import Home from './containers/Home.js';
import Help from './components/Help.js';

const routes = [
	{
    name: 'home',
    path: '/',
    exact: true,
    component: Home,
	},
  {
    name: 'add race',
    path: '/addrace',
    component: AddRace,
  },
  {
    name: 'races',
    path: '/races/:raceSlug/',
    component: RaceWrapper,
    routes: [
      {
        name: "stage",
        path: '/races/:raceSlug/stages/:stageSlug',
        component: StageWrapper,
        routes: [
          {
            name: 'Live Report',
            path: '/races/:raceSlug/stages/:stageSlug/live',
            exact: true,
            component: LiveReport,
          },
          {
            name: 'Situations',
            path: '/races/:raceSlug/stages/:stageSlug/situations',
            exact: true,
            component: Situations,
          },
          {
            name: 'Results',
            path: '/races/:raceSlug/stages/:stageSlug/results',
            exact: true,
            component: StageResults,
          },
          {
            name: 'Course Map',
            path: '/races/:raceSlug/stages/:stageSlug/coursemap',
            exact: true,
            component: StageCourseMap,
          },
          {
            name: 'Start List',
            path: '/races/:raceSlug/stages/:stageSlug/startlist',
            exact: true,
            component: StageStartList,
          },
          {
            name: 'Settings',
            path: '/races/:raceSlug/stages/:stageSlug/settings',
            exact: true,
            component: StageSettings,
          },
        ]
      },
      {
        name: "Race Results",
        path: '/races/:raceSlug/results',
        exact: true,
        component: RaceResults,
      },
      {
        name: "Race Course map",
        path: '/races/:raceSlug/coursemap',
        exact: true,
        component: RaceCourseMap,
      },
      {
        name: "Race Start List",
        path: '/races/:raceSlug/startlist',
        exact: true,
        component: RaceStartList,
      },
      {
        name: "Race Settings",
        path: '/races/:raceSlug/settings',
        exact: true,
        component: RaceSettings,
      },
    ]
  },
  {
    name: 'help',
    path: '/help',
    component: Help,
  }
];

export const Sidebar = {
  home: [
    {
      name: 'Dashboard',
      path: '/',
    },
    {
      name: 'Add Race',
      path: '/addrace',
    },
  ],
  race:[
    {
      name: 'Dashboard',
      path: '/',
    },
    {
      name: 'Race',
      path: '/races/:raceSlug/',
    },
    {
      name: 'Results',
      path: '/races/:raceSlug/results',
    },
    {
      name: 'Course Map',
      path: '/races/:raceSlug/coursemap',
    },
    {
      name: 'Start List',
      path: '/races/:raceSlug/startlist',
    },
    {
      name: 'Race Settings',
      path: '/races/:raceSlug/settings',
    },
  ],
  stage:[
    {
      name: 'Dashboard',
      path: '/',
    },
    {
      name: 'Race',
      path: '/races/:raceSlug/',
    },
    {
      name: 'Live',
      path: '/races/:raceSlug/stages/:stageSlug/live',
    },
    {
      name: 'Situations',
      path: '/races/:raceSlug/stages/:stageSlug/situations',
    },
    {
      name: 'Results',
      path: '/races/:raceSlug/stages/:stageSlug/results',
    },
    {
      name: 'Stage Map',
      path: '/races/:raceSlug/stages/:stageSlug/coursemap',
    },
    {
      name: 'Start List',
      path: '/races/:raceSlug/stages/:stageSlug/startlist',
    },
    {
      name: 'Settings',
      path: '/races/:raceSlug/stages/:stageSlug/settings',
    },
  ],
}

export function RouteWithSubRoutes(route) {
  return <Route path={route.path} exact={route.exact} render={props => (
    // pass the sub-routes down to keep nesting
    <route.component {...props} routes={route.routes}/>
  )}/>
}

export default routes;
