import React, { Component } from 'react';

import AddIcon from '@material-ui/icons/Add';
import {
  IconButton
} from '@material-ui/core';

import ResultCard from './ResultCard.js';
import EditResult from  '../components/EditResult.js';

class Results extends Component {
  constructor(props) {
    super(props);

    this.state = {
      showAdd: false,
    }
  }

  save(result) {
    console.log('going to save the results', result);
    this.props.save(result);
  }

  render() {
    console.log("results:", this.props.results);

    return (
      <div className="results-container">
        <p>Results</p>

        <p>List of results</p>
        {this.props.results.map((result) => {
          return <ResultCard
            id={result._id}
            key={result._id}
            name={result.name}
            type={result.type}
            description={result.description}
            entries={result.entries}
            onSave={this.save.bind(this)}
            onRemove={this.props.remove}
          />
        })}

        { !this.state.showAdd &&
          <IconButton
            onClick={(e) => this.setState({showAdd: true})}
          >
            <AddIcon />
          </IconButton>
        }

        { this.state.showAdd &&
            <EditResult
              onSave={this.save.bind(this)}
              onCancel={e => this.setState({showAdd: false})}
            />
        }
      </div>
    )
  }
}

export default Results;
