import React from 'react';
import PropTypes from 'prop-types';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import { Link } from 'react-router-dom';

class ListItemLink extends React.Component {
  renderLink = itemProps => <Link to={this.props.to} {...itemProps} />;

  render() {
    const { icon, primary, onClick } = this.props;
    return (
      <li>
        <ListItem button onClick={onClick}>
          {icon &&
            <ListItemIcon>{icon}</ListItemIcon>
          }

          <ListItemText primary={primary} />
        </ListItem>
      </li>
    );
  }
}

ListItemLink.propTypes = {
  icon: PropTypes.node,
  primary: PropTypes.node.isRequired,
  to: PropTypes.string.isRequired,
};

export default ListItemLink;
