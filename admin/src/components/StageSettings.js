import React, { Component } from 'react';
import { Editor } from 'react-draft-wysiwyg';
import { convertFromHTML, EditorState, ContentState } from 'draft-js';
import 'react-draft-wysiwyg/dist/react-draft-wysiwyg.css';
import { convertToRaw } from 'draft-js';
import draftToHtml from 'draftjs-to-html';
import moment from 'moment';

import {
  DatePicker,
  TimePicker,
  MuiPickersUtilsProvider,
} from 'material-ui-pickers';
import MomentUtils from '@date-io/moment';

import Button from'./abstracts/Button';
import TextField from'./abstracts/TextField';

import {
  uploadFile,
} from '../share/utils.js';

class StageSettings extends Component {
  constructor(props) {
    super(props)

    const propsData = this.getSettingsData(props.settings);

    this.handleFileUpload = this.handleFileUpload.bind(this);

    // Set the state
    this.state = {
      _id: propsData._id,
      name: propsData.name,
      startDate: propsData.startDate,
      startTime: propsData.startTime,
      distance: propsData.distance,
      startLocation: propsData.startLocation,
      endLocation: propsData.endLocation,
      description: propsData.description,
      coverImageUrl: propsData.coverImageUrl,
    }
  }

  handleFileUpload(event) {
    console.log('handle file upload', event);

    const file = event.target.files[0];

    console.log('the file', file);
    // Upload to server
    uploadFile(file)
      .then(data => {
        console.log('uploaded this is data', data, data.data.link);

        // Going to set the preview and save to setting
        this.setState({
          coverImageUrl: data.data.link,
        })
      })
      .catch(error => console.log(error));
  }

  // Get settings or defaults
  getSettingsData(props) {
    console.log('going to transform props', props);
    var {
      _id,
      name,
      startDate,
      distance,
      startLocation,
      endLocation,
      description,
      coverImageUrl,
    } = props;

    let startLocationName = '';
    if (startLocation && startLocation.name) {
      startLocationName = startLocation.name;
    }

    let endLocationName = '';
    if (endLocation && endLocation.name) {
      endLocationName = endLocation.name;
    }

    // Make it easy to format
    //startDate = moment(startDate);
    if (startDate) {
      startDate = new Date(startDate);
    }

    // Default description
    if (description) {
      let blocksFromHtml = convertFromHTML(description);
      description = EditorState.createWithContent(ContentState.createFromBlockArray(
        blocksFromHtml.contentBlocks,
        blocksFromHtml.entityMap
      ));
    } else {
      description = EditorState.createWithContent(ContentState.createFromText(''));
    }

    // Set the state
    return {
      _id,
      name,
      startDate: startDate,
      startTime: startDate,
      distance,
      startLocation: startLocationName,
      endLocation: endLocationName,
      description,
      coverImageUrl,
    }
  }

  componentWillReceiveProps(nextProps) {
    const propsData = this.getSettingsData(nextProps.settings);
    console.log('will receive props', propsData);

    this.setState({...propsData})
  }

  onEditorStateChange(editorState) {
    console.log('editor state changed', editorState);
    this.setState({
      description: editorState,
    });
  }

  save() {
    console.log('going to save settings', this.state);
    // Handle the start time
    let dateTime = new Date();
    try {
      const startDate = moment(this.state.startDate);
      const startTime = moment(this.state.startTime);

      dateTime = new Date(startDate.format('YYYY-MM-DD') + ' ' + startTime.format('hh:mm:ss'));
    } catch (error) {
      console.log('error at convert dates', error);
      return false;
    }

    // Get the content state
    let description = '';
    const contentState = this.state.description.getCurrentContent();
    if (contentState.hasText()) {
      const rawContent = convertToRaw(contentState);
			description = draftToHtml(rawContent);
    }

    console.log('datetime', dateTime);
    this.props.save({
      _id: this.state._id,
      name: this.state.name,
      startDate: dateTime,
      distance: this.state.distance,
      startLocation: {
        name: this.state.startLocation,
      },
      endLocation: {
        name: this.state.endLocation,
      },
      description,
      coverImageUrl: this.state.coverImageUrl,
    });
  }

  render() {

    console.log(this.props.settings, 'settings');
    return (

      <MuiPickersUtilsProvider utils={MomentUtils}>
        <div>
          <p>settings</p>
          <TextField
            hintText="Stage Name"
            value={this.state.name}
            onChange={(e, val) => this.setState({name: val})}
            floatingLabelText="Stage Name"
          />
          <br/>

          <DatePicker
            label="Start Date"
            value={this.state.startDate}
            onChange={(val) => this.setState({startDate: val})}
          />
          <TimePicker
            format="24hr"
            label="Start time"
            value={this.state.startTime}
            onChange={(val) => this.setState({startTime: val})}
          />
          <br/>

          <TextField
            hintText="Distance"
            value={this.state.distance}
            onChange={(e, val) => this.setState({distance: val})}
            floatingLabelText="Distance"
          />
          <br/>

          <TextField
            hintText="Start Location"
            value={this.state.startLocation}
            onChange={(e, val) => this.setState({startLocation: val})}
            floatingLabelText="Start Location"
          />
          <br/>

          <TextField
            hintText="End Location"
            value={this.state.endLocation}
            onChange={(e, val) => this.setState({endLocation: val})}
            floatingLabelText="End Location"
          />
          <br />

          <h3>Description</h3>
          <Editor
            editorState={this.state.description}
            toolbarClassName="home-toolbar"
            wrapperClassName="home-wrapper"
            editorClassName="home-editor"
            onEditorStateChange={this.onEditorStateChange.bind(this)}
            toolbar={{
              inline: { inDropdown: true },
              list: { inDropdown: true },
              textAlign: { inDropdown: true },
              link: { inDropdown: true },
              history: { inDropdown: true },
            }}
          />

          <h3>Cover Image</h3>
          {this.state.coverImageUrl && (
            <img src={this.state.coverImageUrl} width="500"/>
          )}

          <input
            accept="image/*"
            className="class-input"
            id="contained-button-file"
            multiple
            type="file"
            onChange={this.handleFileUpload}
          />
          <label htmlFor="contained-button-file">
            <Button variant="contained" component="span" className="class-input">
              Upload
            </Button>
          </label>

          <div className="buttons">
            <Button
              label="Save"
              className="left"
              secondary={true}
              onClick={this.save.bind(this)}
            />
            <Button
              className="right"
              label="Delete this stage"
              secondary={false}
              onClick={() => this.props.delete(this.state._id)}
            />
            <div className="clear"></div>
          </div>
        </div>
      </MuiPickersUtilsProvider>
    )
  }
}

export default StageSettings;
