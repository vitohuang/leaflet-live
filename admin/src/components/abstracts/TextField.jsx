import React from 'react';
import {
  TextField,
} from '@material-ui/core';

const LE_TextField = ({ value, onChange, floatingLabelText, label, hintText, onKeyPress, disabled }) => {
	// Have to use different name
	let notAllowChange = false;
	if (disabled) {
		notAllowChange = true;
	}

  // On change take proceeding
  let handleChange = onChange || onKeyPress;

  return (
    <TextField
      label={label || floatingLabelText}
      margin="dense"
      fullWidth
      InputLabelProps={{
        shrink: true,
      }}
			disabled={notAllowChange}
      value={value}
      onChange={(e) => handleChange(e, e.target.value)}
    />
	);
};

export default LE_TextField;
