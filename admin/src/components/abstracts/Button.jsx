import React from 'react';
import {
  Button,
} from '@material-ui/core';

const LE_Button = ({ variant, label, onClick, disabled, primary, secondary, children }) => {
  let color = 'default';
  if (primary) {
    color = 'primary';
  } if (secondary) {
    color = 'secondary';
  }

  return (
    <Button
      variant={ variant || 'contained' }
      disabled={ disabled || false }
      color={color}
      onClick={onClick}
    >
      {label}
      {children}
    </Button>
	);
};

export default LE_Button;
