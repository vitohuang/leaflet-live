import React, { Component } from 'react';

import Button from'./abstracts/Button';
import TextField from'./abstracts/TextField';

import {
  Card,
  CardActions,
  List,
} from '@material-ui/core';

import RemoteSearch from './RemoteSearch.js';
import CardItem from './SituationCardItem.js';

import {
  API_ENDPOINT,
} from '../share/config.js';

// Config for remote search
const riderSearchEndPoint = API_ENDPOINT + '/riders?q=';

class SituationEditCardItem extends Component {
  constructor(props) {
    super(props);

    this.state = {
      id: props.id,
      title: props.title,
      subTitle: props.subTitle,
      desc: props.desc,
      items: props.items,
    };
  }

  riderSearchSelected(selection) {
    // Make selection into a object
    if (typeof selection === 'string') {
      selection = {
        name: selection,
        id: + new Date(),
      }
    }

    // Add to the items
    this.setState({
      items: [selection].concat(this.state.items),
    });
  }

  save() {
    console.log('going to save situation')
    let newSituation = {
      id: this.state.id,
      title: this.state.title,
      subTitle: this.state.subTitle,
      desc: this.state.desc,
      items: this.state.items,
    };

    // If its save it
    if (this.state.id) {
      this.props.edit(this.state.id, newSituation);
    } else {
      newSituation.id = + new Date();
      this.props.add(newSituation);
    }

    // Going to cacnel it as well
    this.props.onCancel();
  }

  remove(removeItem) {
    console.log('going to remove this item', removeItem);
    // Filter out the remove item
    const newItems = this.state.items.filter((item) => {
      return item.id !== removeItem.id;
    });

    // Set the new item states
    this.setState({
      items: newItems,
    });
  }

  render() {

    return (
      <Card>
        <CardActions>
          <Button
            label="Save Situation"
            secondary={true}
            onClick={this.save.bind(this)}
          />
          <Button
            label="Cancel"
            onClick={this.props.onCancel}
          />
        </CardActions>
        <TextField
          hintText="Title"
          floatingLabelText="Title"
          value={this.state.title}
          onChange={(e, newValue) => this.setState({title: newValue})}
        />
        <br />

        <TextField
          hintText="Sub title"
          floatingLabelText="Sub title"
          value={this.state.subTitle}
          onChange={(e, newValue) => this.setState({subTitle: newValue})}
        />
        <br />

        <TextField
          hintText="Description"
          floatingLabelText="Description"
          value={this.state.desc}
          onChange={(e, newValue) => this.setState({desc: newValue})}
        />
        <br />

        <RemoteSearch
          clearAfterSelection={true}
          endPoint={riderSearchEndPoint}
          floatingLabelText="Add Rider..."
          onSelected={this.riderSearchSelected.bind(this)}
        />

        <List>
          {this.state.items.map((item) =>
            <CardItem
              key={item.id}
              item={item}
              onRemove={this.remove.bind(this)}
            />
          )}
        </List>
      </Card>
    )
  }
}

SituationEditCardItem.defaultProps = {
  id: null,
  title: '',
  subTitle: '',
  desc: '',
  items: [],
  onCancel: e => true,
}

export default SituationEditCardItem;
