import React, { Component } from 'react';
import {List, ListItem} from '@material-ui/core';
import {
  Link,
} from 'react-router-dom';

class Home extends Component {
  constructor(props) {
    super(props);

    // Get the races if there is non
    if (props.races.length === 0) {
      console.log('home, going to get race');
    }
      props.getRaces();
  }

  changeTitle() {
    this.props.appBarTitle('Home');
  }

  componentWillMount() {
    this.changeTitle();
  }

  componentWillUpdate() {
    this.changeTitle();
  }

	// Render the component
  render() {
    const {
      races,
    } = this.props;

		// Render the tags
    return (
      <div>
        <List>
        { races.map((race) => (
          <ListItem key={race.slug}>
            <Link to={`/races/${race.slug}/`}>
              { race.name }
            </Link>
          </ListItem>
        )) }
        </List>
      </div>
    );
  }
}

export default Home;
