import React, { Component } from 'react';
import ResultEntry from './ResultEntry.js';
import EditResult from  '../components/EditResult.js';
import ResultTable from './ResultTable.js';

import {
  Button,
  MenuItem,
  List,
  Card,
  CardActions,
  CardHeader,
  CardContent,
  Select,
} from '@material-ui/core';

export default class ResultCard extends Component {
  constructor(props) {
    super(props);

    this.state = {
      edit: false,
    }
  }

  remove() {
    console.log('going to remove this result', this.props.id);
    if (this.props.id) {
      this.props.onRemove(this.props.id);
    }
  }

  render() {
    const { id, name, type, description, entries} = this.props;

    let itemContent = null;

		if (entries.length > 0) {
      itemContent = <List>
          {entries.map((item) =>
            <ResultEntry
              key={item.id}
              item={item}
              displayRemoveButton={false}
            />
          )}
        </List>
		}

    let cardContent = <Card>
          <CardHeader
            title={name}
          />

        <Select
          disabled={true}
          floatingLabelText="Type"
          value={type}
        >
          <MenuItem value="time" primaryText="Time" />
          <MenuItem value="point" primaryText="Point" />
          <MenuItem value="teamTime" primaryText="Team Time" />
          <MenuItem value="teamPoint" primaryText="Team Point" />
          <MenuItem value="none" primaryText="None" />
        </Select>

          {description &&
            <CardContent>
                {description}
              </CardContent>
          }

          <CardContent>
            {itemContent}

            <ResultTable data={entries} type={type} />
          </CardContent>
          <CardActions>
            <Button
              label="Edit"
              onClick={e => this.setState({edit: true})}
            />

            <Button
              variant="contained"
              label="Delete"
              secondary={true}
              onClick={this.remove.bind(this)}
            />
          </CardActions>
        </Card>

    if (this.state.edit) {
      cardContent = <EditResult
        id={id}
        name={name}
        type={type}
        description={description}
        entries={entries}
        onSave={e => this.props.onSave(e)}
        onCancel={e => this.setState({edit: false})}
      />
    }

    console.log('card content', cardContent);
    return cardContent
  }
}
