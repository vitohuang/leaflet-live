// @flow
import React from 'react';
import _ from 'lodash';
import LiveMarker from './LiveMarker.js';
import LiveLine from './LiveLine.js';

// Live layer
const LiveLayer = ({ locations }) => {
  console.log('live layer', locations);

  if (!locations || locations.length < 1) {
    return <div></div>;
  }

  // Sorting the locations
  const polyline = _.sortBy(locations, 'timestamp').map(l => l.data);

  console.log('location polyline', polyline);
  const currentPos = _.last(polyline);

  return <div className="live-layer">
      <LiveMarker center={currentPos} />
      <LiveLine positions={polyline} color="orange" />
  </div>
}

export default LiveLayer;
