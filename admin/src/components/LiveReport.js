import React from 'react';
import LocationInput from '../containers/LocationInput.js';
import Posts from '../containers/Posts.js';
import PostEditor from '../containers/PostEditor.js';

const LiveReport = () => {
  return (
    <div className="live-report">
      <LocationInput/>

      <div className="input-container">
        <PostEditor />
      </div>

      <Posts />
    </div>
  );
}

export default LiveReport;
