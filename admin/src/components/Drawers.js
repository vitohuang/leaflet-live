import React from 'react';
import { withRouter } from 'react-router';

import {
  Drawer,
  List,
	ListItem,
	ListItemText,
  Divider,
	IconButton,
  CssBaseline,
	withStyles,
} from '@material-ui/core';
import { Sidebar } from '../Routes.js';

import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';
import ChevronRightIcon from '@material-ui/icons/ChevronRight';

// Router
import {
  Link,
} from 'react-router-dom';

import ListItemLink from './ListItemLink.js';

const drawerWidth = 240;

const styles = theme => ({
  root: {
    display: 'flex',
  },
  appBar: {
    transition: theme.transitions.create(['margin', 'width'], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
  },
  appBarShift: {
    width: `calc(100% - ${drawerWidth}px)`,
    marginLeft: drawerWidth,
    transition: theme.transitions.create(['margin', 'width'], {
      easing: theme.transitions.easing.easeOut,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  menuButton: {
    marginLeft: 12,
    marginRight: 20,
  },
  hide: {
    display: 'none',
  },
  drawer: {
    width: drawerWidth,
    flexShrink: 0,
  },
  drawerPaper: {
    width: drawerWidth,
  },
  drawerHeader: {
    display: 'flex',
    alignItems: 'center',
    padding: '0 8px',
    ...theme.mixins.toolbar,
    justifyContent: 'flex-end',
  },
  content: {
    flexGrow: 1,
    padding: theme.spacing.unit * 3,
    transition: theme.transitions.create('margin', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
    marginLeft: -drawerWidth,
  },
  contentShift: {
    transition: theme.transitions.create('margin', {
      easing: theme.transitions.easing.easeOut,
      duration: theme.transitions.duration.enteringScreen,
    }),
    marginLeft: 0,
  },
});

const Drawers = ({ race, stage, toggleMenu, menuOpen, location, logout, classes, history }) => {

  // Figure out the current route and change the sidebar respectively
  let currentLocation = 'home';
  const path = location.pathname;
  if (path.match('/races/.*/stages/.*')) {
    currentLocation = 'stage';
  } else if (path.match('/races/.*/')) {
    currentLocation = 'race';
  }

  console.log("path", location);
  const routes = Sidebar[currentLocation];
  //todo: need to replace the race name and stage name with the real one

const clickNav = (path) => {
	console.log('this is got clicked', path);
  toggleMenu();
  history.push(path);
}

  // Get the drawer
	return (
    <Drawer
      anchor="left"
      variant="persistent"
      open={menuOpen}
			className={classes.drawer}
			classes={{
        paper: classes.drawerPaper,
      }}
    >

			<div className={classes.drawerHeader}>
				<IconButton onClick={e => toggleMenu()}>
					<ChevronLeftIcon />
				</IconButton>
			</div>

      <Divider />

      <List component="nav">
        {routes.map((route, index) => (
          <ListItem
            key={index}
            onClick={(e) => clickNav(route.path.replace(':raceSlug', race.settings.slug).replace(':stageSlug', stage.settings.slug))}
          >
            {route.name}
          </ListItem>
        ))}

				<ListItem onClick={e => clickNav('/profile')} key={100001}>
          Profile
        </ListItem>

				<ListItem key={1000002} button onClick={e => logout()}>
					<ListItemText primary="Logout" />
				</ListItem>
      </List>
    </Drawer>
	)
}

export default withStyles(styles)(withRouter(Drawers));
