import React, { Component } from 'react';
import {
  BrowserRouter as Router,
  Route,
} from 'react-router-dom';

import AppBar from '../containers/AppBar.js';
import Loading from '../containers/Loading.js';
import Drawers from '../containers/Drawers.js';
import Profile from '../containers/Profile.js';
import Login from '../containers/Login.js';

// Routes
import Routes, { RouteWithSubRoutes } from '../Routes.js';

const styles = {
	app: {
		'fontFamily': 'Helvetica Neue',
    /*
		'display': 'flex',
		'alignItems': 'center',
		'justifyContent': 'center',
		'flexDirection': 'column',
    */
	},
}

// Basename - becaseu on live its inside /admin
let basename = '';
if (process.env.NODE_ENV !== 'production') {
  basename = '/admin';
}

// The app
class Overview extends Component {

	// Render the component
  render() {
    // Default to login screen
    let content = <Login />;

    // If its logged in, then show everything
    if (this.props.loggedIn) {
      content = <Router basename={basename}>
            <div style={styles.app}>
              <Loading />
              <AppBar />
              <Drawers />

              <div className="app-content">

             {Routes.map((route, i) => (
                <RouteWithSubRoutes key={i} {...route}/>
              ))}

              <Route exact path="/profile" component={Profile}/>

              </div>
            </div>
          </Router>
    }

		// Render the tags
    return content;
  }
}

export default Overview;
