import React, { Component } from 'react';
import {
  Button,
  TextField,
} from '@material-ui/core';

import {
  API_ENDPOINT,
} from '../share/config.js';

class Login extends Component {
  constructor(props) {
    super(props);

    this.state = {
      userName: '',
      password: '',
      inputsTouched: false,
    }
  }

  inputChanged(type, val) {
    this.setState({
      [type]: val,
      inputsTouched: true,
    })
  }

  login() {
    console.log('going to login');
    // Todo do empty check etc

    // Set the input touch back to false
    this.setState({
      inputsTouched: false,
    })

    // Login
    this.props.login(
      this.state.userName,
      this.state.password,
    );
  }

  componentWillReceiveProps() {
    console.log('component will receive props');
    this.setState({
      inputsTouched: false,
    })
  }

  onKeyPress(event) {
    if (event.charCode === 13) {
      event.preventDefault();
      this.login();
    }
  }

	// Render the component
  render() {
    const {
      loggedIn,
    } = this.props;

    console.log('inside login', loggedIn, this.state);
    let errorMessage = null;
    if (loggedIn === false && this.state.inputsTouched !== true) {
      errorMessage = <p className="error-message">Incorrect credentials</p>
    }

		// Render the tags
    return (
      <div id="login-container">
        <div id="login-box">

          <p className="social-login">
            <a href={API_ENDPOINT + "/auth/facebook"}>
							<img src="/img/fb.png" alt="fb" />
						</a>
						<a href={API_ENDPOINT + "/auth/twitter"}>
							<img src="/img/twitter.png" alt="twitter" />
						</a>
						<a href={API_ENDPOINT + "/auth/google"}>
							<img src="/img/google.png" alt="google" />
						</a>
          </p>

          {errorMessage}

          <TextField
            label="Username"
            margin="dense"
            fullWidth
            InputLabelProps={{
              shrink: true,
            }}
            value={this.state.userName}
            onChange={(e) => this.inputChanged('userName', e.target.value)}
          />
          <br/>

          <TextField
            type="password"
            label="Password"
            fullWidth
            value={this.state.password}
            onChange={(e) => this.inputChanged('password', e.target.value)}
            InputLabelProps={{
              shrink: true,
            }}
          />
          <br/>

          <Button
            variant="contained"
            className="login-button"
            color="secondary"
            onClick={this.login.bind(this)}
          >
            Login
          </Button>
        </div>
      </div>
    );
  }
}

export default Login;
