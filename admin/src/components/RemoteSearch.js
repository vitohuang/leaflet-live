import React, { Component } from 'react';
import AsyncSelect from 'react-select/lib/Async';

import {
  API_ENDPOINT,
} from '../share/config.js';

const formatResult = data => {
  return data;
}
const options = [
  { id: 1, value: 'chocolate', label: 'Chocolate' },
  { id: 2, value: 'strawberry', label: 'Strawberry' },
  { id: 3, value: 'vanilla', label: 'Vanilla' }
];

class RemoteSearch extends Component {
  constructor(props) {
    super(props);

    this.handleChange = this.handleChange.bind(this);
    this.promiseOptions = this.promiseOptions.bind(this)

    // Refs
    this.selector = React.createRef();

    this.state = {
      value: '',
    };
  }

  promiseOptions(inputValue) {
    console.log('this is the opromise options', inputValue);
    return new Promise(resolve => {
      if (inputValue) {
        fetch(this.props.endPoint + inputValue, {
          credentials: 'include',
        })
          .then((response) => {
            return response.json();
          })
          .then((data) => {
            resolve(options);
            //resolve(formatResult(data));
          })
          .catch((error) => {
            console.log('there is something wrong searching for ', inputValue, error);
          });
      } else {
        resolve([]);
      }
    });
  }

  handleChange(option, event) {
    let value = option;
    if (this.props.clearAfterSelection) {
      value = null;
    }

    // Tell others about the select as well
    this.props.onSelected(value);

    this.setState({
      value,
    });

  }

  render() {
    const {
      value,
    } = this.state;

    return (
      <AsyncSelect
        ref={this.selector}
        value={value}
        cacheOptions
        defaultOptions
        loadOptions={this.promiseOptions}
        onChange={this.handleChange}
      />
    )
  }
}

// Default props
RemoteSearch.defaultProps = {
  maxResults: 15,
  floatingLabelText: 'Search...',
	endPoint: API_ENDPOINT + '/riders?q=',
  dataSourceConfig: {
    text: 'name',
    value: 'id',
  },
  clearAfterSelection: false,
}

export default RemoteSearch;
