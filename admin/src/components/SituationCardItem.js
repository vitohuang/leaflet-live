import React, { Component } from 'react';
import {
  IconButton,
  Avatar,
  ListItem,
  Icon
} from '@material-ui/core';

class SituationCardItem extends Component {
  remove() {
    this.props.onRemove(this.props.item);
  }

  render() {
    const { item, displayRemoveButton } = this.props;

    // List item properties
    let listItemProps = {
      primaryText: item.name,
      leftAvatar: <Avatar src={item.avatar} />,
    };

    // Check if need to display remove button
    if (displayRemoveButton) {
      listItemProps.rightIconButton =
        <IconButton
          touch={true}
          onClick={this.remove.bind(this)}
        >
          <Icon>remove</Icon>
        </IconButton>
    }

    return (
            <ListItem
              {...listItemProps}
            />
    );
  }
}

SituationCardItem.defaultProps = {
  displayRemoveButton: true,
}

export default SituationCardItem;
