import React, { Component } from 'react';
import { Editor } from 'react-draft-wysiwyg';
import { convertFromHTML, EditorState, ContentState } from 'draft-js';
import 'react-draft-wysiwyg/dist/react-draft-wysiwyg.css';
import { convertToRaw } from 'draft-js';
import draftToHtml from 'draftjs-to-html';
import moment from 'moment';

import {
  DatePicker,
  MuiPickersUtilsProvider,
} from 'material-ui-pickers';

import MomentUtils from '@date-io/moment';
import Button from'./abstracts/Button';
import TextField from'./abstracts/TextField';

import {
  Select,
  MenuItem,
} from '@material-ui/core';

import {
  uploadFile,
} from '../share/utils.js';

const disciplines = [
  {
    text: 'Racing',
    value: 'racing',
  },
  {
    text: 'Off road',
    value: 'offRoad',
  },
  {
    text: 'Triathlon',
    value: 'triathlon',
  },
  {
    text: 'Conference',
    value: 'conference',
  },
]

function genMenuItems (items) {
  return items.map((item, index) => (
    <MenuItem key={index} value={item.value}>
      {item.text}
    </MenuItem>
  ));
}

class RaceSettings extends Component {
  constructor(props) {
    super(props)

    this.handleFileUpload = this.handleFileUpload.bind(this);

    const propsData = this.getSettingsData(props.settings);
    // Set the state
    this.state = {
      _id: propsData._id,
      name: propsData.name,
      startDate: propsData.startDate,
      endDate: propsData.endDate,
      distance: propsData.distance,
      numberOfStages: propsData.numberOfStages,
      series: propsData.series,
      group: propsData.group,
      discipline: propsData.discipline || 'racing',
      edition: propsData.edition,
      description: propsData.description,
      coverImageUrl: propsData.coverImageUrl,
    }
  }

  // Get settings or defaults
  getSettingsData(props) {
    console.log('going to transform props', props);
    var {
      _id,
      name,
      startDate,
      endDate,
      distance,
      numberOfStages,
      series,
      group,
      discipline,
      edition,
      description,
      coverImageUrl,
    } = props;

    // Make it easy to format
    //startDate = moment(startDate);
    if (startDate) {
      startDate = new Date(startDate);
    }
    if (endDate) {
      endDate = new Date(endDate);
    }

    // Default description
    if (description) {
      let blocksFromHtml = convertFromHTML(description);
      description = EditorState.createWithContent(ContentState.createFromBlockArray(
        blocksFromHtml.contentBlocks,
        blocksFromHtml.entityMap
      ));
    } else {
      description = EditorState.createWithContent(ContentState.createFromText(''));
    }

    // Set the state
    return {
      _id,
      name,
      startDate: startDate,
      endDate: endDate,
      distance,
      numberOfStages,
      series,
      group,
      discipline,
      edition,
      description,
      coverImageUrl,
    }
  }

  handleFileUpload(event) {
    console.log('handle file upload', event);

    const file = event.target.files[0];

    console.log('the file', file);
    // Upload to server
    uploadFile(file)
      .then(data => {
        console.log('uploaded this is data', data, data.data.link);

        // Going to set the preview and save to setting
        this.setState({
          coverImageUrl: data.data.link,
        })
      })
      .catch(error => console.log(error));
  }

  componentWillReceiveProps(nextProps) {
    const propsData = this.getSettingsData(nextProps.settings);
    console.log('will receive props', propsData);

    this.setState({...propsData})
  }

  onEditorStateChange(editorState) {
    console.log('editor state changed', editorState);
    this.setState({
      description: editorState,
    });
  }

  save() {
    console.log('going to save settings', this.state);
    const startDate = moment(this.state.startDate);
    const endDate = moment(this.state.endDate);

    // Get the content state
    let description = '';
    const contentState = this.state.description.getCurrentContent();
    if (contentState.hasText()) {
      const rawContent = convertToRaw(contentState);
			description = draftToHtml(rawContent);
    }

    this.props.save({
      _id: this.state._id,
      name: this.state.name,
      startDate,
      endDate,
      distance: this.state.distance,
      numberOfStages: this.state.numberOfStages,
      series: this.state.series,
      group: this.state.group,
      discipline: this.state.discipline,
      edition: this.state.edition,
      description,
      coverImageUrl: this.state.coverImageUrl,
    });
  }

  render() {

    console.log(this.props.settings, 'race settings settings');
    return (

      <MuiPickersUtilsProvider utils={MomentUtils}>
      <div className="settings-container">
        <p>settings</p>
        <TextField
          hintText="Race Name"
          defaultValue={this.state.name}
          value={this.state.name}
          onChange={(e, val) => this.setState({name: val})}
          floatingLabelText="Race Name"
        />
        <br/>

        <DatePicker
          label="Start Date"
          value={this.state.startDate}
          onChange={(val) => this.setState({startDate: val})}
        />
        <DatePicker
          label="End Date"
          value={this.state.endDate}
          onChange={(val) => this.setState({endDate: val})}
        />
				<br />

        <TextField
          hintText="Distance"
          defaultValue={this.state.distance}
          value={this.state.distance}
          onChange={(e, val) => this.setState({distance: val})}
          floatingLabelText="Distance"
        />
        <br/>

        <TextField
          hintText="Number of Stages"
          defaultValue={this.state.numberOfStages}
          value={this.state.numberOfStages}
          onChange={(e, val) => this.setState({numberOfStages: val})}
          floatingLabelText="Number of Stages"
        />
        <br/>

        <TextField
          hintText="Series"
          defaultValue={this.state.series}
          value={this.state.series}
          onChange={(e, val) => this.setState({series: val})}
          floatingLabelText="Series"
        />
        <br/>

        <TextField
          hintText="Group"
          defaultValue={this.state.group}
          value={this.state.group}
          onChange={(e, val) => this.setState({group: val})}
          floatingLabelText="Group"
        />
        <br/>

        <Select
          inputProps={{
            name: "Discipline",
          }}
          value={this.state.discipline}
          onChange={(e) => this.setState({discipline: e.target.value})}
        >
          {genMenuItems(disciplines)}
        </Select>
        <br/>

        <TextField
          hintText="Edition"
          defaultValue={this.state.edition}
          value={this.state.edition}
          onChange={(e, val) => this.setState({edition: val})}
          floatingLabelText="Edition"
        />
        <br/>

        <h3>Description</h3>
        <Editor
          editorState={this.state.description}
          toolbarClassName="home-toolbar"
          wrapperClassName="home-wrapper"
          editorClassName="home-editor"
          onEditorStateChange={this.onEditorStateChange.bind(this)}
					toolbar={{
						inline: { inDropdown: true },
						list: { inDropdown: true },
						textAlign: { inDropdown: true },
						link: { inDropdown: true },
						history: { inDropdown: true },
					}}
        />

          <h3>Cover Image</h3>
          {this.state.coverImageUrl && (
            <img src={this.state.coverImageUrl} width="500"/>
          )}

          <input
            accept="image/*"
            className="class-input"
            id="contained-button-file"
            multiple
            type="file"
            onChange={this.handleFileUpload}
          />
          <label htmlFor="contained-button-file">
            <Button variant="contained" component="span" className="class-input">
              Upload
            </Button>
          </label>

        <div className="buttons">
          <Button
            variant="contained"
            label="Save"
            className="left"
            secondary={true}
            onClick={this.save.bind(this)}
          />
          <Button
            variant="contained"
            className="right"
            label="Delete this race"
            secondary={false}
            onClick={() => this.props.delete(this.state._id)}
          />
          <div className="clear"></div>
        </div>
      </div>
      </MuiPickersUtilsProvider>
    )
  }
}

export default RaceSettings;
