import React from 'react';
import {
  AppBar,
  Toolbar,
  IconButton,
	Typography,
} from '@material-ui/core';
import { withStyles } from '@material-ui/core/styles';
import MenuIcon from '@material-ui/icons/Menu';

// Get the status menu
import StatusMenu from './StatusMenu.js';

const styles = {
  root: {
    flexGrow: 1,
  },
  grow: {
    flexGrow: 1,
  },
  menuButton: {
    marginLeft: -12,
    marginRight: 20,
  },
};

const AppBarCom = ({ title, showStageStatus, stageStatus, changeStatus, toggleMenu, toggleSecondMenu, classes }) => {

  // Check if its current at the stage
  // Add the status menu if its on staging component
  let iconElement = false;
  if (showStageStatus && stageStatus) {
    iconElement = <StatusMenu currentStatus={stageStatus} changeStatus={changeStatus} />;
  }
	return (
      <AppBar position="static">
				<Toolbar>
					<IconButton onClick={(e) => toggleMenu()} className={classes.menuButton} color="inherit" aria-label="Menu">
						<MenuIcon />
					</IconButton>

					<Typography variant="h6" color="inherit" className={classes.grow}>
						{title}
					</Typography>

					{iconElement}
				</Toolbar>
			</AppBar>
	)
}

export default withStyles(styles)(AppBarCom);
