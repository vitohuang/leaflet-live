import React, { Component } from 'react';
import PropTypes from 'prop-types';
import update from 'immutability-helper';
import { DragDropContext } from 'react-dnd';
import HTML5Backend from 'react-dnd-html5-backend';
import Card from './DNDCard.js';

const style = {
  width: 400,
};

class DragDrop extends Component {
  static propTypes = {
    data: PropTypes.array.isRequired,
  }

  constructor(props) {
    super(props);
    this.moveCard = this.moveCard.bind(this);
    this.state = {
      cards: props.data,
    };
  }

  componentWillReceiveProps(nextProps) {
    this.setState({
      cards: nextProps.data,
    })
  }

  moveCard(dragIndex, hoverIndex) {
    const { cards } = this.state;
    const dragCard = cards[dragIndex];

    this.setState(update(this.state, {
      cards: {
        $splice: [
          [dragIndex, 1],
          [hoverIndex, 0, dragCard],
        ],
      },
    }));

    // Tell the parent
    this.props.onMove(this.state.cards);
  }

  render() {
    const { cards } = this.state;

    return (
      <div style={style}>
        {cards.map((card, i) => (
          <Card
            key={card.id}
            index={i}
            id={card.id}
            moveCard={this.moveCard}
          >
            {card.content}
          </Card>
        ))}
      </div>
    );
  }
}

export default DragDropContext(HTML5Backend)(DragDrop);
