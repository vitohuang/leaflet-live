import React, { Component } from 'react'
import {
  FeatureGroup,
  Circle,
} from 'react-leaflet'
import 'leaflet-draw/dist/leaflet.draw.css';
import { EditControl } from "react-leaflet-draw";
import L, { Map } from 'leaflet';
import popupContent from '../utils/popupContent.js';
import geojsonRewind from 'geojson-rewind';
import simplestyle from '../utils/simplestyle.js';
import * as d3 from 'd3';
import PropTypes from 'prop-types';
import _ from 'lodash';

// Generate icon from mapbox
function icon(fp, options) {
    fp = fp || {};

    var sizes = {
            small: [20, 50],
            medium: [30, 70],
            large: [35, 90]
        },
        size = fp['marker-size'] || 'medium',
        symbol = ('marker-symbol' in fp && fp['marker-symbol'] !== '') ? '-' + fp['marker-symbol'] : '',
        color = (fp['marker-color'] || '7e7e7e').replace('#', '');

    return L.icon({
        iconUrl: 'https://a.tiles.mapbox.com/v4/marker/' +
            'pin-' + size.charAt(0) + symbol + '+' + color +
            // detect and use retina markers, which are x2 resolution
            (L.Browser.retina ? '@2x' : '') + '.png?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXFhYTA2bTMyeW44ZG0ybXBkMHkifQ.gUGbDOPUN1v1fTs5SeOR4A',
        iconSize: sizes[size],
        iconAnchor: [sizes[size][0] / 2, sizes[size][1] / 2],
        popupAnchor: [0, -sizes[size][1] / 2]
    });
}

class DrawMap extends Component {
	bindPopup(l) {
    const context = {
      map: this.context.map,
      drawLayer: this.featureGroup.leafletElement,
      dataChanged: this.dataChanged.bind(this),
      update: this.update.bind(this),
    };

    // Bind the popup
		popupContent(l, L);

    l.on('popupopen', (e) => {
      var sel = d3.select(e.popup._contentNode);

      sel.selectAll('.cancel')
          .on('click', clickClose);

      sel.selectAll('.save')
          .on('click', saveFeature);

      sel.selectAll('.add')
          .on('click', addRow);

      sel.selectAll('.delete-invert')
          .on('click', removeFeature);

      function clickClose() {
        context.map.closePopup(e.popup);
      }

      function saveFeature() {
        var obj = {};
        var table = sel.select('table.marker-properties');
        table.selectAll('tr').each(collectRow);
        function collectRow() {
            var inputs = d3.select(this).selectAll('input').nodes();
            if (inputs[0].value) {
                obj[inputs[0].value] =
                    losslessNumber(inputs[1].value);
            }
        }
        console.log('properties', obj);
        e.popup._source.feature.properties = obj;
        //context.data.set({map: context.drawLayer.toGeoJSON()}, 'popup');
        context.dataChanged(context.drawLayer.toGeoJSON());
        context.update();
        console.log({map: context.drawLayer.toGeoJSON()}, 'save feature popup');
        context.map.closePopup(e.popup);
      }

      function addRow() {
        var tr = sel.select('table.marker-properties tbody')
            .append('tr');

        tr.append('th')
            .append('input')
            .attr('type', 'text');

        tr.append('td')
            .append('input')
            .attr('type', 'text');
      }

      function removeFeature() {
        if (e.popup._source && context.drawLayer.hasLayer(e.popup._source)) {
            context.drawLayer.removeLayer(e.popup._source);
            //context.dataChanged(context.drawLayer.toGeoJSON());
            //context.data.set({map: context.drawLayer.toGeoJSON()}, 'popup');
            context.update();
            console.log({map: context.drawLayer.toGeoJSON()}, 'remove feature popup');
        }
      }

      function losslessNumber(x) {
          var fl = parseFloat(x);
          if (fl.toString() === x) return fl;
          else return x;
      }

    })
	}
	onEdited(e) {
		console.log('on edited', e);
		const layers = e.layers;
		layers.eachLayer((layer) => {
			console.log('layer', layer);
		})

    this.update();
	}

	layerToGeoJSON(layer) {
		var features = [];
		layer.eachLayer(collect);

		function collect(l) {
      if ('toGeoJSON' in l)
        features.push(l.toGeoJSON());
    }

		return {
				type: 'FeatureCollection',
				features: features
		};
	}

  geojsonToLayer(geojson, layer) {
    if (!_.isEmpty(geojson)) {
      const that = this;

      // Clear the layer
      layer.clearLayers();
      L.geoJson(geojson, {
          style: simplestyle.style,
          pointToLayer: function(feature, latlon) {
            console.log('geojson feature', feature);
              let props = {};
              if (feature.properties) {
                props = feature.properties;
              }

              if (props.Title) {
                // Return a marker
                return L.marker(latlon, {
                  icon: icon(props),
                  title: props.Title || '',
                })
              }
          }
      }).eachLayer(add);

      console.log('geojsonto layer 11', that, this);
      // Add each layer
      function add(l) {
      console.log('geojsonto layer fjdkfjd', that, this);
          that.bindPopup(l);
          l.addTo(layer);
      };
    }
  }

  update() {
    console.log('going to update the draw map');
    // Get the current layer
    const drawLayer = this.featureGroup.leafletElement;
    console.log('update things', drawLayer);
    let jsonData = drawLayer.toGeoJSON();
    console.log("everthing on the map json", jsonData);
    jsonData = geojsonRewind(jsonData);
    console.log("after rewind - everthing on the map json", jsonData);

		// Redraw the layer
		this.geojsonToLayer(jsonData, drawLayer);

    // Convert it back to json
    jsonData = this.layerToGeoJSON(drawLayer);
    console.log("layer to geojson", jsonData);
    this.dataChanged(jsonData);
  }

  dataChanged(data) {
    console.log('data changed in the draw map');
    // Tell others
    if (this.props.onChange) {
      this.props.onChange(data);
    }
  }

	onCreated(e) {

		console.log('on created', e, e.layerType);
		const layer = e.layer;
		console.log('layer', layer);
		console.log('layer json', layer.toGeoJSON());
		this.bindPopup(layer);

    this.update();
	}

	onDeleted(e) {
		console.log('on deleted', e);
		const layers = e.layers;
		layers.eachLayer((layer) => {
			console.log('layer', layer);
		})

    this.update();
	}

	onMounted(e) {
		console.log('on mounted', e, this.drawLayer);
	}

  componentDidMount() {
    console.log('draw map component is mounted', this, this.featureGroup);
    // Add the data to feature group
    // Redraw the layers
    this.geojsonToLayer(this.props.data, this.featureGroup.leafletElement);

  }
  render() {
    console.log('render draw map');
    return (
			<FeatureGroup ref={g => this.featureGroup = g} color="purple">
        <Circle center={[52.51, -0.06]} radius={800} />
				<EditControl
					position='topright'
					onEdited={this.onEdited.bind(this)}
					onCreated={this.onCreated.bind(this)}
					onDeleted={this.onDeleted.bind(this)}
					onMounted={this.onMounted.bind(this)}
					draw={{
					rectangle: false
				}}
				/>
			</FeatureGroup>
		)
	}
}

// Get data from context api
DrawMap.contextTypes = {
  map: PropTypes.instanceOf(Map),
  layerContainer: PropTypes.shape({
		addLayer: PropTypes.func.isRequired,
		removeLayer: PropTypes.func.isRequired,
	}),
  popupContainer: PropTypes.object,
}

export default DrawMap;
