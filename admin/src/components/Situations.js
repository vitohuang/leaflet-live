import React, { Component } from 'react';
import AddIcon from '@material-ui/icons/Add';

import Button from'./abstracts/Button';

import {
  IconButton,
} from '@material-ui/core';

import SituationCard from './SituationCard.js';
import EditCardItem from  '../containers/SituationEditCardItem.js';
import DND from './dragdrop/index.js';

export default class Situations extends Component {
  constructor(props) {
    super(props);

    this.state = {
      showAdd: false,
    }
  }

  onMove(data) {
    console.log('situation moved', data);
    const newSituations = data.map((item) => {
      return {
        id: item.id,
        title: item.title,
        subTitle: item.subTitle,
        desc: item.desc,
        items: item.items,
      }
    });

    this.props.rearrange(newSituations);
  }

  getData() {
    return this.props.situations.map((item) => {
      item.content = <SituationCard
        id={item.id}
        title={item.title}
        subTitle={item.subTitle}
        desc={item.desc}
        items={item.items}
        onRemove={this.props.remove}
      />

        return item;
    });
  }

  save() {
    // Filter out the content
    const situations = this.props.situations.map((situation) => {
      return {
        id: situation.id,
        title: situation.title,
        subTitle: situation.subTitle,
        desc: situation.desc,
        items: situation.items,
      }
    });

    console.log('going to save the situations - push it to the pipe', situations);
    this.props.save(situations);
  }

  render() {
    // todo:need to call get data to see set the content
    // not sure why
    this.getData();

    return (
      <div>
        <h1>Situations</h1>

        <DND data={this.props.situations} onMove={this.onMove.bind(this)}/>

        <Button
          variant="contained"
          label="Save"
          secondary={true}
          onClick={this.save.bind(this)}
        />

        {!this.state.showAdd &&
          <IconButton
            onClick={(e) => this.setState({showAdd: true})}
          >
            <AddIcon />
          </IconButton>
        }

        { this.state.showAdd &&
            <EditCardItem
              onCancel={e => this.setState({showAdd: false})}
            />
        }
      </div>
    );
  }
}
