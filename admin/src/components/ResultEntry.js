import React, { Component } from 'react';
import RemoveIcon from '@material-ui/icons/Remove';

import {
  Avatar,
  IconButton,
  ListItem,
} from '@material-ui/core';

class ResultEntry extends Component {
  remove() {
    this.props.onRemove(this.props.item);
  }

  render() {
    const { item, displayRemoveButton } = this.props;

    return (
            <ListItem>
              <Avatar src={item.avatar} />

              {item.name}

              <IconButton onClick={this.remove.bind(this)}>
                <RemoveIcon/>
              </IconButton>
            </ListItem>
    );
  }
}

ResultEntry.defaultProps = {
  displayRemoveButton: true,
}

export default ResultEntry;
