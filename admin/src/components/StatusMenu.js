import React from 'react';
import {
  IconButton,
  MenuItem,
  Select,
} from '@material-ui/core';

// Icons
import PreLiveIcon from '@material-ui/icons/Delete';
import LiveIcon from '@material-ui/icons/Delete';
import DraftIcon from '@material-ui/icons/Delete';
import FinishedIcon from '@material-ui/icons/Delete';

const StatusMenu = ({currentStatus, changeStatus}) => {

  // Figure out the current icon
  let statusIcon = '';
  switch (currentStatus) {
    case 'preLive':
      statusIcon = <PreLiveIcon />;
      break;
    case 'live':
      statusIcon = <LiveIcon />;
      break;
    case 'finished':
      statusIcon = <FinishedIcon />;
      break;
    default:
      statusIcon = <DraftIcon />;
      break;
  }

  console.log('currentstatus', currentStatus, statusIcon);
  return (
    <Select
      value={currentStatus}
      onChange={e => changeStatus(e.target.value)}
      displayEmpty
    >
      <MenuItem value="draft" onClick={(e) => changeStatus('draft')}>
        <DraftIcon />
          Draft
      </MenuItem>

      <MenuItem value="preLive" onClick={(e) => changeStatus('preLive')}>
        <PreLiveIcon />
        Pre Live
      </MenuItem>

      <MenuItem value="live" onClick={(e) => changeStatus('live')}>
        <LiveIcon />
        Live
      </MenuItem>

      <MenuItem value="finished" onClick={(e) => changeStatus('finished')}>
        <FinishedIcon />
        Finished
      </MenuItem>
    </Select>
  )
}

export default StatusMenu;
