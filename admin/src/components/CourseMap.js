import React, { Component } from 'react'
import { LayersControl, GeoJSON, Map, TileLayer, Marker, Popup } from 'react-leaflet'
import FullscreenControl from 'react-leaflet-fullscreen';
import L from 'leaflet';
import Dropzone from 'react-dropzone';
import 'react-leaflet-fullscreen/dist/styles.css'
import 'leaflet.elevation/dist/Leaflet.Elevation-0.0.2.src.js';
import 'leaflet.elevation/dist/Leaflet.Elevation-0.0.2.css';

import './CourseMap.css';
import LiveLayer from '../containers/LiveLayer.js';
import DrawMap from './DrawMap.js';
import {
  API_ENDPOINT,
} from '../share/config.js';

import Button from'./abstracts/Button';

// End point for elevation decoding
const endPoint = API_ENDPOINT + '/geo/elevation';
const cartoDBTiles = 'https://cartodb-basemaps-{s}.global.ssl.fastly.net/light_all/{z}/{x}/{y}.png'
const mapAttr = '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors';
//const osmTiles ='http://{s}.tile.osm.org/{z}/{x}/{y}.png';

var elevationControl = L.control.elevation({
  	position: "bottomleft",
	theme: "steelblue-theme", //default: lime-theme
	width: 400,
	height: 125,
	margins: {
		top: 10,
		right: 30,
		bottom: 30,
		left: 50
	},
	useHeightIndicator: true, //if false a marker is drawn at map position
	interpolation: "linear", //see https://github.com/mbostock/d3/wiki/SVG-Shapes#wiki-area_interpolate
	hoverNumber: {
		decimalsX: 3, //decimals on distance (always in km)
		decimalsY: 0, //deciamls on hehttps://www.npmjs.com/package/leaflet.coordinatesight (always in m)
		formatter: undefined //custom formatter function may be injected
	},
	xTicks: undefined, //number of ticks in x axis, calculated by default according to width
	yTicks: undefined, //number of ticks on y axis, calculated by default according to height
	collapsed: false,  //collapsed mode, show chart on click or mouseover
	imperial: false    //display imperial units instead of metric
});
console.log('Elevation control layer', elevationControl);

function featureContainElevation(feature) {
  let result = false;
  if (feature.geometry) {
    if (feature.geometry.type === 'LineString') {
      if (feature.geometry.coordinates) {
        // Every coordinate must have 3 values - lon, lat, elevation/altitude
        result = feature.geometry.coordinates.every((coord) => {
          return coord.length === 3;
        });
      }
    }
  }

  return result;
}

// Added elevation
let addedElevation = false;
class MapContainer extends Component {
  constructor(props) {
    super(props);

    this.state = {
      lat: -44.696476296,
      lng: 169.13693,
      zoom: 13,
      drawData: props.drawData,
      course: props.course,
    }
  }

  addElevation(geoJson) {
    const leafletMap = this.leafletMap.leafletElement;
    if (geoJson) {
      elevationControl.addTo(leafletMap);
      const courseGeo = L.geoJson(geoJson, {
        onEachFeature: (feature, layer) => {
          console.log('feature and layer', feature, layer);
          // Check for the geometry and only add the elevation map if its line string and contain elevation data
          if (!addedElevation && featureContainElevation(feature)) {
            console.log('going to add elevation');
            elevationControl.addData.call(elevationControl, feature, layer);

            addedElevation = true;
          }
          console.log("going to add content");
          layer.bindPopup("This is the stage route");
        }
      });
      courseGeo.addTo(leafletMap);

      // Zoom th map, so its able to see everything
      leafletMap.fitBounds(courseGeo.getBounds());
      console.log("component did mount this leaflet element", leafletMap);
      console.log(leafletMap.getZoom());
    }
  }

  drawChange(data) {
    console.log('drawing is changed', data);
    this.setState({
      drawData: data,
    })
  }

  componentWillReceiveProps(nextProps) {
    // Only update course and drawData
    let newState = {};
    if (nextProps.course !== this.state.course) {
      newState.course = nextProps.course;
    }

    if (nextProps.drawData !== this.state.drawData) {
      newState.drawData = nextProps.drawData;
    }

    if (Object.keys(newState).length > 0) {
      this.setState(newState)
    }
  }

  componentDidMount() {
    console.log("did mount course map component", this.props);

    // Add the elevation to the map
    if (this.state.course) {
      console.log('going to add elevation');
      this.addElevation(this.state.course);
    }
  }

  componentDidUpdate() {
    const leafletMap = this.leafletMap.leafletElement;
    console.log("did update course map component", leafletMap);

    // Add the elevation to the map
    /*
    if (this.props.course) {
      console.log('going to add elevation');
      debugger;
      this.addElevation(this.props.course);
    }
    */
  }

  renderDrawingToLayerControl() {
    // Put the draw data into layer control
    let drawedLayers = '';
    if (this.state.drawData && this.state.drawData.features) {
      drawedLayers = this.state.drawData.features.map((data, index) => {
        console.log('features data', data);
        // Figure out the names
        let name = `Drawing ${index}`;
        if (data.properties.name) {
          name = data.properties.name;
        }

        return <LayersControl.Overlay name={name} checked={true}>
              <GeoJSON data={data} />
        </LayersControl.Overlay>
      });
    }

    return drawedLayers;
  }

	onDrop(acceptedFiles, rejectedFiles) {
		console.log("you dropped something", acceptedFiles, rejectedFiles);

		if (acceptedFiles.length > 0) {
			// Start a file reader
			var reader = new FileReader();
			reader.onload = () => {
				var geoJson = JSON.parse(reader.result);

        console.log('going to save the into the state', geoJson);
        /*
        this.setState({
          course: geoJson,
        });
        */

        // Only get the linestring
        if (geoJson.features[0].geometry.type === 'LineString') {
          fetch(endPoint, {
            method: 'POST',
            credentials: 'include',
            body: JSON.stringify(geoJson.features[0].geometry.coordinates),
          }).then(r => r.json())
            .then(result => {
              console.log('got result from server', result);
              console.log('json in string');
              console.log(JSON.stringify(geoJson));

              // Only added when its success
              if (result.success) {
                geoJson.features[0].geometry.coordinates = result.data;
                this.addElevation(geoJson);

                this.setState({
                  course: geoJson,
                });
              }

            })
            .catch((error) => {
              console.log('cant decode', error);
            })
        }
      }

			acceptedFiles.forEach(file => {
				// Read the content
				reader.readAsText(file);

				// Delete the preview to avoid memory leaks
				window.URL.revokeObjectURL(file.preview)
			});
		}
	}

  save() {
    console.log("going to save the course map");
    const {
      drawData,
      course,
    } = this.state;

    this.props.save(course, drawData);
  }

  render() {
    console.log("course map component", this.props);
    const position = [this.state.lat, this.state.lng]
    return (
      <div className="course-map-container">
      <Dropzone
        disableClick
        style={{}}
        onDrop={this.onDrop.bind(this)}
      >
        <Map ref={m => { this.leafletMap = m; }} center={position} zoom={this.state.zoom}>
          <TileLayer
            attribution={mapAttr}
            url={cartoDBTiles}
          />

          <FullscreenControl position="topleft" />

          <LayersControl position='topright'>
            <LayersControl.BaseLayer name='OpenStreetMap.BlackAndWhite'>
              <TileLayer
                attribution='&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
                url='http://{s}.tiles.wmflabs.org/bw-mapnik/{z}/{x}/{y}.png'
              />
            </LayersControl.BaseLayer>
            <LayersControl.BaseLayer name='OpenStreetMap.Mapnik'>
              <TileLayer
                attribution='&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
                url='http://{s}.tile.osm.org/{z}/{x}/{y}.png'
              />
            </LayersControl.BaseLayer>
            <LayersControl.Overlay name='Marker with popup'>
              <Marker position={[51.51, -0.06]}>
                <Popup>
                  <span>A pretty CSS3 popup. <br/> Easily customizable.</span>
                </Popup>
              </Marker>
            </LayersControl.Overlay>
          </LayersControl>

          <DrawMap onChange={this.drawChange.bind(this)} data={this.state.drawData}/>
        </Map>

      </Dropzone>
        <div className="clear"></div>
        <div className="action-row">
          <Button
            variant="contained"
            label="Publish Map"
            secondary={true}
            onClick={this.save.bind(this)}
          />
        </div>
      </div>
    )
  }
}

export default MapContainer
