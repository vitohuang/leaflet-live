import React, { Component } from 'react';
import { Editor } from 'react-draft-wysiwyg';
import { EditorState, ContentState } from 'draft-js';
import 'react-draft-wysiwyg/dist/react-draft-wysiwyg.css';
import './PostEditor.css';
import { convertToRaw } from 'draft-js';

import Button from'./abstracts/Button';
import TextField from'./abstracts/TextField';

import {
  API_ENDPOINT,
} from '../share/config.js';

import {
  uploadFile,
} from '../share/utils.js';

const styles = {
	publishButton: {
		'fontSize': '2em',
		'margin': 'auto',
		'width': '100%',
		'height': '100%',
		'textAlign': 'center',
	}
}

const endPoint = API_ENDPOINT;
class PostEditor extends Component {
  constructor(props) {
    super(props);

    // Editor state
    this.state = {
        distance: '',
        editorState: EditorState.createWithContent(ContentState.createFromText('')),
    }
  }

  onEditorStateChange(editorState) {
    console.log('editor state changed', editorState);

    const contentState = editorState.getCurrentContent();
    console.log('content state', contentState);

    const rawContent = convertToRaw(contentState);
    console.log('rawContent', rawContent);
    console.log('rawContent json', JSON.stringify(rawContent));

    this.setState({
        editorState,
    });
  }

  uploadS3File(s3Data, file) {
    // Put the object directly to the s3
    return new Promise((resolve, reject) => {
        setTimeout(() => resolve('done'), 2000);
  /*
    const xhr = new XMLHttpRequest();
    xhr.open('PUT', signedRequest);
    xhr.onreadystatechange = () => {
    if(xhr.readyState === 4){
      if(xhr.status === 200){
      document.getElementById('preview').src = url;
      document.getElementById('avatar-url').value = url;
      }
      else{
      alert('Could not upload file.');
      }
    }
    };
    xhr.send(file);
  */
    })
	}

  signS3(file) {
    return new Promise((resolve, reject) => {
        // Fetch sign s3
        fetch(endPoint + `/sign-s3?name=${file.name}&type=${file.type}`, {
          credentials: 'include',
        })
            .then((response) => {
                return response.json();
            })
            .then((data) => {
                console.log('the data', data);
                if (data.url && data.signedRequest) {
                    // Upload file
                    this.uploadS3File(data, file)
                        .then(() => {
                            console.log('its all done');
                            console.log('going to resolve the data');
                            resolve({
                                data: {
                                    link:'http://lorempixel.com/400/200/sports/',
                                }
                            })
                        })
                        .catch((error) => {
                            console.log('there is something wrong while uploading file', error);
                        });
                }
            })
            .catch((error) => {
                console.log('there is something wrong', error);
            });
    });
  }

  uploadImageCallBack(file) {
    console.log('upload image', file);
    return uploadFile(file);
    // Sign s3 will request a signed request and upload directly from client to S3
    //return this.signS3(file);
  }

	sendMessage() {
    // Get the content state
    const contentState = this.state.editorState.getCurrentContent();

    // Only send if there are text
    if (contentState.hasText()) {
      const rawContent = convertToRaw(contentState);
      const content = JSON.stringify(rawContent);

      console.log('about to send message', rawContent, content);
      this.props.publishPost({
        timestamp: + new Date(),
        data: {
          publishDate: new Date(),
          distance: parseFloat(this.state.distance),
          content,
        },
      });

      // Empty the content
      this.setState({
        editorState: EditorState.createWithContent(ContentState.createFromText('')),
      })
    }
	}


  render() {
    const hasText = this.state.editorState.getCurrentContent().hasText();
		// Buttons
		let publishButton = <Button variant="contained" label="Publish" onClick={this.sendMessage.bind(this)} disabled={true} style={styles.publishButton}/>;
		if (hasText) {
			publishButton = <Button variant="contained" label="Publish" onClick={this.sendMessage.bind(this)} secondary={true} style={styles.publishButton}/>;
		}

    const { mentionSuggestions } = this.props;

    console.log("metnon suggestions", mentionSuggestions);
    return (

      <div className="post-editor">
        <TextField
          label="Distance"
          defaultValue={this.state.distance}
          value={this.state.distance}
          onChange={(e, val) => this.setState({distance: val})}
        />
        <br/>
        <Editor
          editorState={this.state.editorState}
          toolbarClassName="home-toolbar"
          wrapperClassName="home-wrapper"
          editorClassName="home-editor"
					toolbar={{
						inline: { inDropdown: true },
						list: { inDropdown: true },
						textAlign: { inDropdown: true },
						link: { inDropdown: true },
						history: { inDropdown: true },
						image: { uploadCallback: this.uploadImageCallBack.bind(this) }
					}}
          onEditorStateChange={this.onEditorStateChange.bind(this)}
          uploadCallback={this.uploadImageCallBack.bind(this)}
            mention={{
              separator: ' ',
              trigger: '@',
              suggestions: mentionSuggestions,
            }}
            hashtag={{}}
        />

        {publishButton}

      </div>
    );
  }
}

export default PostEditor;
