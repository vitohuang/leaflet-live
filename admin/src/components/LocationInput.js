import React, { Component } from 'react';
import Button from'./abstracts/Button';
import TextField from'./abstracts/TextField';

const styles = {
	publishButton: {
		'fontSize': '2em',
		'width': '100%',
		'height': '80%',
		'textAlign': 'center',
	}
}

class LocationInput extends Component {
	constructor(props) {
		super(props);

		this.state = {
			location: '',
		}
	}

	handleChange(event) {
		// location
		this.setState({
			location: event.target.value,
		});
	}

	sendLocation() {
    try {
      const parts = this.state.location.split(',');
      if (parts.length === 2) {

        let lat = parseFloat(parts[0]);
        let lon = parseFloat(parts[1]);
        // Pase the lattitude and longitude
        this.props.publishLocation({
          id: 'MAIN',
          timestamp: + new Date(),
          data: [lat, lon],
        });

        // Clean the text
        this.setState({
          location: '',
        });
      } else {
        console.log('Please enter a vlid location');
      }
    } catch (error) {
      console.log('there is an error, please input a JSON lcoation', error);
      return false;
    }
	}

	render() {

		// Buttons
		let publishButton = <Button variant="contained" label="Update location" onClick={this.sendLocation.bind(this)} disabled={true} style={styles.publishButton}/>;
		if (this.state.location.length > 0) {
			publishButton = <Button variant="contained" label="Update location" onClick={this.sendLocation.bind(this)} secondary={true} style={styles.publishButton}/>;
		}

		return (
			<div className="location-input">
				<TextField
          hintText="Latest location"
          floatingLabelText="Latest location"
					value={this.state.location}
					onChange={this.handleChange.bind(this)}
					onKeyUp={this.handleChange.bind(this)}
				/>

				{publishButton}

			</div>
		)
	}
}
export default LocationInput;
