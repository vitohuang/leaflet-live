import React, { Component } from 'react';

import Button from'./abstracts/Button';
import TextField from'./abstracts/TextField';

import {
  Select,
  MenuItem,
  Card,
  CardActions,
} from '@material-ui/core';
import RemoteSearch from './RemoteSearch.js';
import ResultEntry from './ResultEntry.js';
import ResultTable from './ResultTable.js';

import DND from './dragdrop/index.js';

import {
  API_ENDPOINT,
} from '../share/config.js';

// Config for remote search
const riderSearchEndPoint = API_ENDPOINT + '/riders?q=';

class EditResult extends Component {
  constructor(props) {
    super(props);

    this.state = {
      id: props.id,
      name: props.name,
      type: props.type,
      description: props.description,
      entries: props.entries,
    };
  }

  riderSearchSelected(selection) {
    // Make selection into a object
    if (typeof selection === 'string') {
      selection = {
        name: selection,
        id: + new Date(),
      }
    }

    // Add to the entries
    this.setState({
      entries: this.state.entries.concat(selection),
    });
  }

  save() {
    console.log('going to result')
    let newResult = {
      _id: this.state.id,
      publishDate: new Date(),
      name: this.state.name,
      type: this.state.type,
      description: this.state.description,
      entries: this.state.entries,
    };

    // Save the result
    this.props.onSave(newResult);
    // Going to cacnel it as well
    this.props.onCancel();
  }

  remove(removeItem) {
    console.log('going to remove this item', removeItem);
    // Filter out the remove item
    const newEntries = this.state.entries.filter((item) => {
      return item.id !== removeItem.id;
    });

    // Set the new item states
    this.setState({
      entries: newEntries,
    });
  }

  move(data) {
    console.log('drag and dropped', data);
  }

  changeType(event) {
    // Change the type
    this.setState({
      type: event.target.value,
    })
  }

  entriesChanged(entries) {
    console.log('entries changed', entries);
    this.setState({
      entries,
    })
  }

  render() {

    const dndData = this.state.entries.map((entry) => {
      return {
        id: entry.id,
        content: <ResultEntry
          key={entry.id}
          item={entry}
          onRemove={this.remove.bind(this)}
        />
      }
    })
    return (
      <Card>
        <CardActions>
          <Button
            variant="contained"
            label="Save Result"
            secondary={true}
            onClick={this.save.bind(this)}
          />
          <Button
            variant="contained"
            label="Cancel"
            onClick={this.props.onCancel}
          />
        </CardActions>
        <TextField
          hintText="Name"
          floatingLabelText="Name"
          value={this.state.name}
          onChange={(e, newValue) => this.setState({name: newValue})}
        />
        <br />

        <Select
          inputProps={{
            name: "Type",
          }}
          value={this.state.type}
          onChange={this.changeType.bind(this)}
        >
          <MenuItem value="time">
            Time
          </MenuItem>

          <MenuItem value="point">
            Point
          </MenuItem>

          <MenuItem value="teamTime">
            Team Time
          </MenuItem>

          <MenuItem value="teamPoint">
            Team Point
          </MenuItem>

          <MenuItem value="none">
            None
          </MenuItem>
        </Select>
        <br />

        <TextField
          hintText="Description"
          floatingLabelText="Description"
          value={this.state.description}
          onChange={(e, newValue) => this.setState({description: newValue})}
        />
        <br />

        <RemoteSearch
          clearAfterSelection={true}
          endPoint={riderSearchEndPoint}
          floatingLabelText="Add Rider..."
          onSelected={this.riderSearchSelected.bind(this)}
        />
        <DND data={dndData} onMove={this.move.bind(this)} />

        <ResultTable
          data={this.state.entries}
          type={this.state.type}
          editable={true}
          onChange={this.entriesChanged.bind(this)}
        />
      </Card>
    )
  }
}

EditResult.defaultProps = {
  id: null,
  name: '',
  type: "time",
  description: '',
  entries: [],
  onCancel: e => true,
}

export default EditResult;
