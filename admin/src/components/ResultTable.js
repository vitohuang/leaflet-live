import React from 'react';
import ReactTable from 'react-table';
import 'react-table/react-table.css';
import _ from 'lodash';
import {
  ListItem,
  Avatar,
} from '@material-ui/core';

class ResultTable extends React.PureComponent {
	constructor(props) {
    super(props);

    this.state = {
      data: props.data,
      type: props.type,
    }
	}

  // Set the props to state
  componentWillReceiveProps (nextProps) {
    this.setState({
      data: nextProps.data,
      type: nextProps.type,
    })
  }

  renderEditable (cellInfo) {
    return (<div style={{ backgroundColor: '#fafafa' }} contentEditable suppressContentEditableWarning onBlur={(e) => {
      const data = [...this.state.data]
      console.log("going to change", cellInfo.column.id);

      let value = e.target.textContent;
      // Make sure certain column is number only
      const columnId = cellInfo.column.id;
      if (_.includes(['position', 'sortOrder'], columnId)) {
        value = parseInt(value, 10);
        if (isNaN(value)) {
          alert('please enter a valid number');
          return false;
        }
      }

      data[cellInfo.index][columnId] = value;
      this.setState({data: data})

      // Tell the parent about change
      this.props.onChange(this.state.data);
    }}>{this.state.data[cellInfo.index][cellInfo.column.id]}</div>)
  }

  render () {
    console.log('the data stuff', this.props);
      // Configure the columns
    let columns = [
		{
			Header: 'Name',
			accessor: 'name',
			Cell: row => (
        <ListItem>
          {row.original.avatar && <Avatar src={row.original.avatar} />}

          {row.value}
        </ListItem>
      ),
		},
		{
			Header: 'Team',
			id: 'team',
			accessor: 'team',
		},
		{
			Header: 'Age',
			accessor: 'age',
		},
    ]

    let editableColumns = [
      {
        Header: 'Position',
        id: 'position',
        accessor: 'position',
      },
      {
        Header: 'Time',
        id: 'time',
        accessor: 'time',
      },
      {
        Header: 'Time Gap',
        id: 'timeGap',
        accessor: 'timeGap',
      },
      {
        Header: 'Points',
        id: 'points',
        accessor: 'points',
      },
      {
        Header: 'Average Speed',
        id: 'averageSpeed',
        accessor: 'averageSpeed',
      },
      {
        Header: 'Sort Order',
        id: 'sortOrder',
        accessor: 'sortOrder',
      },
      {
        Header: 'Comment',
        id: 'comment',
        accessor: 'comment',
      },
	];

    // Filter out columns based on type
    // Not show points when its time
    if (this.props.type.toLowerCase().indexOf('time') !== -1) {
      editableColumns = editableColumns.filter(i => !_.includes(['points'], i.id));
    } else if (this.props.type.toLowerCase().indexOf('point') !== -1) {
      editableColumns = editableColumns.filter(i => !_.includes(['time', 'timeGap'], i.id));
    }

    // Add editable
    if (this.props.editable) {
      editableColumns = editableColumns.map((column) => {
          column.Cell = this.renderEditable.bind(this);
          return column;
      });
    }

    // Add the editable columns
    columns = columns.concat(editableColumns);

    console.log('render the table again');
    return (
      <div>
        <div className='table-wrap'>
          <ReactTable
            filterable={true}
            className='-striped -highlight'
            data={this.state.data}
            columns={columns}
            defaultPageSize={10}
          />
        </div>
      </div>
    )
  }
}

export default ResultTable;
