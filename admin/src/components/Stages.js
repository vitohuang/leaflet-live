import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import Button from'./abstracts/Button';
import TextField from'./abstracts/TextField';

import {
  List,
  ListItem,
} from '@material-ui/core';

class Stages extends Component {
  state = {
    name: '',
  }

  add() {
    this.props.add({
      raceId: this.props.race.settings._id,
      name: this.state.name,
      status: 'draft',
    });
  }

  render() {
    const {
      stages,
      settings,
      addStageRequest,
    } = this.props.race;

		let publishButton = <Button variant="contained" label="Add Stage" disabled={true}/>;
		if (this.state.name.length > 0) {
			publishButton = <Button variant="contained" label="Add Stage" onClick={this.add.bind(this)} secondary={true}/>;
		}

    let content = <div>
          <TextField
            hintText="Stage Name"
            value={this.state.name}
            onChange={(e, val) => this.setState({name: val})}
            floatingLabelText="Stage Name"
          />
          {publishButton}
      </div>

      let requestResult = <div>
        </div>

      if (addStageRequest.status === 'ADDING') {
        requestResult = <div>
          <p>Adding a new stage</p>
        </div>
      } else if (addStageRequest.status === 'FAILED') {
        requestResult = <div>
          <p>Failed to add a new stage: {addStageRequest.msg}</p>
        </div>
      } else if (addStageRequest.status === 'ADDED') {
        requestResult = <div>
          <p>Stage added</p>
        </div>
      }

    return (
      <div className="stage-content">
        <h1>Add new stage</h1>
        {content}

        {requestResult}

        <List>
        { stages.map((stage) => (
          <ListItem key={stage.slug}>
            <Link to={`/races/${settings.slug}/stages/${stage.slug}`}>
              { stage.name }
            </Link>
          </ListItem>
        )) }
        </List>
      </div>
    )
  }
}

export default Stages;
