import React from 'react';
import ReactTable from 'react-table';
import 'react-table/react-table.css';
import _ from 'lodash';
import {
  Avatar,
  ListItem,
} from '@material-ui/core';

class ListTable extends React.PureComponent {
  render () {
    console.log('the data stuff', this.props);
      // Configure the columns
    const columns = [
		{
			Header: 'Name',
			accessor: 'name',
			Cell: row => (
        <ListItem>
          {row.original.avatar && <Avatar src={row.original.avatar} />}

          {row.value}
        </ListItem>
      ),
		},
		{
			Header: 'Team',
			id: 'team',
			accessor: 'team',
		},
		{
			Header: 'Age',
			accessor: 'age',
		},
		{
			Header: 'Bib',
			accessor: 'bib',
		},
	];

    return (
      <div>
        <div className='table-wrap'>
          <ReactTable
            filterable={true}
            className='-striped -highlight'
            data={this.props.data}
            columns={columns}
            defaultPageSize={10}
          />
        </div>
        <div style={{textAlign: 'center'}}>
          <br />
          <em>Tip: Hold shift when sorting to multi-sort!</em>
        </div>
      </div>
    )
  }
}

export default ListTable;
