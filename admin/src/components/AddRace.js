import React, { Component } from 'react';
import { Link } from 'react-router-dom';

import Button from'./abstracts/Button';
import TextField from'./abstracts/TextField';

class AddRace extends Component {
  state = {
    name: '',
  }

  add() {
    this.props.add({
      name: this.state.name,
    });
  }

  render() {

    const {
      race,
    } = this.props;

		let publishButton = <Button variant="contained" label="Add Race" disabled={true}/>;
		if (this.state.name.length > 0) {
			publishButton = <Button variant="contained" label="Add Race" onClick={this.add.bind(this)} secondary={true}/>;
		}

    let content = <div>
      </div>

    if (race.addRequest.status === 'ADDING') {
      content = <div>
        <p>Adding a new race</p>
      </div>
    } else if (race.addRequest.status === 'FAILED') {
      content = <div>
        <p>Failed to add: {race.addRequest.msg}</p>
      </div>
    } else if (race.addRequest.status === 'ADDED') {
      content = <div>
        <p>Race added</p>
        <Link to={`/races/${race.settings.slug}/`} >{race.settings.name}</Link>
      </div>
    }

      // Todo need to re render and make the race added disppear

    return (
      <div>
        <h1>Add new race</h1>

        <TextField
          hintText="Race Name"
          value={this.state.name}
          onChange={(e, val) => this.setState({name: val})}
          floatingLabelText="Race Name"
        />
        {publishButton}

        {content}
      </div>
    )
  }
}

export default AddRace;
