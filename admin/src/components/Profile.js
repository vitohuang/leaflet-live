import React, { Component } from 'react';
import TextField from './abstracts/TextField.jsx';

class Profile extends Component {
  constructor(props) {
    super(props);

    this.state = {
      userName: '',
      password: '',
      inputsTouched: false,
    }
  }

  componentWillReceiveProps() {
    console.log('component will receive props');
    this.setState({
      inputsTouched: false,
    })
  }

  onKeyPress(event) {
    if (event.charCode === 13) {
      event.preventDefault();
      this.login();
    }
  }

	// Render the component
  render() {
    const {
      user,
    } = this.props;

    console.log('inside profiile', user);

		// Render the tags
    return (
      <div id="profile-container">
          <TextField
            hintText="Username"
            value={user.userName}
            onKeyPress={this.onKeyPress.bind(this)}
            floatingLabelText="Username"
          />
          <br/>
          <TextField
            hintText="Email"
            value={user.email}
            onKeyPress={this.onKeyPress.bind(this)}
            floatingLabelText="Email"
          />
          <br/>
          <TextField
            hintText="Display Name"
            value={user.displayName}
            onKeyPress={this.onKeyPress.bind(this)}
            floatingLabelText="Display Name"
          />
          <br/>
          <TextField
            hintText="First Name"
            value={user.firstName}
            onKeyPress={this.onKeyPress.bind(this)}
            floatingLabelText="First Name"
          />
          <br/>
          <TextField
            hintText="Last Name"
            value={user.lastName}
            onKeyPress={this.onKeyPress.bind(this)}
            floatingLabelText="Last Name"
          />
          <br/>
          { user.facebookId &&
            <TextField
              hintText="Facebook Id"
              value={user.facebookId}
              disabled={true}
              onKeyPress={this.onKeyPress.bind(this)}
              floatingLabelText="Facebook Id"
            />
          }
          { user.twitterId &&
            <TextField
              hintText="Twitter Id"
              value={user.twitterId}
              disabled={true}
              onKeyPress={this.onKeyPress.bind(this)}
              floatingLabelText="Twitter Id"
            />
          }
          { user.googleId &&
            <TextField
              hintText="Google Id"
              value={user.googleId}
              disabled={true}
              onKeyPress={this.onKeyPress.bind(this)}
              floatingLabelText="Google Id"
            />
          }
      </div>
    );
  }
}

export default Profile;

