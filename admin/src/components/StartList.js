import React, { Component } from 'react';
import Dropzone from 'react-dropzone';
import Papaparse from 'papaparse';

import ListTable from './ListTable.js';
import Button from'./abstracts/Button';
import TextField from'./abstracts/TextField';

import RemoteSearch from './RemoteSearch.js';

import {
  API_ENDPOINT,
} from '../share/config.js';

const riderSearchEndPoint = API_ENDPOINT + '/riders?q=';

// Sart list component
class StartList extends Component {
	constructor(props) {
		super(props)

		this.state = {
			files: [],
      data: this.props.data,
      newRider: false,
      newRiderBib: '',
		}
	}

	onDrop(acceptedFiles, rejectedFiles) {
        const parseConfig = {
            header: true,
        };

		console.log("you dropped something", acceptedFiles, rejectedFiles);
		this.setState({
			files:acceptedFiles,
		})

		if (acceptedFiles.length > 0) {
			// Start a file reader
			var reader = new FileReader();
			reader.onload = () => {
				var text = reader.result;
				console.log('text', text);
                const result = Papaparse.parse(text, parseConfig);
                console.log('Parse result', result);

                // Chech for errors
                if (result.errors.length > 0) {
                    console.log("There is something wrong with the", result.errors);
                } else {
                    this.setState({
                        data: result.data.concat(this.state.data),
                    })
                }
			}

			acceptedFiles.forEach(file => {
				// Read the content
				reader.readAsText(file);

				// Delete the preview to avoid memory leaks
				window.URL.revokeObjectURL(file.preview)
			});
		}
	}

  save() {
    this.props.save(this.state.data);
  }

  add() {
    if (typeof this.state.newRider !== 'object') {
      // Todo flash error
      alert('enter a valid rider name');
      return false;
    }

    // In case the rider is not an object, just text input
    let newRider = this.state.newRider;
    newRider.bib = this.state.newRiderBib;

    this.setState({
      data: [newRider].concat(this.state.data),
      newRiderBib: '',
    });
  }

  componentWillReceiveProps(nextProps) {
    this.setState({
      data: nextProps.data,
    });
  }

  riderSearchSelected(selection) {
    this.setState({
      newRider: selection,
    })
  }

  onNewRiderBibChange(e, newValue) {
    this.setState({
      newRiderBib: newValue
    });
  }

  render() {
    return (
      <Dropzone
        disableClick
        style={{}}
        onDrop={this.onDrop.bind(this)}
        accept="text/plain, text/csv"
      >

      <div className="add-row">
        <RemoteSearch
          endPoint={riderSearchEndPoint}
          floatingLabelText="Search Riders..."
          onSelected={this.riderSearchSelected.bind(this)}
        />

        <TextField
          hintText="Bib"
          floatingLabelText="Bib"
          value={this.state.newRiderBib}
          onChange={this.onNewRiderBibChange.bind(this)}
        />

        <Button
          variant="contained"
          label="Add"
          secondary={true}
          onClick={this.add.bind(this)}
        />
      </div>

        <ListTable data={this.state.data} />

        <Button
          variant="contained"
          label="Save"
          secondary={true}
          onClick={this.save.bind(this)}
        />
      </Dropzone>
    );
  }
}

export default StartList;
