// @flow

import {
  //PropTypes,
  Path
} from 'react-leaflet'
import { polyline } from 'leaflet'

export default class LiveLine extends Path {
/*
  static propTypes = {
    children: PropTypes.children,
    positions: PropTypes.oneOfType([
      latlngListType,
      PropTypes.arrayOf(latlngListType),
    ]).isRequired,
  }
*/

  createLeafletElement(props: Object): Object {
console.log('live line', props);
    const { positions, ...options } = props
    return polyline(positions, this.getOptions(options))
  }

  updateLeafletElement(fromProps: Object, toProps: Object) {
console.log('live line going to update', fromProps, toProps);
    if (toProps.positions.length !== fromProps.positions.length) {
      console.log('going to set the lat lngs', toProps.positions);
      this.leafletElement.setLatLngs(toProps.positions)
      console.log('after the set lat lngs in live layer');
    }
    this.setStyleIfChanged(fromProps, toProps)
  }
}
