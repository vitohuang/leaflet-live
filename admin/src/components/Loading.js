import React from 'react';

import {
  Snackbar,
  LinearProgress,
} from '@material-ui/core';
const Loading = ({ registered }) => {
		let loading = <LinearProgress mode="indeterminate" />
    /*
		if (registered) {
			loading = <Snackbar open={true} message="Connected!" autoHideDuration={2000} />
		}
		console.log("loading", loading, registered);
    */

    return loading;
}

export default Loading;
