import React, { Component } from 'react';
import CardItem from './SituationCardItem.js';
import EditCard from  '../containers/SituationEditCardItem.js';
import {
  Card,
  CardActions,
  CardHeader,
  CardContent,
  List,
} from '@material-ui/core';

import Button from'./abstracts/Button';

export default class SituationCard extends Component {
  constructor(props) {
    super(props);

    this.state = {
      edit: false,
    }
  }

  remove() {
    console.log('going to remove this situation', this.props.id);
    if (this.props.id) {
      this.props.onRemove(this.props.id);
    }
  }

  render() {
    const { id, title, subTitle, desc, items } = this.props;

    let itemContent = null;

    console.log('inside the situation card render', this.props);
		if (items.length > 0) {
      itemContent = <List>
          {items.map((item) =>
            <CardItem
              key={item.id}
              item={item}
              displayRemoveButton={false}
            />
          )}
        </List>
		}

    let cardContent = <Card>
          <CardHeader
            title={title}
            subtitle={subTitle}
          />

          {desc &&
            <CardContent>
                {desc}
              </CardContent>
          }

          <CardContent>
            {itemContent}
          </CardContent>
          <CardActions>
            <Button
              label="Edit"
              onClick={e => this.setState({edit: true})}
            />

            <Button
              variant="contained"
              label="Delete"
              secondary={true}
              onClick={this.remove.bind(this)}
            />
          </CardActions>
        </Card>

    if (this.state.edit) {
      cardContent = <EditCard
        id={id}
        title={title}
        subTitle={subTitle}
        desc={desc}
        items={items}
        onCancel={e => this.setState({edit: false})}
      />
    }

    console.log('card content', cardContent);
    return cardContent
  }
}
