// @flow

import PropTypes from 'prop-types';
import { Path } from 'react-leaflet'
import { circleMarker } from 'leaflet'

export default class LiveMarker extends Path {
  static propTypes = {
    center: PropTypes.isRequired,
    children: PropTypes.children,
  }

  createLeafletElement(props: Object): Object {
    console.log('live marker create element', props);
    const { center, ...options } = props
    const liveMarker = circleMarker(center, this.getOptions(Object.assign({
      radius: 5,
      color: 'orange',
      fillColor: '#333',
      fillOpacity: 1,
      className: 'leaflet-marker-live',
    }, options)))

    // Add click event
    liveMarker.on('click', function(event) {
      console.log('live marker clicked', event);
    });

    // Return the live marker
    return liveMarker;
  }

  updateLeafletElement(fromProps: Object, toProps: Object) {
    console.log('live marker update element', fromProps, toProps);
    if (toProps.center !== fromProps.center) {
      this.leafletElement.setLatLng(toProps.center)
    }
    if (toProps.radius !== fromProps.radius) {
      this.leafletElement.setRadius(toProps.radius)
    }
  }
}
