import { connect } from 'react-redux';
import LoadingCom from '../components/Loading.js';

const mapStateToProps = (state) => {
	return {
		registered: state.overall.registered,
	}
}

// Connect the props to components
const Loading = connect(
	mapStateToProps,
)(LoadingCom);

// Export the module
export default Loading;
