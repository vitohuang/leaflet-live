import { connect } from 'react-redux';
import LoginCom from '../components/Login.js';

// Actions
import {
  login,
} from '../store/actions.js';

const mapStateToProps = (state) => {
	return {
    loggedIn: state.overall.loggedIn,
	}
}

const mapDispatchToProps = (dispatch) => {
	return {
		login: (userName, password) => {
			dispatch(login(userName, password));
		},
	}
}

// Connect the props to components
const Login = connect(
	mapStateToProps,
	mapDispatchToProps,
)(LoginCom);

// Export the module
export default Login;
