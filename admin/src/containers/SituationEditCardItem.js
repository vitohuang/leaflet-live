import { connect } from 'react-redux';
import SituationEditCardItemCom from '../components/SituationEditCardItem.js';

// Actions
import {
  addSituation,
  editSituation,
} from '../store/actions.js';

const mapStateToProps = (state) => {
	return {
	}
}

const mapDispatchToProps = (dispatch) => {
	return {
		add: (situation) => {
			console.log('going to add situation');
			dispatch(addSituation(situation));
		},
		edit: (id, situation) => {
			console.log('going to edit situation');
			dispatch(editSituation(id, situation));
		},
	}
}

// Connect the props to components
const SituationEditCardItem = connect(
	mapStateToProps,
	mapDispatchToProps,
)(SituationEditCardItemCom);

// Export the module
export default SituationEditCardItem;
