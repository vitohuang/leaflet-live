import { connect } from 'react-redux';
import ResultsCom from '../../components/Results.js';

// Actions
import {
  deleteResult,
  saveResult,
} from '../../store/actions.js';

const mapStateToProps = (state) => {
	return {
    results: state.race.results,
	}
}

const mapDispatchToProps = (dispatch) => {
	return {
    remove: (id) => {
      dispatch(deleteResult('race', id));
    },
    save: (result) => {
      console.log('going to save race result');
      dispatch(saveResult('race', result));
    },
	}
}

// Connect the props to components
const Results = connect(
	mapStateToProps,
	mapDispatchToProps,
)(ResultsCom);

// Export the module
export default Results;
