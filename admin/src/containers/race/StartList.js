import { connect } from 'react-redux';
import StartListCom from '../../components/StartList.js';

import {
  saveStartList,
} from '../../store/actions.js';

const mapStateToProps = (state) => {
	return {
    data: state.race.startList,
	}
}

const mapDispatchToProps = (dispatch) => {
	return {
    save(startList) {
      console.log('goign to dispatch race startlist save', startList);
      dispatch(saveStartList('race', startList));
    }
	}
}

// Connect the props to components
const StartList = connect(
	mapStateToProps,
	mapDispatchToProps,
)(StartListCom);

// Export the module
export default StartList;
