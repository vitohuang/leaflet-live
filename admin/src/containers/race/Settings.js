import { connect } from 'react-redux';
import RaceSettingsCom from '../../components/RaceSettings.js';

// Actions
import {
  saveSettings,
  deleteColl,
} from '../../store/actions.js';

const mapStateToProps = (state) => {
	return {
    settings: state.race.settings,
	}
}

const mapDispatchToProps = (dispatch) => {
	return {
		save: (settings) => {
			console.log('going to save race settings');
			dispatch(saveSettings('race', settings));
		},
    delete: (raceId) => {
      console.log('going to delete a race', raceId);
      dispatch(deleteColl('race', raceId));
    }
	}
}

// Connect the props to components
const RaceSettings = connect(
	mapStateToProps,
	mapDispatchToProps,
)(RaceSettingsCom);

// Export the module
export default RaceSettings;
