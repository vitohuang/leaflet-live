import { connect } from 'react-redux';
import CourseMapCom from '../../components/CourseMap.js';

// Actions
import {
  saveCourseGeo,
} from '../../store/actions.js';

const mapStateToProps = (state) => {
  let course = null;
  let drawData = null;

  if (state.race.settings.courseGeo) {
    if (state.race.settings.courseGeo.course) {
      course = state.race.settings.courseGeo.course;
    }

    if (state.race.settings.courseGeo.drawData) {
      drawData = state.race.settings.courseGeo.drawData;
    }
  }

  console.log('inside the course map container', course, drawData);
	return {
    course,
    drawData,
	}
}

const mapDispatchToProps = (dispatch) => {
	return {
    save: (course, drawData) => {
      dispatch(saveCourseGeo('race', course, drawData));
    },
	}
}

// Connect the props to components
const CourseMap = connect(
	mapStateToProps,
  mapDispatchToProps
)(CourseMapCom);

// Export the module
export default CourseMap;
