import { connect } from 'react-redux';
import ProfileCom from '../components/Profile.js';

// Actions
import {
  login,
} from '../store/actions.js';

const mapStateToProps = (state) => {
	return {
    user: state.overall.user,
	}
}

const mapDispatchToProps = (dispatch) => {
	return {
		login: (userName, password) => {
			dispatch(login(userName, password));
		},
	}
}

// Connect the props to components
const Profile = connect(
	mapStateToProps,
	mapDispatchToProps,
)(ProfileCom);

// Export the module
export default Profile;
