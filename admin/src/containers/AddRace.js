import { connect } from 'react-redux';
import AddRaceCom from '../components/AddRace.js';

// Actions
import {
  addRace,
  clearAdd,
} from '../store/actions.js';

const mapStateToProps = (state) => {
	return {
    race: state.race,
	}
}

const mapDispatchToProps = (dispatch) => {
	return {
		add: (race) => {
			dispatch(addRace(race));
		},
    clearAddRace: () => {
      dispatch(clearAdd('race'));
    }
	}
}

// Connect the props to components
const AddRace = connect(
	mapStateToProps,
	mapDispatchToProps,
)(AddRaceCom);

// Export the module
export default AddRace;
