import { connect } from 'react-redux';
import LocationInputCom from '../components/LocationInput.js';
import { sendNewObject } from '../store/actions.js';

const mapStateToProps = (state) => {
	return {
	}
}

const mapDispatchToProps = (dispatch) => {
	return {
		publishLocation: (location) => {
			console.log('going to publish new location', location);
			dispatch(sendNewObject('location', location));
		}
	}
}

// Connect the props to components
const LocationInput = connect(
	mapStateToProps,
	mapDispatchToProps,
)(LocationInputCom);

// Export the module
export default LocationInput;
