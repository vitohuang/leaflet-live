import { connect } from 'react-redux';
import PostEditorCom from '../components/PostEditor.js';

import { sendNewObject } from '../store/actions.js';

const mapStateToProps = (state) => {

  // Get the mention suggestions from startlist
  const mentionSuggestions = state.stage.startList.map((rider) => {
    let displayName = rider.name.toUpperCase();
    return {
      text: displayName,
      value: displayName,
      url: rider.handle,
    }
  });
	return {
    mentionSuggestions,
	}
}

const mapDispatchToProps = (dispatch) => {
	return {
		publishPost: (post) => {
			console.log('going to publish new message', post);
			dispatch(sendNewObject('post', post));
		}
	}
}

// Connect the props to components
const PostEditor = connect(
	mapStateToProps,
	mapDispatchToProps,
)(PostEditorCom);

// Export the module
export default PostEditor;
