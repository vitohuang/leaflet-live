import { connect } from 'react-redux';
import StageSettingsCom from '../components/StageSettings.js';

// Actions
import {
  saveSettings,
  deleteColl,
} from '../store/actions.js';

const mapStateToProps = (state) => {
	return {
    settings: state.stage.settings,
	}
}

const mapDispatchToProps = (dispatch) => {
	return {
		save: (settings) => {
			console.log('going to save settings');
			dispatch(saveSettings('stage', settings));
		},
    delete: (stageId) => {
      console.log('going to delete a stage', stageId);
      dispatch(deleteColl('stage', stageId));
    }
	}
}

// Connect the props to components
const StageSettings = connect(
	mapStateToProps,
	mapDispatchToProps,
)(StageSettingsCom);

// Export the module
export default StageSettings;
