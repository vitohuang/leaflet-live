import { connect } from 'react-redux';
import StagesCom from '../components/Stages.js';

// Actions
import {
  addStage,
  clearAdd,
} from '../store/actions.js';

const mapStateToProps = (state) => {
	return {
    race: state.race,
	}
}

const mapDispatchToProps = (dispatch) => {
	return {
		add: (stage) => {
			dispatch(addStage(stage));
		},
    clearAddStage: () => {
      dispatch(clearAdd('stage'));
    }
	}
}

// Connect the props to components
const Stages = connect(
	mapStateToProps,
	mapDispatchToProps,
)(StagesCom);

// Export the module
export default Stages;
