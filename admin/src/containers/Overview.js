import { connect } from 'react-redux';
import OverviewCom from '../components/Overview.js';

// Actions
import {
  login,
} from '../store/actions.js';

const mapStateToProps = (state) => {
	return {
    loggedIn: state.overall.loggedIn,
	}
}

const mapDispatchToProps = (dispatch) => {
	return {
		login: (userName, password) => {
			dispatch(login(userName, password));
		},
	}
}

// Connect the props to components
const Overview = connect(
	mapStateToProps,
	mapDispatchToProps,
)(OverviewCom);

// Export the module
export default Overview;
