import { connect } from 'react-redux';
import SituationsCom from '../components/Situations.js';

// Actions
import {
  rearrangeSituations,
  removeSituation,
  saveSituations,
} from '../store/actions.js';

const mapStateToProps = (state) => {
	return {
    situations: state.stage.situations,
	}
}

const mapDispatchToProps = (dispatch) => {
	return {
    rearrange: (situations) => {
      dispatch(rearrangeSituations(situations));
    },
    remove: (id) => {
      dispatch(removeSituation(id));
    },
    save: (situations) => {
      // Save the situations to the setting as well
      dispatch(saveSituations(situations));
    },
	}
}

// Connect the props to components
const Situations = connect(
	mapStateToProps,
	mapDispatchToProps,
)(SituationsCom);

// Export the module
export default Situations;
