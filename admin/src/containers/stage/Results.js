import { connect } from 'react-redux';
import ResultsCom from '../../components/Results.js';

// Actions
import {
  deleteResult,
  saveResult,
} from '../../store/actions.js';

const mapStateToProps = (state) => {
	return {
    results: state.stage.results,
	}
}

const mapDispatchToProps = (dispatch) => {
	return {
    remove: (id) => {
      dispatch(deleteResult('stage', id));
    },
    save: (result) => {
      dispatch(saveResult('stage', result));
    },
	}
}

// Connect the props to components
const Results = connect(
	mapStateToProps,
	mapDispatchToProps,
)(ResultsCom);

// Export the module
export default Results;
