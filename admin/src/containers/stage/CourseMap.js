import { connect } from 'react-redux';
import CourseMapCom from '../../components/CourseMap.js';

// Actions
import {
  saveCourseGeo,
} from '../../store/actions.js';

const mapStateToProps = (state) => {
  let course = null;
  let drawData = null;

  if (state.stage.settings.courseGeo) {
    if (state.stage.settings.courseGeo.course) {
      course = state.stage.settings.courseGeo.course;
    }

    if (state.stage.settings.courseGeo.drawData) {
      drawData = state.stage.settings.courseGeo.drawData.data;
    }
  }

  console.log('inside the course map container', course, drawData);
	return {
    course,
    drawData,
	}
}

const mapDispatchToProps = (dispatch) => {
	return {
    save: (course, drawData) => {
      dispatch(saveCourseGeo('stage', course, drawData));
    },
	}
}

// Connect the props to components
const CourseMap = connect(
	mapStateToProps,
  mapDispatchToProps
)(CourseMapCom);

// Export the module
export default CourseMap;
