import { connect } from 'react-redux';
import StartListCom from '../../components/StartList.js';

import {
  saveStartList,
} from '../../store/actions.js';

const mapStateToProps = (state) => {
	return {
    data: state.stage.startList,
	}
}

const mapDispatchToProps = (dispatch) => {
	return {
    save(startList) {
      console.log('goign to dispatch save', startList);
      dispatch(saveStartList('stage', startList));
    }
	}
}

// Connect the props to components
const StartList = connect(
	mapStateToProps,
	mapDispatchToProps,
)(StartListCom);

// Export the module
export default StartList;
