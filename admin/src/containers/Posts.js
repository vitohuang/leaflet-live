import { connect } from 'react-redux';
import PostsCom from '../components/Posts.js';
import {
  topicNameFromStage,
} from '../share/utils.js';

const mapStateToProps = (state) => {
  // Figure out the posts
  let posts = [];
  if (state.stage.posts[topicNameFromStage(state.stage.settings)]) {
		posts = state.stage.posts[topicNameFromStage(state.stage.settings)];
  }

	return {
    posts,
	}
}

// Connect the props to components
const Posts = connect(
	mapStateToProps
)(PostsCom);

// Export the module
export default Posts;
