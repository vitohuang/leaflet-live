import { connect } from 'react-redux';
import LiveLayerCom from '../components/LiveLayer.js';
import {
  topicNameFromStage,
} from '../share/utils.js';

const mapStateToProps = (state) => {
  // Figure out the locations
  let locations = [];
  if (state.stage.locations[topicNameFromStage(state.stage.settings)]) {
		locations = state.stage.locations[topicNameFromStage(state.stage.settings)];
  }

  return {
    locations,
  };
}

// Connect the props to components
const LiveLayer = connect(
	mapStateToProps
)(LiveLayerCom);

// Export the module
export default LiveLayer;
