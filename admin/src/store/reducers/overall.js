import {
  REGISTERED,
  WS_STATUS,
  TOGGLE_MENU,
  TOGGLE_SECOND_MENU,
  LOGIN_SUCCESS,
  LOGIN_FAILED,
  LOGOUT,
  CHANGE_BAR_TITLE,
  RECEIVED_RACES,
} from '../actions.js';

// The initial state
let initialState = {
  user: null,
  barTitle: '',
	registered: false,
	roomId: null,
	wsStatus: null,
	menuOpen: false,
	secondMenuOpen: false,
  // Use null to indicate it never touched, false means already touch etc
  loggedIn: null,
  races: [],
};

export default (state = initialState, action) => {
  switch (action.type) {
		case CHANGE_BAR_TITLE:
			return Object.assign({}, state, {
        barTitle: action.title,
			});
		case LOGIN_SUCCESS:
			return Object.assign({}, state, {
        loggedIn: true,
        user: action.data
			});
    case LOGIN_FAILED:
			return Object.assign({}, state, {
        loggedIn: false,
			});
    case LOGOUT:
			return Object.assign({}, state, {
        loggedIn: null, // Its null in here to indicate nothing touched it before
			});
		case REGISTERED:
			return Object.assign({}, state, {
				registered: true,
        roomId: action.roomId,
			});
		case WS_STATUS:
			return Object.assign({}, state, {
				wsStatus: action.status,
			});
		case TOGGLE_MENU:
			return Object.assign({}, state, {
				menuOpen: !state.menuOpen,
			});
		case TOGGLE_SECOND_MENU:
			return Object.assign({}, state, {
				secondMenuOpen: !state.secondMenuOpen,
			});
		case RECEIVED_RACES:
			return Object.assign({}, state, {
        races: action.data,
			});
    default:
      return state
  }
}
