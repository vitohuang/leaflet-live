import _ from 'lodash';

// Import stuff from action
import {
  MESSAGE,
  MESSAGE_CHUNK,
  ADD_SITUATION,
  EDIT_SITUATION,
  REMOVE_SITUATION,
  REARRANGE_SITUATION,
  SAVING_STAGE_RESULT,
  SAVING_STAGE_RESULT_ERROR,
  SAVED_STAGE_RESULT,
  DELETING_STAGE_RESULT,
  DELETING_STAGE_RESULT_ERROR,
  DELETED_STAGE_RESULT,
  SAVED_STAGE_SETTINGS,
  CHANGING_STAGE_STATUS,
  CHANGED_STAGE_STATUS,
  FETCHING_STAGE_SETTINGS,
  FETCHING_STAGE_SETTINGS_ERROR,
  RECEIVED_STAGE_SETTINGS,
} from '../actions.js';
import {
  handleMessages,
} from '../../share/utils.js';

const initRequestObj = {
  status: '',
  msg: '',
}

// The initial state
let initialState = {
	locations: {},
	posts: {},
  situationHistory: {},
  resultsHistory: {},
  startList: [],
  situations: [],
  results: [],
  settings: {},
  request: initRequestObj,
  saveResultRequest: initRequestObj,
  deleteResultRequest: initRequestObj,
};

export default (state = initialState, action) => {
  switch (action.type) {
    case MESSAGE:
      let topicName = action.message.topic;
			// Check what type of message is it
			switch (action.message.key) {
				case 'location':
          // Check see if its really a location
          // Only checking the length, really need to check for the value
          //let resultState = handleMessages(_.pick(state, ['locations']), [action.message]);
          return Object.assign({}, state, handleMessages(_.pick(state, ['locations']), [action.message]));
				case 'post':
          return Object.assign({}, state, handleMessages(_.pick(state, ['posts']), [action.message]));
        default:
          return state;
			}
    case MESSAGE_CHUNK:
      const oldState = _.pick(state, ['maps', 'posts', 'situationHistory', 'resultsHistory', 'locations']);
      let resultState = handleMessages(oldState, action.data);
      console.log('MESSAGE_CHUNK, going to set the result state', resultState);
      let aa  = Object.assign({}, state, resultState);
      console.log('MESSAGE_CHUNK, the final state going to return', aa);
      return aa;
		case ADD_SITUATION:
      // Add it to the first
			return Object.assign({}, state, {
				situations: [action.data].concat(state.situations),
			});
		case EDIT_SITUATION:
      const newSituations = state.situations.map((situation) => {
        if (situation.id === action.data.id) {
          return action.data.situation;
        } else {
          return situation;
        }
      });

			return Object.assign({}, state, {
				situations: newSituations,
			});
		case REMOVE_SITUATION:
			return Object.assign({}, state, {
				situations: state.situations.filter(s => s.id !== action.id),
			});
		case REARRANGE_SITUATION:
      // Add it to the first
			return Object.assign({}, state, {
				situations: action.data,
			});
		case SAVED_STAGE_SETTINGS:
      // Add it to the first
			return Object.assign({}, state, {
				settings: action.data,
        startList: action.data.startList || [],
			});
    case FETCHING_STAGE_SETTINGS:
			return Object.assign({}, state, {
        request: {
          status: 'FETCHING',
          msg: '',
        },
			});
    case FETCHING_STAGE_SETTINGS_ERROR:
			return Object.assign({}, state, {
        request: {
          status: 'FAILED',
          msg: action.data,
        }
			});
		case RECEIVED_STAGE_SETTINGS:
      // Get the situations
      let situations = [];
      if (action.data.situations) {
        if (action.data.situations.timestamp) {
          situations = action.data.situations.data;
        } else {
          situations = action.data.situations;
        }
      }

      let results = [];
      if (action.data.results) {
        if (action.data.results.timestamp) {
          results = action.data.results.data;
        } else {
          results = action.data.results;
        }
      }

      // Add it to the first
			return Object.assign({}, state, {
        request: initialState.request,
        results,
        situations,
				settings: action.data,
        startList: action.data.startList || [],
			});
		case CHANGING_STAGE_STATUS:
      let prevSettings = state.settings;
      prevSettings.status = action.data;

			return Object.assign({}, state, {
        settings: prevSettings,
			});
		case CHANGED_STAGE_STATUS:
      let oldSettings = state.settings;
      oldSettings.status = action.data;

			return Object.assign({}, state, {
        settings: oldSettings,
			});
    case SAVING_STAGE_RESULT:
			return Object.assign({}, state, {
        saveResultRequest: {
          status: 'SAVING',
          msg: '',
        },
			});
    case SAVING_STAGE_RESULT_ERROR:
			return Object.assign({}, state, {
        saveResultRequest: {
          status: 'FAILED',
          msg: action.data,
        }
			});
    case SAVED_STAGE_RESULT:
      let newResults = state.results;

      // Change the existing one if needed
      let changed = false;
      newResults = state.results.map((result) => {
        if (result._id=== action.data._id) {
          changed = true;
          return action.data;
        } else {
          return result;
        }
      });

      // Add to the end if there is no result in the old results
      if (!changed) {
        newResults.push(action.data);
      }
			return Object.assign({}, state, {
        saveResultRequest: initRequestObj,
        results: newResults,
			});
    case DELETING_STAGE_RESULT:
			return Object.assign({}, state, {
        deleteResultRequest: {
          status: 'DELETING',
          msg: '',
        },
			});
    case DELETING_STAGE_RESULT_ERROR:
			return Object.assign({}, state, {
        deleteResultRequest: {
          status: 'FAILED',
          msg: action.data,
        }
			});
    case DELETED_STAGE_RESULT:
      // Filter out the delete one
      let remainResults = state.results.filter((result) => {
        if (result._id === action.data._id) {
          return false;
        } else {
          return true;
        }
      });

			return Object.assign({}, state, {
        deleteResultRequest: initRequestObj,
        results: remainResults,
			});
    default:
      return state
  }
}
