import {
  SAVING_RACE_RESULT,
  SAVING_RACE_RESULT_ERROR,
  SAVED_RACE_RESULT,
  DELETING_RACE_RESULT,
  DELETING_RACE_RESULT_ERROR,
  DELETED_RACE_RESULT,
  FETCHING_RACE_SETTINGS,
  FETCHING_RACE_SETTINGS_ERROR,
  RECEIVED_RACE_SETTINGS,
  SAVED_RACE_SETTINGS,
  ADDING_RACE,
  ADDING_RACE_ERROR,
  ADDED_RACE,
  ADDING_STAGE,
  ADDING_STAGE_ERROR,
  ADDED_STAGE,
  CLEAR_ADDED_RACE,
} from '../actions.js';

const initRequestObj = {
  status: '',
  msg: '',
}

// The initial state
let initialState = {
  raceId: null,
  stages:[],
  startList: [],
  results: [],
  settings: {},
  saveResultRequest: initRequestObj,
  deleteResultRequest: initRequestObj,
  addStageRequest: initRequestObj,
  addRequest: initRequestObj,
  request: initRequestObj,
};

export default (state = initialState, action) => {
  switch (action.type) {
    case ADDING_RACE:
			return Object.assign({}, state, {
        addRequest: {
          status: 'ADDING',
          msg: '',
        },
			});
    case ADDING_RACE_ERROR:
			return Object.assign({}, state, {
        addRequest: {
          status: 'FAILED',
          msg: action.data,
        }
			});
    case ADDED_RACE:
			return Object.assign({}, state, {
        addRequest: {
          status: 'ADDED',
          msg: '',
        },
				settings: action.data,
        startList: action.data.startList || [],
			});
    case ADDING_STAGE:
			return Object.assign({}, state, {
        addRequest: {
          status: 'ADDING',
          msg: '',
        },
			});
    case ADDING_STAGE_ERROR:
			return Object.assign({}, state, {
        addRequest: {
          status: 'FAILED',
          msg: action.data,
        }
			});
    case ADDED_STAGE:
			return Object.assign({}, state, {
        addStageRequest: {
          status: 'ADDED',
          msg: '',
        },
        stages: state.stages.concat([action.data]),
			});
    case FETCHING_RACE_SETTINGS:
			return Object.assign({}, state, {
        request: {
          status: 'FETCHING',
          msg: '',
        },
			});
    case FETCHING_RACE_SETTINGS_ERROR:
			return Object.assign({}, state, {
        request: {
          status: 'FAILED',
          msg: action.data,
        }
			});
    case RECEIVED_RACE_SETTINGS:
			return Object.assign({}, state, {
        request: initialState.request,
        stages: action.data.stages || [],
        results: action.data.results || [],
				settings: action.data,
        startList: action.data.startList || [],
			});
    case SAVED_RACE_SETTINGS:
			return Object.assign({}, state, {
				settings: action.data,
        startList: action.data.startList || [],
			});
    case CLEAR_ADDED_RACE:
			return Object.assign({}, state, {
        added: false,
			});
    case SAVING_RACE_RESULT:
			return Object.assign({}, state, {
        saveResultRequest: {
          status: 'SAVING',
          msg: '',
        },
			});
    case SAVING_RACE_RESULT_ERROR:
			return Object.assign({}, state, {
        saveResultRequest: {
          status: 'FAILED',
          msg: action.data,
        }
			});
    case SAVED_RACE_RESULT:
      let newResults = state.results;

      // Change the existing one if needed
      let changed = false;
      newResults = state.results.map((result) => {
        if (result._id=== action.data._id) {
          changed = true;
          return action.data;
        } else {
          return result;
        }
      });

      // Add to the end if there is no result in the old results
      if (!changed) {
        newResults.push(action.data);
      }
			return Object.assign({}, state, {
        saveResultRequest: initRequestObj,
        results: newResults,
			});
    case DELETING_RACE_RESULT:
			return Object.assign({}, state, {
        deleteResultRequest: {
          status: 'DELETING',
          msg: '',
        },
			});
    case DELETING_RACE_RESULT_ERROR:
			return Object.assign({}, state, {
        deleteResultRequest: {
          status: 'FAILED',
          msg: action.data,
        }
			});
    case DELETED_RACE_RESULT:
      // Filter out the delete one
      let remainResults = state.results.filter((result) => {
        if (result._id === action.data._id) {
          return false;
        } else {
          return true;
        }
      });

			return Object.assign({}, state, {
        deleteResultRequest: initRequestObj,
        results: remainResults,
			});
    default:
      return state
  }
}
