import _ from 'lodash';

// Import the send message on ws
import { registerRoom } from './ws.js';

import {
  API_ENDPOINT,
} from '../share/config.js';

import {
  topicNameFromStage,
} from '../share/utils.js';

export const CHANGE_BAR_TITLE = 'CHANGE_BAR_TITLE';
export const WS_STATUS = 'WS_STATUS';
export const REGISTERED = 'REGISTERED';

export const NO_OP = 'NO_OP';
export const LOADING = 'LOADING';

export const LOGIN_SUCCESS = 'LOGIN_SUCCESS';
export const LOGIN_FAILED = 'LOGIN_FAILED';
export const LOGOUT = 'LOGOUT';

export const MESSAGE = 'MESSAGE';
export const SENDING_MESSAGE = 'SENDING_MESSAGE';
export const SEND_MESSAGE = 'SEND_MESSAGE';
export const MESSAGE_RECEIVED = 'MESSAGE_RECEIVED';

// Chunk message
export const MESSAGE_CHUNK = 'MESSAGE_CHUNK';

export const TOGGLE_MENU = 'TOGGLE_MENU';
export const TOGGLE_SECOND_MENU = 'TOGGLE_SECOND_MENU';

export const REQUEST_QRCODE = 'REQUEST_QRCODE';
export const RECEIVE_QRCODE = 'RECEIVE_QRCODE';

export const LOCATION_SENT = 'LOCATION_SENT';
export const POST_SENT = 'POST_SENT';
export const RESULT_SENT = 'RESULT_SENT';
export const SITUATIONS_SENT = 'SITUATIONS_SENT';
export const MAP_SENT = 'MAP_SENT';

export const ADD_SITUATION = 'ADD_SITUATION';
export const EDIT_SITUATION = 'EDIT_SITUATION';
export const REARRANGE_SITUATION = 'REARRANGE_SITUATION';
export const REMOVE_SITUATION = 'REMOVE_SITUATION';

export const SAVE_STARTLIST = 'SAVE_STARTLIST';

// Stage
export const SAVING_STAGE_SETTINGS = 'SAVING_STAGE_SETTINGS';
export const SAVING_STAGE_SETTINGS_ERROR = 'SAVING_STAGE_SETTINGS_ERROR';
export const SAVED_STAGE_SETTINGS = 'SAVED_STAGE_SETTINGS';

export const DELETED_STAGE = 'DELETED_STAGE';
export const DELETING_STAGE_ERROR = 'DELETING_STAGE_ERROR';
export const DELETING_STAGE = 'DELETING_STAGE';

export const SAVING_STAGE_RESULT = 'SAVING_STAGE_RESULT';
export const SAVING_STAGE_RESULT_ERROR = 'SAVING_STAGE_RESULT_ERROR';
export const SAVED_STAGE_RESULT = 'SAVED_STAGE_RESULT';

export const FETCHING_STAGE_SETTINGS_ERROR = 'FETCHING_STAGE_SETTINGS_ERROR';
export const FETCHING_STAGE_SETTINGS = 'FETCHING_STAGE_SETTINGS';
export const RECEIVED_STAGE_SETTINGS = 'RECEIVED_STAGE_SETTINGS';

export const ADDING_RACE = 'ADDING_RACE';
export const ADDING_RACE_ERROR = 'ADDING_RACE_ERROR';
export const ADDED_RACE = 'ADDED_RACE';
export const CLEAR_ADDED_RACE = 'CLEAR_ADDED_RACE';

export const ADDING_STAGE = 'ADDING_STAGE';
export const ADDING_STAGE_ERROR = 'ADDING_STAGE_ERROR';
export const ADDED_STAGE = 'ADDED_STAGE';
export const CLEAR_ADDED_STAGE = 'CLEAR_ADDED_STAGE';

export const FETCHING_STAGE_RESULT = 'FETCHING_STAGE_RESULT';
export const FETCHING_STAGE_RESULT_ERROR = 'FETCHING_STAGE_RESULT_ERROR';
export const RECEIVED_STAGE_RESULT = 'RECEIVED_STAGE_RESULT';

export const DELETING_STAGE_RESULT = 'DELETING_STAGE_RESULT';
export const DELETING_STAGE_RESULT_ERROR = 'DELETING_STAGE_RESULT_ERROR';
export const DELETED_STAGE_RESULT = 'DELETED_STAGE_RESULT';

// Race
export const FETCHING_RACES = 'FETCHING_RACES';
export const FETCHING_RACES_ERROR = 'FETCHING_RACES_ERROR';
export const RECEIVED_RACES = 'RECEIVED_RACES';

export const SAVING_RACE_SETTINGS = 'SAVING_RACE_SETTINGS';
export const SAVING_RACE_SETTINGS_ERROR = 'SAVING_RACE_SETTINGS_ERROR';
export const SAVED_RACE_SETTINGS = 'SAVED_RACE_SETTINGS';

export const DELETED_RACE = 'DELETED_RACE';
export const DELETING_RACE_ERROR = 'DELETING_RACE_ERROR';
export const DELETING_RACE = 'DELETING_RACE';

export const SAVING_RACE_RESULT = 'SAVING_RACE_RESULT';
export const SAVING_RACE_RESULT_ERROR = 'SAVING_RACE_RESULT_ERROR';
export const SAVED_RACE_RESULT = 'SAVED_RACE_RESULT';

export const FETCHING_RACE_SETTINGS = 'FETCHING_RACE_SETTINGS';
export const FETCHING_RACE_SETTINGS_ERROR = 'FETCHING_RACE_SETTINGS_ERROR';
export const RECEIVED_RACE_SETTINGS = 'RECEIVED_RACE_SETTINGS';

export const FETCHING_RACE_RESULT = 'FETCHING_RACE_RESULT';
export const FETCHING_RACE_RESULT_ERROR = 'FETCHING_RACE_RESULT_ERROR';
export const DELETED_RACE_RESULT = 'DELETED_RACE_RESULT';

export const DELETING_RACE_RESULT = 'DELETING_RACE_RESULT';
export const DELETING_RACE_RESULT_ERROR = 'DELETING_RACE_RESULT_ERROR';
export const RECEIVED_RACE_RESULT = 'RECEIVED_RACE_RESULT';
// Stage status
export const CHANGING_STAGE_STATUS = 'CHANGING_STAGE_STATUS';
export const CHANGING_STAGE_STATUS_ERROR = 'CHANGING_STAGE_STATUS_ERROR';
export const CHANGED_STAGE_STATUS = 'CHANGED_STAGE_STATUS';

// Change bar title
export function changeBarTitle(title) {
  return {
    type: CHANGE_BAR_TITLE,
    title,
  }
}

// Send websocket message
export function wsStatus(status) {
	return {
		type: WS_STATUS,
		status,
	}
}

export function newMessage(message) {
	return {
		type: MESSAGE,
		message,
	}
}

export function messageChunk(data) {
  return {
    type: MESSAGE_CHUNK,
    data,
  }
}

export function register(roomId) {
	return {
		type: REGISTERED,
		roomId,
	}
}

export function joinRoom(roomId) {
	return (dispatch, getState) => {
    const states = getState();

    if (states.overall.roomId !== roomId) {
      console.log('gooing to register new', roomId);
      dispatch(register(roomId));
      return dispatch((dispatch) => {
        registerRoom(roomId);
      });
    } else {
      console.log('its already in the room');
      return false;
    }
	}
}

// Send new location
export function sendNewObject(type, payload) {
	return (dispatch, getState) => {
    // Get the current stage slug
    const state = getState();
    const topic  = topicNameFromStage(state.stage.settings);
    if (topic) {
      return dispatch(sendObject(topic, type, payload));
    } else {
      return false;
    }
	}
}

export function objectSent(type, payload) {
	switch(type) {
		case 'location':
			return {
				type: LOCATION_SENT,
			}
		case 'post':
			return {
				type: POST_SENT,
			}
    case 'result':
      return {
        type: RESULT_SENT,
      }
    case 'situations':
      return {
        type: SITUATIONS_SENT,
      }
    case 'map':
      return {
        type: MAP_SENT,
      }
    default:
      return null;
	}
}

function sendObject(topic, type, payload) {
	return dispatch => {
		// Send object
		const endPoint = API_ENDPOINT + '/object';
		fetch(endPoint, {
			method: 'POST',
      credentials: 'include',
			body: JSON.stringify({
        topic,
				type,
				payload,
			}),
		}).then((response) => {
			return response.json();
		}).then((data) => {
			console.log('payload sent', data);
			dispatch(objectSent(type));
		}).catch((error) => {
			console.log("There is an error while sending new object", error);
		})
	}
}

// Toggle menu
export function toggleMenu() {
  return {
    type: TOGGLE_MENU,
  }
}

export function toggleSecondMenu() {
  return {
    type: TOGGLE_SECOND_MENU,
  }
}

// Todo: this might be an issue in the future when the geojson is too big
export function saveCourseGeo(type, course, draw) {
  return (dispatch, getState) => {
    const state = getState();

    let drawData = {
      timestamp: + new Date(),
      data: draw,
    }

    // Figure out what sort of settings is it
    let netSettings = false;
    switch (type) {
      case 'race':
        netSettings = state.race.settings;
        break;
      case 'stage':
        netSettings = state.stage.settings;
        break;
      default:
    }

    if (netSettings) {
      netSettings.courseGeo = {
        course,
        drawData,
      };

      // Send the draw data directly to map on the frontned
      // Race is more static
      if (type === 'stage') {
        // Dispatch a change for the map as well
        dispatch(sendNewObject('map', drawData));
      }

      // Save the stage settings
      return dispatch(saveSettings(type, netSettings));
    } else {
      return false;
    }
  }
}

export function saveSituations(data) {
  return (dispatch, getState) => {
    const state = getState();

    // Save the situation
    let situations = {
      timestamp: + new Date(),
      data,
    }

    let netSettings = state.stage.settings;
    netSettings.situations = situations;

    // Send the draw data directly to map on the frontned
    dispatch(sendNewObject('situations', situations));

    // Save the stage settings
    return dispatch(saveSettings('stage', netSettings));
  }
}

export function saveStartList(type, startList) {
  return (dispatch, getState) => {
    const state = getState();

    let newSettings = false;
    switch (type) {
      case 'race':
        newSettings = state.race.settings;
        break;
      case 'stage':
        newSettings = state.stage.settings;
        break;
      default:
    }

    if (newSettings) {
      // Assign the start list to new settings
      newSettings.startList = startList;

      // Save the stage settings
      return dispatch(saveSettings(type, newSettings));
    } else {
      return false;
    }
  }
}

export function addSituation(situation) {
  return {
    type: ADD_SITUATION,
    data: situation,
  }
}

export function editSituation(id, situation) {
  return {
    type: EDIT_SITUATION,
    data: {
      id,
      situation,
    }
  }
}

export function removeSituation(id) {
  return {
    type: REMOVE_SITUATION,
    id,
  }
}

export function rearrangeSituations(situations) {
  return {
    type: REARRANGE_SITUATION,
    data: situations,
  }
}

export function login(userName, password) {
  const actionType = 'login';
	return (dispatch, getState) => {
    // Change the request
    dispatch(preFetch(actionType));

    // Set the form body
    let formBody = [];
    formBody.push(encodeURIComponent('username') + '=' + encodeURIComponent(userName));
    formBody.push(encodeURIComponent('password') + '=' + encodeURIComponent(password));

    // The request object
    const requestObj = new Request(
      API_ENDPOINT + '/login',
      {
        method: "POST",
        credentials: 'include',
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded',
        },
        body: formBody.join('&'),
      },
    );
		return dispatch(doFetch(actionType, requestObj));
	}
}

export function logout() {
  const actionType = 'logout';
	return (dispatch, getState) => {
    // Log out
    dispatch({
      type: LOGOUT,
    });

    // The request object
    const requestObj = new Request(
      API_ENDPOINT + '/logout',
      {
        method: "GET",
        credentials: 'include',
      },
    );
		return dispatch(doFetch(actionType, requestObj));
	}
}

export function checkLogin() {
  const actionType = 'checkLogin';
	return (dispatch, getState) => {
    // The request object
    const requestObj = new Request(
      API_ENDPOINT + '/me',
      {
        method: "GET",
        credentials: 'include',
      },
    );
		return dispatch(doFetch(actionType, requestObj));
	}
}

//const jsonHeader = {
  //'Accept': 'application/json, text/plain, */*',
 // 'Content-Type': 'application/json',
//}
// Save settings
export function saveSettings(type, settings) {
  const actionType = `save${_.capitalize(type)}Settings`;
	return (dispatch, getState) => {
    // Change the request
    dispatch(preFetch(actionType));

    const requestObj = new Request(
      API_ENDPOINT + `/coll/${type}`,
      {
        method: "PUT",
        mode: 'cors',
        credentials: 'include',
        body: JSON.stringify(settings),
      },
    );
    console.log('going to save settings', settings, requestObj);
		return dispatch(doFetch(actionType, requestObj));
	}
}

// Delete collections - race, stage and etc.
export function deleteColl(type, id) {
  const actionType = `delete${_.capitalize(type)}`;
	return (dispatch, getState) => {
    // Change the request
    dispatch(preFetch(actionType));

    const requestObj = new Request(
      API_ENDPOINT + `/coll/${type}/${id}`,
      {
        method: "DELETE",
        mode: 'cors',
        credentials: 'include',
      },
    );
    console.log('going to delete', type, requestObj);
		return dispatch(doFetch(actionType, requestObj));
	}
}

export function getSettings(type, slug) {
  const actionType = `get${_.capitalize(type)}Settings`;
	return (dispatch, getState) => {
    // Change the request
    dispatch(preFetch(actionType));

    // Get the url
    let url = API_ENDPOINT + `/coll/${type}?slug=${slug}`;

    // If its stage, then we need to get the race id
    if (type === 'stage') {
      const state = getState();
      if (state.race.settings._id) {
        url += `&raceId=${state.race.settings._id}`;
      }
    }

    // Request Object
    const requestObj = new Request(
      url,
      {
        method: 'GET',
        credentials: 'include',
      },
    );

    var afterFetch = function(payload) {
      const settings = payload.data;
      console.log('this is the then in getSettings', getState(), settings);
      dispatch(changeBarTitle(settings.name));
    }

		dispatch(doFetch(actionType, requestObj, afterFetch));
	}
}

export function preFetch(type, data) {
	switch(type) {
		case 'saveRaceSettings':
			return {
				type: SAVING_RACE_SETTINGS,
        data,
			}
		case 'saveStageSettings':
			return {
				type: SAVING_STAGE_SETTINGS,
        data,
			}
		case 'deleteStage':
			return {
				type: DELETING_STAGE,
        data,
			}
		case 'deleteRace':
			return {
				type: DELETING_RACE,
        data,
			}
		case 'getRaceSettings':
			return {
				type: FETCHING_RACE_SETTINGS,
        data,
			}
		case 'getStageSettings':
			return {
				type: FETCHING_STAGE_SETTINGS,
        data,
			}
    case 'getRaces':
      return {
        type: FETCHING_RACES,
        data,
      }
    case 'addRace':
      return {
        type: ADDING_RACE,
        data,
      }
    case 'addStage':
      return {
        type: ADDING_STAGE,
        data,
      }
    case 'changeStageStatus':
      return {
        type: CHANGING_STAGE_STATUS,
        data,
      }
    case 'saveRaceResult':
      return {
        type: SAVING_RACE_RESULT,
        data,
      }
    case 'saveStageResult':
      return {
        type: SAVING_STAGE_RESULT,
        data,
      }
    case 'deleteStageResult':
      return {
        type: DELETING_STAGE_RESULT,
        data,
      }
    case 'deleteRaceResult':
      return {
        type: DELETING_RACE_RESULT,
        data,
      }
    case 'getRaceResult':
      return {
        type: FETCHING_RACE_RESULT,
        data,
      }
    case 'getStageResult':
      return {
        type: FETCHING_STAGE_RESULT,
        data,
      }
    case 'login':
      return {
        type: LOADING,
        data,
      }
    default:
      return null;
	}
}

export function fetched(type, payload) {
	switch(type) {
		case 'saveRaceSettings':
			return {
				type: SAVED_RACE_SETTINGS,
        data: payload.data,
			}
		case 'saveStageSettings':
			return {
				type: SAVED_STAGE_SETTINGS,
        data: payload.data,
			}
		case 'deleteStage':
			return {
				type: DELETED_STAGE,
        data: payload.data,
			}
		case 'deleteRace':
			return {
				type: DELETED_RACE,
        data: payload.data,
			}
		case 'getRaceSettings':
			return {
				type: RECEIVED_RACE_SETTINGS,
        data: payload.data,
			}
		case 'getStageSettings':
			return {
				type: RECEIVED_STAGE_SETTINGS,
        data: payload.data,
			}
    case 'getRaces':
      return {
        type: RECEIVED_RACES,
        data: payload.data,
      }
    case 'addRace':
      return {
        type: ADDED_RACE,
        data: payload.data,
      }
    case 'addStage':
      return {
        type: ADDED_STAGE,
        data: payload.data,
      }
    case 'changeStageStatus':
      return {
        type: CHANGED_STAGE_STATUS,
        data: payload.data,
      }
    case 'saveRaceResult':
      return {
        type: SAVED_RACE_RESULT,
        data: payload.data,
      }
    case 'saveStageResult':
      return {
        type: SAVED_STAGE_RESULT,
        data: payload.data,
      }
    case 'deleteStageResult':
      return {
        type: DELETED_STAGE_RESULT,
        data: payload.data,
      }
    case 'deleteRaceResult':
      return {
        type: DELETED_RACE_RESULT,
        data: payload.data,
      }
    case 'getRaceResult':
      return {
        type: RECEIVED_RACE_RESULT,
        data: payload.data,
      }
    case 'getStageResult':
      return {
        type: RECEIVED_STAGE_RESULT,
        data: payload.data,
      }
    case 'login':
      return {
        type: LOGIN_SUCCESS,
        data: payload.data,
      }
    case 'checkLogin':
      return {
        type: LOGIN_SUCCESS,
        data: payload.data,
      }
    default:
	}
}

export function fetchError(type, data) {
	switch(type) {
		case 'saveRaceSettings':
			return {
				type: SAVING_RACE_SETTINGS_ERROR,
        data,
			}
		case 'saveStageSettings':
			return {
				type: SAVING_STAGE_SETTINGS_ERROR,
        data,
			}
		case 'deleteStage':
			return {
				type: DELETING_STAGE_ERROR,
        data,
			}
		case 'deleteRace':
			return {
				type: DELETING_RACE_ERROR,
        data,
			}
		case 'getRaceSettings':
			return {
				type: FETCHING_RACE_SETTINGS_ERROR,
        data,
			}
		case 'getStageSettings':
			return {
				type: FETCHING_STAGE_SETTINGS_ERROR,
        data,
			}
    case 'getRaces':
      return {
        type: FETCHING_RACES_ERROR,
        data,
      }
    case 'addRace':
      return {
        type: ADDING_RACE_ERROR,
        data,
      }
    case 'addStage':
      return {
        type: ADDING_STAGE_ERROR,
        data,
      }
    case 'saveRaceResult':
      return {
        type: SAVING_RACE_RESULT_ERROR,
        data,
      }
    case 'saveStageResult':
      return {
        type: SAVING_STAGE_RESULT_ERROR,
        data,
      }
    case 'deleteRaceResult':
      return {
        type: DELETING_RACE_RESULT_ERROR,
        data,
      }
    case 'deleteStageResult':
      return {
        type: DELETING_STAGE_RESULT_ERROR,
        data,
      }
    case 'getRaceResult':
      return {
        type: FETCHING_RACE_RESULT_ERROR,
        data,
      }
    case 'getStageResult':
      return {
        type: FETCHING_STAGE_RESULT_ERROR,
        data,
      }
    case 'changeStageStatus':
      return {
        type: CHANGING_STAGE_STATUS_ERROR,
        data,
      }
    case 'login':
      return {
        type: LOGIN_FAILED,
        data,
      }
    case 'checkLogin':
      return {
        type: NO_OP,
      }
    default:
	}
}

function doFetch(type, requestObj, cb) {
	return dispatch => {
    // Do a fetch
		fetch(requestObj).then((response) => {
			return response.json();
		}).then((payload) => {
			console.log('payload sent', payload);

      // Check for payload success
      if (payload.success) {
        dispatch(fetched(type, payload));
      } else {
        dispatch(fetchError(type, payload.msg));
      }

      // Execute the cb
      if (cb && payload.success) {
        cb(payload);
      }
		}).catch((error) => {
			console.error("There is an error while doing a fetch object", error, type, requestObj);
      dispatch(fetchError(type, `Sorry can not fetch settings for ${type}`));
		})
	}
}

export function addStage(stage) {
	return (dispatch, getState) => {
    const requestObj = new Request(
      API_ENDPOINT + '/coll/stage',
      {
        method: 'POST',
        credentials: 'include',
        body: JSON.stringify(stage),
      },
    );
		return dispatch(doFetch('addStage', requestObj));
	}
}

// Race stuff
export function addRace(race) {
	return (dispatch, getState) => {
    const requestObj = new Request(
      API_ENDPOINT + '/coll/race',
      {
        method: 'POST',
        credentials: 'include',
        body: JSON.stringify(race),
      },
    );
		return dispatch(doFetch('addRace', requestObj));
	}
}

export function clearAdd(type) {
  switch(type) {
    case 'race':
      return {
        type: CLEAR_ADDED_RACE,
      }
    case 'stage':
      return {
        type: CLEAR_ADDED_STAGE,
      }
    default:
      return null;
  }
}
export function getRaces() {
	return (dispatch, getState) => {
    // todo:Get the owner id as well
    // so will only get race for the current owner
    const state = getState();
    const requestObj = new Request(
      API_ENDPOINT + '/coll/race?multi=true&owner=' + state.overall.user._id,
      {
        method: 'GET',
        credentials: 'include',
      },
    );
		return dispatch(doFetch('getRaces', requestObj));
	}
}

export function changeStageStatus(status) {
  return (dispatch, getState) => {
    // Request the change
    dispatch(preFetch('changeStageStatus', status));

    // Payload for update
    const state = getState();
    const payload = {
      _id: state.stage.settings._id,
      status,
    }

    // Request object
    const requestObj = new Request(
      API_ENDPOINT + '/coll/stage',
      {
        method: 'PUT',
        credentials: 'include',
        body: JSON.stringify(payload),
      },
    );
		return dispatch(doFetch('changeStageStatus', requestObj));
  }
}

// Save result
export function saveResult(type, result) {
  const actionType = `save${_.capitalize(type)}Result`;
	return (dispatch, getState) => {
    // Change the request
    dispatch(preFetch(actionType));

    // Get the id depend on the type
    if (result.relationType || !result.relationId) {
      // Set to the current type
      result.relationType = type;

      let states = getState();
      switch (type) {
        case 'race':
          result.relationId = states.race.settings._id;
          break;
        case 'stage':
          result.relationId = states.stage.settings._id;
          break;
        default:
      }
    }

    const requestObj = new Request(
      API_ENDPOINT + `/coll/result`,
      {
        method: "PUT",
        mode: 'cors',
        credentials: 'include',
        body: JSON.stringify(result),
      },
    );

    // After save result
    // Also send the live update
    let afterSaveResult = null;
    if (type === 'stage') {
      afterSaveResult = function(response) {
        console.log('going to dispatch new result', result);
        const publishDate = new Date(result.publishDate);
        dispatch(sendNewObject('result', {
          timestamp: publishDate.getTime(),
          data: response.data,
        }));
      }
    }

    console.log('going to save result', result, requestObj);
		return dispatch(doFetch(actionType, requestObj, afterSaveResult));
	}
}

// get result
export function getResult(type, id) {
  const actionType = `get{_.capitalize(type)}Result`;
	return (dispatch, getState) => {
    // Change the request
    dispatch(preFetch(actionType));

    const requestObj = new Request(
      API_ENDPOINT + `/coll/result?relationType=${type}&relationId=${id}`,
      {
        method: "GET",
        credentials: 'include',
      },
    );
    console.log('going to get result', id, requestObj);
		return dispatch(doFetch(actionType, requestObj));
	}
}

// Delete result
export function deleteResult(type, id) {
  const actionType = `delete${_.capitalize(type)}Result`;
	return (dispatch, getState) => {
    // Change the request
    dispatch(preFetch(actionType));

    const requestObj = new Request(
      API_ENDPOINT + `/coll/result/${id}`,
      {
        method: "DELETE",
        credentials: 'include',
      },
    );

    let afterDeleteResult = null;
    if (type === 'stage') {
      afterDeleteResult = function() {
        dispatch(sendNewObject('command', {
          action: 'delete',
          value: {
            type: 'result',
            id: id,
          }
        }));
      }
    }

    console.log('going to remove result', id, requestObj);
		return dispatch(doFetch(actionType, requestObj, afterDeleteResult));
	}
}
