import _ from 'lodash';
import {
  API_ENDPOINT,
} from './config.js';

function debugOutput(one, two, three) {
	console.log(one, two, three);
}

export function useLatest(arr) {
  if (arr && arr.length > 0) {
    if (arr.length === 1) {
      return arr[0];
    } else {
      let last = arr[0];
      for (let i = 1; i < arr.length; i++) {
        let current = arr[i];

        if (current.timestamp > last.timestamp) {
          last = current;
        }
      }

      return last;
    }
  } else {
    return false;
  }
}

// get topic name from stage
export function topicNameFromStage(stage) {
  return `${stage._id}_${stage.slug}`;
}

// Check if the locaiton array is valid or not
export function isValidCoordinate(location) {
  if (location.length === 2) {
    // its greater than 90 bound
    if (location[0] > 90 || location[0] < -90) {
      return false;
    }

    // its greater than 180 bound
    if (location[1] > 180 || location[1] < -180) {
      return false;
    }
  } else {
    return false;
  }

  return true;
}

async function uploadToS3(file, signedRequest, acl = 'public-read') {
  const options = {
    method: 'PUT',
    headers: {
      'Content-Type': file.type,
      'x-amz-acl': acl,
      ACL: acl,
    },
    body: file,
  };

  // Going to put it to the S3
  const response = await fetch(signedRequest, options);

  // Check if the status is ok
  // S3 return empty body
  return response.status === 200;
}

export async function uploadFile(file) {
	// Read content to csv
	const res = await fetch(`${API_ENDPOINT}/signS3?fileName=${file.name}&fileType=${file.type}&filePath=uploads`, {
		method: 'POST',
		credentials: 'include',
    /*
		headers: {
			'Content-Type': 'application/json',
		},
    */
	});
	const data = await res.json();

	if (data.success) {
		const {
			signedRequest,
			url,
		} = data.data;

		// Upload to s3
		const ret = await uploadToS3(file, signedRequest, 'private');

		// Everything is good, then return the file info
		return {
      data: {
        link: url,
        name: file.name,
        size: file.size,
        type: file.type,
      }
    }
	} else {
		return false;
	}
}

/**
 * Mapping for the message key to states
 * these states all have following structure
 * posts: {
 *  stage-1: [
 *    d1, d2, d3
 *  ],
 *  stage-2: [
 *    d1, d2, d3
 *  ]
 */
const messageKeyToStateMapping = {
  map: 'maps',
  post: 'posts',
  situations: 'situationHistory',
  result: 'resultHistory',
  location: 'locations',
}
// Handle chunk message
export function handleMessages(existingInput, data) {
	let existingData = _.cloneDeep(existingInput);
	debugOutput('begining data and existing data******************', data, existingData);

	if (existingData['posts'] && existingData['posts']) {
		debugOutput('begining data and existing data******************', existingData['posts']);
	}

	if (data.length > 0) {

		debugOutput('this is before calculating the results');
		let result = incomingDataToExistingStructure(data);

		debugOutput('sorted results and existing data******************', result, existingData);

		// Sort the result
		result = sortByTimestamp(result);

		// Also sort the existing data as well
		existingData = sortByTimestamp(existingData);

		debugOutput('sorted results and existing data******************', result, existingData);

		// Go through each datatype to see if there is new on results
		// That not in the existing
		for (let dType of Object.values(messageKeyToStateMapping)) {
				debugOutput('dType', dType);
        if (result[dType] && !existingData[dType]) {
          // the existing data doesn't have it, then just add the whole thing
          existingData[dType] = result[dType];
        } else if (result[dType]) {
						debugOutput('keys in this dType', result[dType], typeof result[dType], Object.keys(result[dType]));
						let newKeys = _.difference(Object.keys(result[dType]), Object.keys(existingData[dType]));

						// Add this new key to the existing data
						if (newKeys.length > 0) {
								for (let newKey of newKeys) {
										// going to add the new stage name
										debugOutput('going to add new stage name', newKey);
										existingData[dType][newKey] = result[dType][newKey];
								}
						}
				}
		}

		debugOutput('sorted results and existing data******************', result, existingData);

		// Construct a new existing data that's merge and sort two array into a third sorted array
		for (let dType of Object.keys(existingData)) {
				for (let topic of Object.keys(existingData[dType])) {
						if (result[dType] && result[dType][topic]) {
								// merge the two sorted array
								debugOutput('going to merge', dType, topic);
								existingData[dType][topic] = mergeByTimestamp(existingData[dType][topic], result[dType][topic]);
						} else {// do nothing
						}
				}
		}

		debugOutput('before return results and existing data*******************', result, existingData);
		return existingData;
	} else {
			return {};
	}
}

export function incomingDataToExistingStructure(data) {
    debugOutput('this is inside the gettng data into the same format', data);
    // The result in here is have same struct as the existing object
    return data.reduce((acc,item)=>{
      console.log('going through each item', item['_topic'], item.topic, item.key, item);
        // Figure out the topic and key/type
        // Before the old structure doesn't have topic and key/type in the value itself
        let topicName = null;
        let keyName = null;
        let value = null;
        if (_.isEmpty(item['_topic'])) {
            topicName = item.topic;
            keyName = item.key;
            value = item.value;
        } else {
            topicName = item['_topic'];
            keyName = item['_type'];
            value = item;
        }

        debugOutput('each item', topicName, keyName, value);

        // Check if we can map the item's key or not
        let stateKey = messageKeyToStateMapping[keyName];
        if (!_.isEmpty(stateKey)) {
            if (_.isEmpty(acc[stateKey])) {
                acc[stateKey] = {};
            }

            // Check location for integrity
            if (keyName === 'location') {
                debugOutput('location data');
                // Check for the data
                if (value.data) {
                    if (value.data[0] > 90 || value.data[0] < -90) {
                        debugOutput('its greater than 90 bound');
                        return acc;
                    }

                    if (value.data[1] > 180 || value.data[1] < -180) {
                        debugOutput('its greater than 180 bound');
                        return acc;
                    }
                }
            }

            // Check if the topic key exist or not
            if (_.isEmpty(acc[stateKey][topicName])) {
                acc[stateKey][topicName] = []
            }

            // Assign to the belonging topic
            acc[stateKey][topicName].push(value);
        }

        // Return the accumulation
        return acc;
    }
    , {});
}

export function sortByTimestamp(data) {
    for (let dataType in data) {
        for (let topic in data[dataType]) {
            data[dataType][topic] = _.sortBy(data[dataType][topic], ['timestamp']);
        }
    }

    return data;
}
//sortByTimestamp(existingData);


// Merge two array by and order by timestamp low to high
export function mergeByTimestamp(input1, input2) {
    const result = [];
    let running = true;

    // Sort both array first
    const arr1 = _.sortBy(input1, 'timestamp');
    const arr2 = _.sortBy(input2, 'timestamp');

    const addedTimestamps = {};
    const addToResult = (input)=>{
        if (!addedTimestamps[input.timestamp]) {
            result.push(input);
            addedTimestamps[input.timestamp] = true;
        }
    }

    do {
        // Nothing to do if there is nothing in the array
        if (arr1.length === 0 && arr2.length === 0) {
            running = false;
        } else {
            // Check if they equal or not
            if (arr1.length > 0 && arr2.length === 0) {
                addToResult(arr1.shift());
            } else if (arr1.length === 0 && arr2.length > 0) {
                addToResult(arr2.shift());
            } else {
                // Compare the two value
                const a = new Date(arr1[0].timestamp).getTime();
                const b = new Date(arr2[0].timestamp).getTime();
                if (a > b) {
                    addToResult(arr2.shift());
                } else if (b > a) {
                    addToResult(arr1.shift());
                } else {
                    // equal just pick one and take it off both
                    addToResult(arr1.shift());
                    arr2.shift();
                }
            }
        }
    } while (running);return result;
}
